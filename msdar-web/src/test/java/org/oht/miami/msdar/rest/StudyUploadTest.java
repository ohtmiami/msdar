package org.oht.miami.msdar.rest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import junit.framework.Assert;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartInput;
import org.jboss.resteasy.plugins.providers.multipart.MultipartOutput;
import org.jboss.resteasy.plugins.providers.multipart.OutputPart;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.oht.miami.msdar.data.StudyInfoRepository;
import org.oht.miami.msdar.model.StudyInfo;
import org.oht.miami.msdar.rest.MINTMultipartWriter;
import org.oht.miami.msdar.rest.StudyUploadRESTService;
import org.oht.miami.msdar.service.CassandraService;
import org.oht.miami.msdar.service.StudyStorageService;
import org.oht.miami.msdar.util.Resources;
import org.oht.miami.msdtk.store.LocalStudyStore;

@RunWith(Arquillian.class)
public class StudyUploadTest {

    private static final Path BASE_OUTPUT_PATH = Paths.get("target/test-out/rest");

    private static final String STUDY_INSTANCE_UID = "1.2.392.200036.9116.2.2.2.1762893313.1029997326.945873";

    private static final String VERSION_UUID = "94910afc-91cd-4dcb-87fb-03809166f385";

    private static final String BULK_DATA_UUID = "74403917-7b74-48c5-b97d-d80e37e58fe2";

    private static final String VERSION_RESOURCE_NAME = String.format("ms-dicom/20phase/%s/%s.%s",
            STUDY_INSTANCE_UID, VERSION_UUID, LocalStudyStore.VERSION_FILE_EXT);

    private static final String BULK_DATA_RESOURCE_NAME = String.format(
            "ms-dicom/20phase/%s/%s.%s", STUDY_INSTANCE_UID, BULK_DATA_UUID,
            LocalStudyStore.BULK_DATA_FILE_EXT);

    @Inject
    private StudyInfoRepository studyInfoRepository;

    @Inject
    private StudyStorageService studyStorage;

    @Deployment
    public static Archive<?> createTestArchive() {

        return ShrinkWrap
                .create(WebArchive.class, "msdar-web-rest-test.war")
                .addClasses(StudyStorageService.class, CassandraService.class)
                .addPackage(StudyInfo.class.getPackage())
                .addPackage(StudyInfoRepository.class.getPackage())
                .addPackage(StudyUploadRESTService.class.getPackage())
                .addPackage(Resources.class.getPackage())
                .addAsLibraries(
                        Maven.resolver().loadPomFromFile("pom.xml").importRuntimeDependencies()
                                .asFile()).addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("cassandra.properties").addAsResource(VERSION_RESOURCE_NAME)
                .addAsResource(BULK_DATA_RESOURCE_NAME);
    }

    @BeforeClass
    public static void setUp() {

        // The HTTP parser in Apache James will not recognize a message as
        // Multi-part MIME if its content type does not begin with "multipart/"
        // (see MimeEntity.java, line 136). Therefore, "multipart/mint" is used
        // instead of "application/mint".
        ResteasyProviderFactory.getInstance().addMessageBodyWriter(MINTMultipartWriter.class);
        ResteasyProviderFactory.getInstance().addMessageBodyReader(MINTMultipartReader.class);
    }

    @Test
    @InSequence(1)
    public void initStudyStore() throws IOException {

        try {
            studyStorage.getStudyStore().purge();
        } catch (NoSuchFileException e) {
            // Ignore
        }
    }

    @Test
    @RunAsClient
    @InSequence(2)
    public void testUploadStudy(@ArquillianResource URL baseURL) throws Exception {

        final ClassLoader loader = Thread.currentThread().getContextClassLoader();
        final String serviceURL = "rest/studies";

        try (InputStream versionIn = loader.getResourceAsStream(VERSION_RESOURCE_NAME);
                InputStream bulkDataIn = loader.getResourceAsStream(BULK_DATA_RESOURCE_NAME)) {
            MultipartOutput output = new MultipartOutput();
            OutputPart versionPart = output.addPart(versionIn,
                    HTTPConstants.APPLICATION_MSDICOM_TYPE);
            versionPart.getHeaders().putSingle(HTTPConstants.UUID_HEADER, VERSION_UUID);
            OutputPart bulkDataPart = output.addPart(bulkDataIn,
                    MediaType.APPLICATION_OCTET_STREAM_TYPE);
            bulkDataPart.getHeaders().putSingle(HTTPConstants.UUID_HEADER, BULK_DATA_UUID);

            ClientRequest request = new ClientRequest(new URL(baseURL, serviceURL).toString());
            request.body(HTTPConstants.MULTIPART_MINT_TYPE, output);
            ClientResponse<?> response = request.post();
            Assert.assertEquals(Response.Status.CREATED, response.getResponseStatus());
        }
    }

    @Test
    @RunAsClient
    @InSequence(3)
    public void testRetrieveStudy(@ArquillianResource URL baseURL) throws Exception {

        final String serviceURL = String.format("rest/studies/%s", STUDY_INSTANCE_UID);
        final Path outputPath = BASE_OUTPUT_PATH.resolve("retrieved_study");

        ClientRequest request = new ClientRequest(new URL(baseURL, serviceURL).toString());
        request.accept(HTTPConstants.MULTIPART_MINT_TYPE);
        MultipartInput input = request.getTarget(MultipartInput.class);
        List<InputPart> parts = input.getParts();
        Assert.assertEquals(2, parts.size());

        Files.createDirectories(outputPath);

        InputPart versionPart = parts.get(0);
        Assert.assertEquals(VERSION_UUID,
                versionPart.getHeaders().getFirst(HTTPConstants.UUID_HEADER));
        try (ReadableByteChannel versionIn = Channels.newChannel(versionPart.getBody(
                InputStream.class, null))) {
            Path versionPath = outputPath.resolve(VERSION_UUID + LocalStudyStore.VERSION_FILE_EXT);
            try (FileChannel versionOut = FileChannel.open(versionPath, StandardOpenOption.CREATE,
                    StandardOpenOption.WRITE)) {
                versionOut.transferFrom(versionIn, 0, Integer.MAX_VALUE);
            }
        }

        InputPart bulkDataPart = parts.get(1);
        Assert.assertEquals(BULK_DATA_UUID,
                bulkDataPart.getHeaders().getFirst(HTTPConstants.UUID_HEADER));
        try (ReadableByteChannel bulkDataIn = Channels.newChannel(bulkDataPart.getBody(
                InputStream.class, null))) {
            Path bulkDataPath = outputPath.resolve(BULK_DATA_UUID
                    + LocalStudyStore.BULK_DATA_FILE_EXT);
            try (FileChannel bulkDataOut = FileChannel.open(bulkDataPath,
                    StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
                bulkDataOut.transferFrom(bulkDataIn, 0, Integer.MAX_VALUE);
            }
        }
    }

    @Test
    @RunAsClient
    @InSequence(3)
    public void testRetrieveMetadata(@ArquillianResource URL baseURL) throws Exception {

        final String serviceURL = String.format("rest/studies/%s/metadata", STUDY_INSTANCE_UID);
        final Path outputPath = BASE_OUTPUT_PATH.resolve("retrieved_metadata");

        ClientRequest request = new ClientRequest(new URL(baseURL, serviceURL).toString());
        request.accept(HTTPConstants.APPLICATION_MSDICOM_TYPE);
        ClientResponse<InputStream> response = request.get(InputStream.class);
        Assert.assertEquals(Response.Status.OK, response.getResponseStatus());
        Assert.assertEquals(VERSION_UUID, response.getHeaders().getFirst(HTTPConstants.UUID_HEADER));

        Files.createDirectories(outputPath);

        try (ReadableByteChannel versionIn = Channels.newChannel(response.getEntity())) {
            Path versionPath = outputPath.resolve(VERSION_UUID + LocalStudyStore.VERSION_FILE_EXT);
            try (FileChannel versionOut = FileChannel.open(versionPath, StandardOpenOption.CREATE,
                    StandardOpenOption.WRITE)) {
                versionOut.transferFrom(versionIn, 0, Integer.MAX_VALUE);
            }
        }
    }

    @Test
    @RunAsClient
    @InSequence(3)
    public void testRetrieveBulkData(@ArquillianResource URL baseURL) throws Exception {

        final String serviceURL = String.format("rest/studies/%s/bulkdata", STUDY_INSTANCE_UID);
        final Path outputPath = BASE_OUTPUT_PATH.resolve("retrieved_bulkdata");

        ClientRequest request = new ClientRequest(new URL(baseURL, serviceURL).toString());
        request.accept(MediaType.APPLICATION_OCTET_STREAM_TYPE);
        ClientResponse<InputStream> response = request.get(InputStream.class);
        Assert.assertEquals(Response.Status.OK, response.getResponseStatus());
        Assert.assertEquals(BULK_DATA_UUID,
                response.getHeaders().getFirst(HTTPConstants.UUID_HEADER));

        Files.createDirectories(outputPath);

        try (ReadableByteChannel bulkDataIn = Channels.newChannel(response.getEntity())) {
            Path bulkDataPath = outputPath.resolve(VERSION_UUID + LocalStudyStore.VERSION_FILE_EXT);
            try (FileChannel bulkDataOut = FileChannel.open(bulkDataPath,
                    StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
                bulkDataOut.transferFrom(bulkDataIn, 0, Integer.MAX_VALUE);
            }
        }
    }
}
