package org.oht.miami.msdar.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.MultipartOutput;
import org.jboss.resteasy.plugins.providers.multipart.OutputPart;
import org.oht.miami.msdar.data.StudyInfoRepository;
import org.oht.miami.msdar.model.StudyInfo;
import org.oht.miami.msdar.service.StudyStorageService;
import org.oht.miami.msdtk.store.StudyStore;
import org.oht.miami.msdtk.util.DicomUID;

@Path("/studies")
@RequestScoped
public class StudyRetrieveRESTService {

    @Inject
    private Logger log;

    @Inject
    private StudyInfoRepository studyInfoRepository;

    @Inject
    private StudyStorageService studyStorage;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> listStudies() {

        List<StudyInfo> studyInfos = studyInfoRepository.findAll();
        List<String> studyInstanceUIDs = new ArrayList<String>(studyInfos.size());
        for (StudyInfo studyInfo : studyInfos) {
            studyInstanceUIDs.add(studyInfo.getStudyInstanceUID());
        }
        log.debugf("Found %d studies", studyInstanceUIDs.size());
        return studyInstanceUIDs;
    }

    private StudyInfo findStudyInfo(DicomUID studyInstanceUID) throws WebApplicationException {

        StudyInfo studyInfo = studyInfoRepository.findByStudyInstanceUID(studyInstanceUID
                .toString());
        if (studyInfo == null) {
            log.errorf("Study %s does not exist", studyInstanceUID);
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return studyInfo;
    }

    @GET
    @Path("/{uid:[0-9.][0-9.]*}")
    @Produces(HTTPConstants.MULTIPART_MINT)
    public MultipartOutput retrieveStudy(@PathParam("uid") DicomUID studyInstanceUID)
            throws WebApplicationException {

        log.debugf("GET Whole Study (studyInstanceUID=%s)", studyInstanceUID);

        StudyInfo studyInfo = findStudyInfo(studyInstanceUID);
        UUID versionUUID = studyInfo.getVersionUUID();

        MultipartOutput parts = new MultipartOutput();
        StudyStore studyStore = studyStorage.getStudyStore();

        java.nio.file.Path versionFile = studyStore.getVersionFile(studyInstanceUID, versionUUID);
        OutputPart versionPart = parts.addPart(versionFile.toFile(),
                HTTPConstants.APPLICATION_MSDICOM_TYPE);
        versionPart.getHeaders().putSingle(HTTPConstants.UUID_HEADER, versionUUID);

        for (UUID bulkDataUUID : studyInfo.getBulkDataUUIDs()) {
            java.nio.file.Path bulkDataFile = studyStore.getBulkDataFile(studyInstanceUID,
                    bulkDataUUID);
            OutputPart bulkDataPart = parts.addPart(bulkDataFile.toFile(),
                    MediaType.APPLICATION_OCTET_STREAM_TYPE);
            bulkDataPart.getHeaders().putSingle(HTTPConstants.UUID_HEADER, bulkDataUUID);
        }
        return parts;
    }

    @GET
    @Path("/{uid:[0-9.][0-9.]*}/metadata")
    @Produces(HTTPConstants.APPLICATION_MSDICOM)
    public Response retrieveMetadata(@PathParam("uid") DicomUID studyInstanceUID)
            throws WebApplicationException {

        log.debugf("GET Metadata (studyInstanceUID=%s)", studyInstanceUID);

        StudyInfo studyInfo = findStudyInfo(studyInstanceUID);
        UUID versionUUID = studyInfo.getVersionUUID();

        StudyStore studyStore = studyStorage.getStudyStore();
        java.nio.file.Path versionFile = studyStore.getVersionFile(studyInstanceUID, versionUUID);
        return Response.ok(versionFile.toFile(), HTTPConstants.APPLICATION_MSDICOM_TYPE)
                .header(HTTPConstants.UUID_HEADER, versionUUID).build();
    }

    @GET
    @Path("/{uid:[0-9.][0-9.]*}/bulkdata")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response retrieveBulkData(@PathParam("uid") DicomUID studyInstanceUID)
            throws WebApplicationException {

        log.debugf("GET Bulk Data (studyInstanceUID=%s)", studyInstanceUID);

        StudyInfo studyInfo = findStudyInfo(studyInstanceUID);
        Set<UUID> bulkDataUUIDs = studyInfo.getBulkDataUUIDs();
        if (bulkDataUUIDs.isEmpty()) {
            return Response.noContent().build();
        }
        // FIXME This only returns the first Bulk Data file
        UUID bulkDataUUID = bulkDataUUIDs.iterator().next();

        StudyStore studyStore = studyStorage.getStudyStore();
        java.nio.file.Path bulkDataFile = studyStore
                .getBulkDataFile(studyInstanceUID, bulkDataUUID);
        return Response.ok(bulkDataFile.toFile(), MediaType.APPLICATION_OCTET_STREAM_TYPE)
                .header(HTTPConstants.UUID_HEADER, bulkDataUUID).build();
    }
}
