package org.oht.miami.msdar.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.faces.context.FacesContext;

import org.jboss.logging.Logger;

/**
 * This class uses CDI to alias Java EE resources, such as the persistence
 * context, to CDI beans
 * <p>
 * Example injection on a managed bean field:
 * </p>
 * 
 * <pre>
 * &#064;Inject
 * private EntityManager em;
 * </pre>
 */
public class Resources {

    @Produces
    public Logger produceLog(InjectionPoint injectionPoint) {

        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }

    @Produces
    @RequestScoped
    public FacesContext produceFacesContext() {

        return FacesContext.getCurrentInstance();
    }

    @Produces
    @Cached
    @ApplicationScoped
    public ExecutorService produceCachedThreadPool() {

        return Executors.newCachedThreadPool();
    }

    @Produces
    @ApplicationScoped
    public ScheduledExecutorService produceScheduledThreadPool() {

        // TODO Tune the size of the thread pool
        return Executors.newScheduledThreadPool(2);
    }

    public void shutDown(@Disposes @Cached ExecutorService threadPool) {

        try {
            threadPool.shutdownNow();
        } catch (SecurityException e) {
            threadPool.shutdown();
        }
    }

    public void shutDown(@Disposes ScheduledExecutorService scheduledThreadPool) {

        try {
            scheduledThreadPool.shutdownNow();
        } catch (SecurityException e) {
            scheduledThreadPool.shutdown();
        }
    }
}
