package org.oht.miami.msdar.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.util.Cached;
import org.oht.miami.msdtk.store.LocalStudyStore;
import org.oht.miami.msdtk.store.StudyStore;

@ApplicationScoped
public class StudyStorageService {

    @Inject
    private Logger log;

    @Inject
    @Cached
    private ExecutorService threadPool;

    @Inject
    private ScheduledExecutorService scheduledThreadPool;

    private static final Path BASE_PATH = Paths.get(System.getProperty("jboss.server.data.dir"));

    private String studyStorePathName = "msdar/study-store";

    private String deIdentifiedStudyStorePathName = "msdar/de-identified-study-store";

    private LocalStudyStore studyStore = StudyStore.getLocalStudyStore(BASE_PATH
            .resolve(studyStorePathName));

    private LocalStudyStore deIdentifiedStudyStore = StudyStore.getLocalStudyStore(BASE_PATH
            .resolve(deIdentifiedStudyStorePathName));

    private long inputBulkDataFileTimeoutMillis = 30000;

    private long outputBulkDataFileTimeoutMillis = 30000;

    private long maintenanceInitialDelayMillis = 10000;

    private long maintenancePeriodMillis = 10000;

    public static Path getBasePath() {

        return BASE_PATH;
    }

    public String getStudyStorePathName() {

        return studyStorePathName;
    }

    public void setStudyStorePathName(String studyStorePathName) {

        // Validate the provided path name
        Path studyStorePath = BASE_PATH.resolve(studyStorePathName);
        log.infof("Study Store path has been changed to %s", studyStorePath);
        this.studyStorePathName = studyStorePathName;
        studyStore = StudyStore.getLocalStudyStore(studyStorePath);
    }

    public String getDeIdentifiedStudyStorePathName() {

        return deIdentifiedStudyStorePathName;
    }

    public void setDeIdentifiedStudyStorePathName(String deIdentifiedStudyStorePathName) {

        // Validate the provided path name
        Path deIdentifiedStudyStorePath = BASE_PATH.resolve(deIdentifiedStudyStorePathName);
        log.infof("De-Identified Study Store path has been changed to %s",
                deIdentifiedStudyStorePath);
        this.deIdentifiedStudyStorePathName = deIdentifiedStudyStorePathName;
        deIdentifiedStudyStore = StudyStore.getLocalStudyStore(deIdentifiedStudyStorePath);
    }

    public StudyStore getStudyStore() {

        return studyStore;
    }

    public StudyStore getDeIdentifiedStudyStore() {

        return deIdentifiedStudyStore;
    }

    @PostConstruct
    public void start() {

        // TODO Load configuration from file
        studyStore.getActiveInputBulkDataFiles().enableTimeout(inputBulkDataFileTimeoutMillis,
                threadPool);
        studyStore.getActiveOutputBulkDataFiles().enableTimeout(outputBulkDataFileTimeoutMillis,
                threadPool);

        deIdentifiedStudyStore.setEncryptionEnabled(false);
        deIdentifiedStudyStore.getActiveInputBulkDataFiles().enableTimeout(
                inputBulkDataFileTimeoutMillis, threadPool);
        deIdentifiedStudyStore.getActiveOutputBulkDataFiles().enableTimeout(
                outputBulkDataFileTimeoutMillis, threadPool);

        scheduledThreadPool.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {

                studyStore.getActiveInputBulkDataFiles().cleanUp();
                studyStore.getActiveOutputBulkDataFiles().cleanUp();
                deIdentifiedStudyStore.getActiveInputBulkDataFiles().cleanUp();
                deIdentifiedStudyStore.getActiveOutputBulkDataFiles().cleanUp();
            }
        }, maintenanceInitialDelayMillis, maintenancePeriodMillis, TimeUnit.MILLISECONDS);
    }

    @PreDestroy
    public void stop() {

        // TODO Anything?
    }
}
