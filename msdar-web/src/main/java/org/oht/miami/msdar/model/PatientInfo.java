package org.oht.miami.msdar.model;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.oht.miami.msdtk.studymodel.Study;

public class PatientInfo {

    private UUID preferredUUID;

    private Set<UUID> otherUUIDs;

    private String patientName;

    private String patientID;

    private String issuerOfPatientID;

    private Date patientBirthDate;

    private String patientSex;

    public UUID getPreferredUUID() {

        return preferredUUID;
    }

    public void setPreferredUUID(UUID preferredUUID) {

        this.preferredUUID = preferredUUID;
    }

    public Set<UUID> getOtherUUIDs() {

        return otherUUIDs;
    }

    public void setOtherUUIDs(Set<UUID> otherUUIDs) {

        this.otherUUIDs = otherUUIDs;
    }

    public String getPatientName() {

        return patientName;
    }

    public void setPatientName(String patientName) {

        this.patientName = patientName;
    }

    public String getPatientID() {

        return patientID;
    }

    public void setPatientID(String patientID) {

        this.patientID = patientID;
    }

    public String getIssuerOfPatientID() {

        return issuerOfPatientID;
    }

    public void setIssuerOfPatientID(String issuerOfPatientID) {

        this.issuerOfPatientID = issuerOfPatientID;
    }

    public Date getPatientBirthDate() {

        return patientBirthDate;
    }

    public void setPatientBirthDate(Date patientBirthDate) {

        this.patientBirthDate = patientBirthDate;
    }

    public String getPatientSex() {

        return patientSex;
    }

    public void setPatientSex(String patientSex) {

        this.patientSex = patientSex;
    }

    public void populateWith(Study study) {

        DicomObject attributeSet = new BasicDicomObject();
        StudyInfo.copyAttributeIfPresent(attributeSet, study, Tag.SpecificCharacterSet, VR.CS);
        StudyInfo.copyAttributeIfPresent(attributeSet, study, Tag.PatientName, VR.PN);
        StudyInfo.copyAttributeIfPresent(attributeSet, study, Tag.PatientID, VR.LO);
        StudyInfo.copyAttributeIfPresent(attributeSet, study, Tag.IssuerOfPatientID, VR.LO);
        StudyInfo.copyAttributeIfPresent(attributeSet, study, Tag.PatientBirthDate, VR.DA);
        StudyInfo.copyAttributeIfPresent(attributeSet, study, Tag.PatientBirthTime, VR.TM);
        StudyInfo.copyAttributeIfPresent(attributeSet, study, Tag.PatientSex, VR.CS);

        patientName = attributeSet.getString(Tag.PatientName, "");
        patientID = attributeSet.getString(Tag.PatientID, "");
        issuerOfPatientID = attributeSet.getString(Tag.IssuerOfPatientID, "");
        patientBirthDate = attributeSet.getDate(Tag.PatientBirthDate, Tag.PatientBirthTime);
        patientSex = attributeSet.getString(Tag.PatientSex, "");
    }

    public void populateQueryMatch(DicomObject match, DicomObject keys) {

        if (keys.contains(Tag.PatientName)) {
            match.putString(Tag.PatientName, VR.PN, patientName);
        }
        if (keys.contains(Tag.PatientID)) {
            match.putString(Tag.PatientID, VR.LO, patientID);
        }
        if (keys.contains(Tag.IssuerOfPatientID)) {
            match.putString(Tag.IssuerOfPatientID, VR.LO, issuerOfPatientID);
        }
        if (keys.contains(Tag.PatientBirthDate)) {
            if (patientBirthDate != null) {
                match.putDate(Tag.PatientBirthDate, VR.DA, patientBirthDate);
            } else {
                match.putNull(Tag.PatientBirthDate, VR.DA);
            }
        }
        if (keys.contains(Tag.PatientBirthTime)) {
            if (patientBirthDate != null) {
                match.putDate(Tag.PatientBirthTime, VR.TM, patientBirthDate);
            } else {
                match.putNull(Tag.PatientBirthTime, VR.TM);
            }
        }
        if (keys.contains(Tag.PatientSex)) {
            match.putString(Tag.PatientSex, VR.CS, patientSex);
        }
        // TODO Number of Patient Related Studies
    }
}
