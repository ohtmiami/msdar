package org.oht.miami.msdar.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.data.AEInfoRepository;
import org.oht.miami.msdar.model.AEInfo;

@Model
@Named("aeInfoController")
public class AEInfoController {

    @Inject
    private Logger log;

    @Inject
    private AEInfoRepository aeInfoRepository;

    @Produces
    @Named
    private AEInfo newAEInfo;

    @PostConstruct
    private void initNewAEInfo() {

        newAEInfo = new AEInfo();
    }

    public void register() throws Exception {

        // TODO Check for empty/invalid fields
        aeInfoRepository.save(newAEInfo);
        log.infof("Added/updated AE %s (hostname=%s, port=%d)", newAEInfo.getAETitle(),
                newAEInfo.getHostname(), newAEInfo.getPort());
        initNewAEInfo();
    }
}
