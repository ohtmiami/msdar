package org.oht.miami.msdar.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.model.PatientInfo;
import org.oht.miami.msdar.service.CassandraService;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Query;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.querybuilder.QueryBuilder;

@ApplicationScoped
public class PatientInfoRepository {

    private static final String TABLE_NAME = "patientinfo";

    private static enum Column {

        PREFERRED_UUID("preferred_uuid", DataType.uuid()), OTHER_UUIDS("other_uuids", DataType
                .set(DataType.uuid())), PATIENT_NAME("patient_name", DataType.text()), PATIENT_ID(
                "patient_id", DataType.text()), ISSUER_OF_PATIENT_ID("issuer_of_patient_id",
                DataType.text()), PATIENT_BIRTH_DATE("patient_birth_date", DataType.timestamp()), PATIENT_SEX(
                "patient_sex", DataType.text());

        private String name;

        private DataType dataType;

        private Column(String name, DataType dataType) {

            this.name = name;
            this.dataType = dataType;
        }

        @Override
        public String toString() {

            return String.format("%s %s", name, dataType);
        }
    }

    private static final String CREATE_TABLE_CQL = String.format("CREATE TABLE %s (\n", TABLE_NAME)
            + String.format("  %s PRIMARY KEY,\n", Column.PREFERRED_UUID)
            + String.format("  %s,\n", Column.OTHER_UUIDS)
            + String.format("  %s,\n", Column.PATIENT_NAME)
            + String.format("  %s,\n", Column.PATIENT_ID)
            + String.format("  %s,\n", Column.ISSUER_OF_PATIENT_ID)
            + String.format("  %s,\n", Column.PATIENT_BIRTH_DATE)
            + String.format("  %s\n", Column.PATIENT_SEX) + ")";

    private static final String CREATE_PATIENT_ID_INDEX_CQL = String.format("CREATE INDEX %s"
            + " ON %s ( %s );", TABLE_NAME + "_patient_id", TABLE_NAME, Column.PATIENT_ID.name);

    private static final String CREATE_IPID_INDEX_CQL = String.format("CREATE INDEX %s"
            + " ON %s ( %s );", TABLE_NAME + "_ipid", TABLE_NAME, Column.ISSUER_OF_PATIENT_ID.name);

    private static final String INSERT_CQL = String.format("INSERT INTO %s (\n", TABLE_NAME)
            + String.format("  %s,\n", Column.PREFERRED_UUID.name)
            + String.format("  %s,\n", Column.OTHER_UUIDS.name)
            + String.format("  %s,\n", Column.PATIENT_NAME.name)
            + String.format("  %s,\n", Column.PATIENT_ID.name)
            + String.format("  %s,\n", Column.ISSUER_OF_PATIENT_ID.name)
            + String.format("  %s,\n", Column.PATIENT_BIRTH_DATE.name)
            + String.format("  %s\n", Column.PATIENT_SEX.name) + ") VALUES (\n"
            + CassandraService.generateValuePlaceholders(Column.values().length) + "\n)";

    private static final Query FIND_ALL_QUERY = QueryBuilder.select().all().from(TABLE_NAME);

    private static final String FIND_BY_PREFERRED_UUID_CQL = String.format("SELECT * FROM %s",
            TABLE_NAME) + String.format(" WHERE %s = ?", Column.PREFERRED_UUID.name);

    private static final String FIND_BY_PATIENT_ID_AND_ISSUER_CQL = String.format(
            "SELECT * FROM %s", TABLE_NAME)
            + String.format(" WHERE %s = ?", Column.PATIENT_ID.name)
            + String.format(" AND %s = ?", Column.ISSUER_OF_PATIENT_ID.name)
            + " LIMIT 1 ALLOW FILTERING";

    private static final String FIND_BY_PATIENT_ID_CQL = String.format("SELECT * FROM %s",
            TABLE_NAME) + String.format(" WHERE %s = ?", Column.PATIENT_ID.name);

    @Inject
    private Logger log;

    @Inject
    private CassandraService cassandra;

    private PreparedStatement insertQuery;

    private PreparedStatement findByPreferredUUIDQuery;

    private PreparedStatement findByPatientIDAndIssuerQuery;

    private PreparedStatement findByPatientIDQuery;

    @PostConstruct
    private void init() throws PersistenceException {

        cassandra.createTableIfNotExists(TABLE_NAME, CREATE_TABLE_CQL, CREATE_PATIENT_ID_INDEX_CQL,
                CREATE_IPID_INDEX_CQL);
        insertQuery = cassandra.getSession().prepare(INSERT_CQL);
        findByPreferredUUIDQuery = cassandra.getSession().prepare(FIND_BY_PREFERRED_UUID_CQL);
        findByPatientIDAndIssuerQuery = cassandra.getSession().prepare(
                FIND_BY_PATIENT_ID_AND_ISSUER_CQL);
        findByPatientIDQuery = cassandra.getSession().prepare(FIND_BY_PATIENT_ID_CQL);
    }

    public void save(PatientInfo patientInfo) throws PersistenceException {

        BoundStatement boundInsertQuery = new BoundStatement(insertQuery);
        boundInsertQuery.bind(patientInfo.getPreferredUUID(), patientInfo.getOtherUUIDs(),
                patientInfo.getPatientName(), patientInfo.getPatientID(),
                patientInfo.getIssuerOfPatientID(), patientInfo.getPatientBirthDate(),
                patientInfo.getPatientSex());
        try {
            cassandra.getSession().execute(boundInsertQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(String.format("Failed to create/update database entry"
                    + " for patient %s (name=%s)", patientInfo.getPreferredUUID(),
                    patientInfo.getPatientName()), e);
        }
        log.infof("Created/updated database entry for patient %s (name=%s)",
                patientInfo.getPreferredUUID(), patientInfo.getPatientName());
    }

    private static PatientInfo transformResult(Row row) {

        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setPreferredUUID(row.getUUID(Column.PREFERRED_UUID.name));
        patientInfo.setOtherUUIDs(row.getSet(Column.OTHER_UUIDS.name, UUID.class));
        patientInfo.setPatientName(row.getString(Column.PATIENT_NAME.name));
        patientInfo.setPatientID(row.getString(Column.PATIENT_ID.name));
        patientInfo.setIssuerOfPatientID(row.getString(Column.ISSUER_OF_PATIENT_ID.name));
        patientInfo.setPatientBirthDate(row.getDate(Column.PATIENT_BIRTH_DATE.name));
        patientInfo.setPatientSex(row.getString(Column.PATIENT_SEX.name));
        return patientInfo;
    }

    public List<PatientInfo> findAll() throws PersistenceException {

        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(FIND_ALL_QUERY);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<PatientInfo> patientInfos = new ArrayList<PatientInfo>();
        for (Row result : resultSet) {
            PatientInfo patientInfo = transformResult(result);
            patientInfos.add(patientInfo);
        }
        return patientInfos;
    }

    public PatientInfo findByPreferredUUID(UUID preferredUUID) throws PersistenceException {

        BoundStatement boundFindByPreferredUUIDQuery = new BoundStatement(findByPreferredUUIDQuery);
        boundFindByPreferredUUIDQuery.bind(preferredUUID);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindByPreferredUUIDQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        Row result = resultSet.one();
        return result == null ? null : transformResult(result);
    }

    public PatientInfo findByPatientIDAndIssuer(String patientID, String issuerOfPatientID)
            throws PersistenceException {

        BoundStatement boundFindByPatientIDAndIssuerQuery = new BoundStatement(
                findByPatientIDAndIssuerQuery);
        boundFindByPatientIDAndIssuerQuery.bind(patientID, issuerOfPatientID);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindByPatientIDAndIssuerQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        Row result = resultSet.one();
        return result == null ? null : transformResult(result);
    }

    public List<PatientInfo> findByPatientID(String patientID) throws PersistenceException {

        BoundStatement boundFindByPatientIDQuery = new BoundStatement(findByPatientIDQuery);
        boundFindByPatientIDQuery.bind(patientID);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindByPatientIDQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<PatientInfo> patientInfos = new ArrayList<PatientInfo>();
        for (Row result : resultSet) {
            PatientInfo patientInfo = transformResult(result);
            patientInfos.add(patientInfo);
        }
        return patientInfos;
    }
}
