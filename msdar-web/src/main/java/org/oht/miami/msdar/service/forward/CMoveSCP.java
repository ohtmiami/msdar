/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/gunterze/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * See @authors listed below
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdar.service.forward;

import java.util.List;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.net.pdu.PresentationContext;
import org.oht.miami.dcm4che.net.Association;
import org.oht.miami.dcm4che.net.Connection;
import org.oht.miami.dcm4che.net.Status;
import org.oht.miami.dcm4che.net.service.BasicCMoveSCP;
import org.oht.miami.dcm4che.net.service.BasicRetrieveTask;
import org.oht.miami.dcm4che.net.service.DicomServiceException;
import org.oht.miami.dcm4che.net.service.InstanceLocator;
import org.oht.miami.dcm4che.net.service.QueryRetrieveLevel;
import org.oht.miami.dcm4che.net.service.RetrieveTask;
import org.oht.miami.msdar.model.AEInfo;

class CMoveSCP extends BasicCMoveSCP {

    private final DicomForwardService forwarder;

    private final String[] qrLevels;

    CMoveSCP(DicomForwardService forwarder, String sopClass, String... qrLevels) {

        this.forwarder = forwarder;
        this.setSOPClasses(sopClass);
        this.qrLevels = qrLevels;
    }

    @Override
    protected RetrieveTask calculateMatches(Association association, PresentationContext pc,
            DicomObject rq, DicomObject keys) throws DicomServiceException {

        QueryRetrieveLevel level = QueryRetrieveLevel.valueOf(keys, qrLevels);
        if (level != QueryRetrieveLevel.STUDY) {
            // TODO Must support all Q/R Levels
            throw new DicomServiceException(Status.UnableToProcess, "Q/R Level " + level
                    + " is not yet supported");
        }
        final String destinationAETitle = rq.getString(Tag.MoveDestination);
        AEInfo aeInfo = forwarder.findDestinationAE(destinationAETitle);
        final Connection remoteConnection = new Connection(aeInfo.getHostname(), aeInfo.getPort());
        List<InstanceLocator> instances = forwarder.calculateMatches(keys);
        BasicRetrieveTask retrieveTask = new BasicRetrieveTask(BasicRetrieveTask.Service.C_MOVE,
                association, pc, rq, instances) {

            @Override
            protected Association getStoreAssociation() throws DicomServiceException {

                try {
                    // TODO What if destination AE is one of our local AEs?
                    return association.getApplicationEntity().connect(
                            association.getLocalConnection(), remoteConnection,
                            this.makeAAssociateRQ());
                } catch (Exception e) {
                    forwarder.log.errorf(e, "Failed to establish a DICOM Association to %s",
                            destinationAETitle);
                    throw new DicomServiceException(Status.UnableToPerformSubOperations, e);
                }
            }
        };
        // TODO setSendPendingRSPInterval()
        return retrieveTask;
    }
}
