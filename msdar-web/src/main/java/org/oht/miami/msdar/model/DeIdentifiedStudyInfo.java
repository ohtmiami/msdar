package org.oht.miami.msdar.model;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import org.oht.miami.msdar.model.StudyInfo.LastAccessType;
import org.oht.miami.msdtk.studymodel.Study;

public class DeIdentifiedStudyInfo {

    private String studyInstanceUID;

    private String originalStudyInstanceUID;

    private Set<String> modalities;

    private Set<String> sopClasses;

    private Integer numSeries;

    private Integer numInstances;

    private Integer numFrames;

    private UUID versionUUID;

    private UUID originalVersionUUID;

    private Set<UUID> bulkDataUUIDs;

    private LastAccessType lastAccessType;

    private Date lastAccessDate;

    public String getStudyInstanceUID() {

        return studyInstanceUID;
    }

    public void setStudyInstanceUID(String studyInstanceUID) {

        this.studyInstanceUID = studyInstanceUID;
    }

    public String getOriginalStudyInstanceUID() {

        return originalStudyInstanceUID;
    }

    public void setOriginalStudyInstanceUID(String originalStudyInstanceUID) {

        this.originalStudyInstanceUID = originalStudyInstanceUID;
    }

    public Set<String> getModalities() {

        return modalities;
    }

    public void setModalities(Set<String> modalities) {

        this.modalities = modalities;
    }

    public Set<String> getSOPClasses() {

        return sopClasses;
    }

    public void setSOPClasses(Set<String> sopClasses) {

        this.sopClasses = sopClasses;
    }

    public Integer getNumSeries() {

        return numSeries;
    }

    public void setNumSeries(Integer numSeries) {

        this.numSeries = numSeries;
    }

    public Integer getNumInstances() {

        return numInstances;
    }

    public void setNumInstances(Integer numInstances) {

        this.numInstances = numInstances;
    }

    public Integer getNumFrames() {

        return numFrames;
    }

    public void setNumFrames(Integer numFrames) {

        this.numFrames = numFrames;
    }

    public UUID getVersionUUID() {

        return versionUUID;
    }

    public void setVersionUUID(UUID versionUUID) {

        this.versionUUID = versionUUID;
    }

    public UUID getOriginalVersionUUID() {

        return originalVersionUUID;
    }

    public void setOriginalVersionUUID(UUID originalVersionUUID) {

        this.originalVersionUUID = originalVersionUUID;
    }

    public Set<UUID> getBulkDataUUIDs() {

        return bulkDataUUIDs;
    }

    public void setBulkDataUUIDs(Set<UUID> bulkDataUUIDs) {

        this.bulkDataUUIDs = bulkDataUUIDs;
    }

    public LastAccessType getLastAccessType() {

        return lastAccessType;
    }

    public void setLastAccessType(LastAccessType lastAccessType) {

        this.lastAccessType = lastAccessType;
    }

    public Date getLastAccessDate() {

        return lastAccessDate;
    }

    public void setLastAccessDate(Date lastAccessDate) {

        this.lastAccessDate = lastAccessDate;
    }

    public void populateWith(Study deIdentifiedStudy) {

        studyInstanceUID = deIdentifiedStudy.getStudyInstanceUIDAsString();
        versionUUID = deIdentifiedStudy.getVersionUUID();
        bulkDataUUIDs = deIdentifiedStudy.getBulkDataUUIDs();
        // TODO Modalities, SOP Classes
        numSeries = deIdentifiedStudy.seriesCount();
        numInstances = deIdentifiedStudy.instanceCount();
        numFrames = deIdentifiedStudy.frameCount();
    }
}
