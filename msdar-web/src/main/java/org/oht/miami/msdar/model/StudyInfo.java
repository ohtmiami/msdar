package org.oht.miami.msdar.model;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.oht.miami.msdtk.studymodel.Study;

public class StudyInfo {

    private String studyInstanceUID;

    private Date studyDate;

    private String accessionNumber;

    private String issuerOfAccessionNumber;

    private Set<String> modalities;

    private Set<String> sopClasses;

    private String referringPhysicianName;

    private String studyDescription;

    private String physiciansOfRecord;

    private String studyID;

    private Integer numSeries;

    private Integer numInstances;

    private Integer numFrames;

    private UUID patientUUID;

    private UUID versionUUID;

    private Set<UUID> bulkDataUUIDs;

    private LastAccessType lastAccessType;

    private Date lastAccessDate;

    public String getStudyInstanceUID() {

        return studyInstanceUID;
    }

    public void setStudyInstanceUID(String studyInstanceUID) {

        this.studyInstanceUID = studyInstanceUID;
    }

    public Date getStudyDate() {

        return studyDate;
    }

    public void setStudyDate(Date studyDate) {

        this.studyDate = studyDate;
    }

    public String getAccessionNumber() {

        return accessionNumber;
    }

    public void setAccessionNumber(String accessionNumber) {

        this.accessionNumber = accessionNumber;
    }

    public String getIssuerOfAccessionNumber() {

        return issuerOfAccessionNumber;
    }

    public void setIssuerOfAccessionNumber(String issuerOfAccessionNumber) {

        this.issuerOfAccessionNumber = issuerOfAccessionNumber;
    }

    public Set<String> getModalities() {

        return modalities;
    }

    public void setModalities(Set<String> modalities) {

        this.modalities = modalities;
    }

    public Set<String> getSOPClasses() {

        return sopClasses;
    }

    public void setSOPClasses(Set<String> sopClasses) {

        this.sopClasses = sopClasses;
    }

    public String getReferringPhysicianName() {

        return referringPhysicianName;
    }

    public void setReferringPhysicianName(String referringPhysicianName) {

        this.referringPhysicianName = referringPhysicianName;
    }

    public String getStudyDescription() {

        return studyDescription;
    }

    public void setStudyDescription(String studyDescription) {

        this.studyDescription = studyDescription;
    }

    public String getPhysiciansOfRecord() {

        return physiciansOfRecord;
    }

    public void setPhysiciansOfRecord(String physiciansOfRecord) {

        this.physiciansOfRecord = physiciansOfRecord;
    }

    public String getStudyID() {

        return studyID;
    }

    public void setStudyID(String studyID) {

        this.studyID = studyID;
    }

    public Integer getNumSeries() {

        return numSeries;
    }

    public void setNumSeries(Integer numSeries) {

        this.numSeries = numSeries;
    }

    public Integer getNumInstances() {

        return numInstances;
    }

    public void setNumInstances(Integer numInstances) {

        this.numInstances = numInstances;
    }

    public Integer getNumFrames() {

        return numFrames;
    }

    public void setNumFrames(Integer numFrames) {

        this.numFrames = numFrames;
    }

    public UUID getPatientUUID() {

        return patientUUID;
    }

    public void setPatientUUID(UUID patientUUID) {

        this.patientUUID = patientUUID;
    }

    public UUID getVersionUUID() {

        return versionUUID;
    }

    public void setVersionUUID(UUID versionUUID) {

        this.versionUUID = versionUUID;
    }

    public Set<UUID> getBulkDataUUIDs() {

        return bulkDataUUIDs;
    }

    public void setBulkDataUUIDs(Set<UUID> bulkDataUUIDs) {

        this.bulkDataUUIDs = bulkDataUUIDs;
    }

    public LastAccessType getLastAccessType() {

        return lastAccessType;
    }

    public void setLastAccessType(LastAccessType lastAccessType) {

        this.lastAccessType = lastAccessType;
    }

    public Date getLastAccessDate() {

        return lastAccessDate;
    }

    public void setLastAccessDate(Date lastAccessDate) {

        this.lastAccessDate = lastAccessDate;
    }

    public void populateWith(Study study) {

        studyInstanceUID = study.getStudyInstanceUIDAsString();
        versionUUID = study.getVersionUUID();
        bulkDataUUIDs = study.getBulkDataUUIDs();
        // TODO Modalities, SOP Classes
        numSeries = study.seriesCount();
        numInstances = study.instanceCount();
        numFrames = study.frameCount();

        DicomObject attributeSet = new BasicDicomObject();
        copyAttributeIfPresent(attributeSet, study, Tag.SpecificCharacterSet, VR.CS);
        copyAttributeIfPresent(attributeSet, study, Tag.StudyDate, VR.DA);
        copyAttributeIfPresent(attributeSet, study, Tag.StudyTime, VR.TM);
        copyAttributeIfPresent(attributeSet, study, Tag.AccessionNumber, VR.SH);
        // TODO Issuer of Accession Number
        copyAttributeIfPresent(attributeSet, study, Tag.ReferringPhysicianName, VR.PN);
        copyAttributeIfPresent(attributeSet, study, Tag.StudyDescription, VR.LO);
        copyAttributeIfPresent(attributeSet, study, Tag.PhysiciansOfRecord, VR.PN);
        copyAttributeIfPresent(attributeSet, study, Tag.StudyID, VR.SH);

        studyDate = attributeSet.getDate(Tag.StudyDate, Tag.StudyTime);
        accessionNumber = attributeSet.getString(Tag.AccessionNumber, "");
        // TODO Issuer of Accession Number
        issuerOfAccessionNumber = "";
        referringPhysicianName = attributeSet.getString(Tag.ReferringPhysicianName, "");
        studyDescription = attributeSet.getString(Tag.StudyDescription, "");
        physiciansOfRecord = attributeSet.getString(Tag.PhysiciansOfRecord, "");
        studyID = attributeSet.getString(Tag.StudyID, "");
    }

    public void populateQueryMatch(DicomObject match, DicomObject keys) {

        if (keys.contains(Tag.StudyDate)) {
            if (studyDate != null) {
                match.putDate(Tag.StudyDate, VR.DA, studyDate);
            } else {
                match.putNull(Tag.StudyDate, VR.DA);
            }
        }
        if (keys.contains(Tag.StudyTime)) {
            if (studyDate != null) {
                match.putDate(Tag.StudyTime, VR.TM, studyDate);
            } else {
                match.putNull(Tag.StudyTime, VR.TM);
            }
        }
        if (keys.contains(Tag.AccessionNumber)) {
            match.putString(Tag.AccessionNumber, VR.SH, accessionNumber);
        }
        // TODO Issuer of Accession Number
        if (keys.contains(Tag.ReferringPhysicianName)) {
            match.putString(Tag.ReferringPhysicianName, VR.PN, referringPhysicianName);
        }
        if (keys.contains(Tag.StudyDescription)) {
            match.putString(Tag.StudyDescription, VR.LO, studyDescription);
        }
        if (keys.contains(Tag.PhysiciansOfRecord)) {
            match.putString(Tag.PhysiciansOfRecord, VR.PN, physiciansOfRecord);
        }
        if (keys.contains(Tag.StudyID)) {
            match.putString(Tag.StudyID, VR.SH, studyID);
        }
        if (keys.contains(Tag.StudyInstanceUID)) {
            match.putString(Tag.StudyInstanceUID, VR.UI, studyInstanceUID);
        }
        if (keys.contains(Tag.NumberOfStudyRelatedSeries)) {
            match.putInt(Tag.NumberOfStudyRelatedSeries, VR.IS, numSeries);
        }
        if (keys.contains(Tag.NumberOfStudyRelatedInstances)) {
            match.putInt(Tag.NumberOfStudyRelatedInstances, VR.IS, numInstances);
        }
    }

    public static void copyAttributeIfPresent(DicomObject dest, Study src, int tag, VR vr) {

        byte[] value = src.getValueForAttribute(tag);
        if (value != null) {
            dest.putBytes(tag, vr, value);
        }
    }

    public static enum LastAccessType {

        READ, WRITE, SOFT_DELETE, HARD_DELETE

        // TODO PREFETCH, METADATA_FLUSH, BULK_DATA_FLUSH
    }
}
