package org.oht.miami.msdar.service.search;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.net.pdu.PresentationContext;
import org.oht.miami.dcm4che.net.Association;
import org.oht.miami.dcm4che.net.Status;
import org.oht.miami.dcm4che.net.service.BasicQueryTask;
import org.oht.miami.dcm4che.net.service.DicomServiceException;
import org.oht.miami.msdar.model.PatientInfo;
import org.oht.miami.msdar.model.StudyInfo;

class StudyQueryTask extends BasicQueryTask {

    private final DicomSearchService searcher;

    private final Iterator<StudyInfo> studyInfoIterator;

    StudyQueryTask(DicomSearchService searcher, Association association, PresentationContext pc,
            DicomObject rq, DicomObject keys) throws DicomServiceException {

        super(association, pc, rq, keys);
        this.searcher = searcher;
        try {
            List<StudyInfo> studyInfos = searcher.findStudies(keys);
            studyInfoIterator = studyInfos.iterator();
        } catch (Exception e) {
            searcher.log.errorf(e, "Failed to execute study query:\n%s", keys);
            throw this.wrapException(Status.UnableToProcess, e);
        }
    }

    @Override
    protected boolean hasMoreMatches() throws DicomServiceException {

        return studyInfoIterator.hasNext();
    }

    @Override
    protected DicomObject nextMatch() throws DicomServiceException {

        StudyInfo studyInfo = studyInfoIterator.next();
        DicomObject match = new BasicDicomObject();
        studyInfo.populateQueryMatch(match, this.keys);

        UUID patientUUID = studyInfo.getPatientUUID();
        PatientInfo patientInfo = searcher.findPatient(patientUUID);
        patientInfo.populateQueryMatch(match, this.keys);
        return match;
    }
}
