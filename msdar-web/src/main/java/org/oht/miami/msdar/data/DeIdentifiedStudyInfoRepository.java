package org.oht.miami.msdar.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.model.DeIdentifiedStudyInfo;
import org.oht.miami.msdar.model.StudyInfo;
import org.oht.miami.msdar.service.CassandraService;
import org.oht.miami.msdtk.studymodel.Study;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Query;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.querybuilder.QueryBuilder;

@ApplicationScoped
public class DeIdentifiedStudyInfoRepository {

    private static final String TABLE_NAME = "deidentifiedstudyinfo";

    private static enum Column {

        STUDY_INSTANCE_UID("study_instance_uid", DataType.text()), ORIGINAL_STUDY_INSTANCE_UID(
                "original_study_instance_uid", DataType.text()), MODALITIES("modalities", DataType
                .set(DataType.text())), SOP_CLASSES("sop_classes", DataType.set(DataType.text())), NUM_SERIES(
                "num_series", DataType.cint()), NUM_INSTANCES("num_instances", DataType.cint()), NUM_FRAMES(
                "num_frames", DataType.cint()), VERSION_UUID("version_uuid", DataType.uuid()), ORIGINAL_VERSION_UUID(
                "original_version_uuid", DataType.uuid()), BULK_DATA_UUIDS("bulk_data_uuids",
                DataType.set(DataType.uuid())), LAST_ACCESS_TYPE("last_access_type", DataType
                .text()), LAST_ACCESS_DATE("last_access_date", DataType.timestamp());

        private String name;

        private DataType dataType;

        private Column(String name, DataType dataType) {

            this.name = name;
            this.dataType = dataType;
        }

        @Override
        public String toString() {

            return String.format("%s %s", name, dataType);
        }
    }

    private static final String CREATE_TABLE_CQL = String.format("CREATE TABLE %s (\n", TABLE_NAME)
            + String.format("  %s PRIMARY KEY,\n", Column.STUDY_INSTANCE_UID)
            + String.format("  %s,\n", Column.ORIGINAL_STUDY_INSTANCE_UID)
            + String.format("  %s,\n", Column.MODALITIES)
            + String.format("  %s,\n", Column.SOP_CLASSES)
            + String.format("  %s,\n", Column.NUM_SERIES)
            + String.format("  %s,\n", Column.NUM_INSTANCES)
            + String.format("  %s,\n", Column.NUM_FRAMES)
            + String.format("  %s,\n", Column.VERSION_UUID)
            + String.format("  %s,\n", Column.ORIGINAL_VERSION_UUID)
            + String.format("  %s,\n", Column.BULK_DATA_UUIDS)
            + String.format("  %s,\n", Column.LAST_ACCESS_TYPE)
            + String.format("  %s\n", Column.LAST_ACCESS_DATE) + ")";

    private static final String INSERT_CQL = String.format("INSERT INTO %s (\n", TABLE_NAME)
            + String.format("  %s,\n", Column.STUDY_INSTANCE_UID.name)
            + String.format("  %s,\n", Column.ORIGINAL_STUDY_INSTANCE_UID.name)
            + String.format("  %s,\n", Column.MODALITIES.name)
            + String.format("  %s,\n", Column.SOP_CLASSES.name)
            + String.format("  %s,\n", Column.NUM_SERIES.name)
            + String.format("  %s,\n", Column.NUM_INSTANCES.name)
            + String.format("  %s,\n", Column.NUM_FRAMES.name)
            + String.format("  %s,\n", Column.VERSION_UUID.name)
            + String.format("  %s,\n", Column.ORIGINAL_VERSION_UUID.name)
            + String.format("  %s,\n", Column.BULK_DATA_UUIDS.name)
            + String.format("  %s,\n", Column.LAST_ACCESS_TYPE.name)
            + String.format("  %s\n", Column.LAST_ACCESS_DATE.name) + ") VALUES (\n"
            + CassandraService.generateValuePlaceholders(Column.values().length) + "\n)";

    private static final Query FIND_ALL_QUERY = QueryBuilder.select().all().from(TABLE_NAME);

    private static final String FIND_BY_STUDY_INSTANCE_UID_CQL = String.format("SELECT * FROM %s",
            TABLE_NAME) + String.format(" WHERE %s = ?", Column.STUDY_INSTANCE_UID.name);

    @Inject
    private Logger log;

    @Inject
    private CassandraService cassandra;

    private PreparedStatement insertQuery;

    private PreparedStatement findByStudyInstanceUIDQuery;

    @PostConstruct
    private void init() throws PersistenceException {

        cassandra.createTableIfNotExists(TABLE_NAME, CREATE_TABLE_CQL);
        insertQuery = cassandra.getSession().prepare(INSERT_CQL);
        findByStudyInstanceUIDQuery = cassandra.getSession()
                .prepare(FIND_BY_STUDY_INSTANCE_UID_CQL);
    }

    public void save(DeIdentifiedStudyInfo deIdentifiedStudyInfo) throws PersistenceException {

        BoundStatement boundInsertQuery = new BoundStatement(insertQuery);
        boundInsertQuery.bind(deIdentifiedStudyInfo.getStudyInstanceUID(), deIdentifiedStudyInfo
                .getOriginalStudyInstanceUID(), deIdentifiedStudyInfo.getModalities(),
                deIdentifiedStudyInfo.getSOPClasses(), deIdentifiedStudyInfo.getNumSeries(),
                deIdentifiedStudyInfo.getNumInstances(), deIdentifiedStudyInfo.getNumFrames(),
                deIdentifiedStudyInfo.getVersionUUID(), deIdentifiedStudyInfo
                        .getOriginalVersionUUID(), deIdentifiedStudyInfo.getBulkDataUUIDs(),
                deIdentifiedStudyInfo.getLastAccessType().toString(), deIdentifiedStudyInfo
                        .getLastAccessDate());
        try {
            cassandra.getSession().execute(boundInsertQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(String.format("Failed to create/update database entry"
                    + " for de-identified study %s", deIdentifiedStudyInfo.getStudyInstanceUID()),
                    e);
        }
        log.infof("Created/updated database entry for de-identified study %s",
                deIdentifiedStudyInfo.getStudyInstanceUID());
    }

    public void save(Study deIdentifiedStudy, Study originalStudy) throws PersistenceException {

        DeIdentifiedStudyInfo deIdentifiedStudyInfo = new DeIdentifiedStudyInfo();
        deIdentifiedStudyInfo.populateWith(deIdentifiedStudy);
        deIdentifiedStudyInfo.setOriginalStudyInstanceUID(originalStudy
                .getStudyInstanceUIDAsString());
        deIdentifiedStudyInfo.setOriginalVersionUUID(originalStudy.getVersionUUID());
        deIdentifiedStudyInfo.setLastAccessType(StudyInfo.LastAccessType.WRITE);
        deIdentifiedStudyInfo.setLastAccessDate(new Date());
        save(deIdentifiedStudyInfo);
    }

    private static DeIdentifiedStudyInfo transformResult(Row row) {

        DeIdentifiedStudyInfo deIdentifiedStudyInfo = new DeIdentifiedStudyInfo();
        deIdentifiedStudyInfo.setStudyInstanceUID(row.getString(Column.STUDY_INSTANCE_UID.name));
        deIdentifiedStudyInfo.setOriginalStudyInstanceUID(row
                .getString(Column.ORIGINAL_STUDY_INSTANCE_UID.name));
        deIdentifiedStudyInfo.setModalities(row.getSet(Column.MODALITIES.name, String.class));
        deIdentifiedStudyInfo.setSOPClasses(row.getSet(Column.SOP_CLASSES.name, String.class));
        deIdentifiedStudyInfo.setNumSeries(row.getInt(Column.NUM_SERIES.name));
        deIdentifiedStudyInfo.setNumInstances(row.getInt(Column.NUM_INSTANCES.name));
        deIdentifiedStudyInfo.setNumFrames(row.getInt(Column.NUM_FRAMES.name));
        deIdentifiedStudyInfo.setVersionUUID(row.getUUID(Column.VERSION_UUID.name));
        deIdentifiedStudyInfo
                .setOriginalVersionUUID(row.getUUID(Column.ORIGINAL_VERSION_UUID.name));
        deIdentifiedStudyInfo.setBulkDataUUIDs(row.getSet(Column.BULK_DATA_UUIDS.name, UUID.class));
        deIdentifiedStudyInfo.setLastAccessType(StudyInfo.LastAccessType.valueOf(row
                .getString(Column.LAST_ACCESS_TYPE.name)));
        deIdentifiedStudyInfo.setLastAccessDate(row.getDate(Column.LAST_ACCESS_DATE.name));
        return deIdentifiedStudyInfo;
    }

    public List<DeIdentifiedStudyInfo> findAll() throws PersistenceException {

        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(FIND_ALL_QUERY);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<DeIdentifiedStudyInfo> deIdentifiedStudyInfos = new ArrayList<DeIdentifiedStudyInfo>();
        for (Row result : resultSet) {
            DeIdentifiedStudyInfo deIdentifiedStudyInfo = transformResult(result);
            deIdentifiedStudyInfos.add(deIdentifiedStudyInfo);
        }
        return deIdentifiedStudyInfos;
    }

    public DeIdentifiedStudyInfo findByStudyInstanceUID(String studyInstanceUID)
            throws PersistenceException {

        BoundStatement boundFindByStudyInstanceUIDQuery = new BoundStatement(
                findByStudyInstanceUIDQuery);
        boundFindByStudyInstanceUIDQuery.bind(studyInstanceUID);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindByStudyInstanceUIDQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        Row result = resultSet.one();
        return result == null ? null : transformResult(result);
    }

    public List<DeIdentifiedStudyInfo> findByStudyInstanceUIDs(String[] studyInstanceUIDs)
            throws PersistenceException {

        Query findByStudyInstanceUIDsQuery = QueryBuilder
                .select()
                .all()
                .from(TABLE_NAME)
                .where(QueryBuilder
                        .in(Column.STUDY_INSTANCE_UID.name, (Object[]) studyInstanceUIDs));
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(findByStudyInstanceUIDsQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<DeIdentifiedStudyInfo> deIdentifiedStudyInfos = new ArrayList<DeIdentifiedStudyInfo>();
        for (Row result : resultSet) {
            DeIdentifiedStudyInfo deIdentifiedStudyInfo = transformResult(result);
            deIdentifiedStudyInfos.add(deIdentifiedStudyInfo);
        }
        return deIdentifiedStudyInfos;
    }
}
