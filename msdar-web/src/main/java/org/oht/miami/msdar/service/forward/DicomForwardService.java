package org.oht.miami.msdar.service.forward;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.jboss.logging.Logger;
import org.oht.miami.msdar.data.AEInfoRepository;
import org.oht.miami.msdar.data.DeIdentifiedStudyInfoRepository;
import org.oht.miami.msdar.data.StudyInfoRepository;
import org.oht.miami.dcm4che.net.Status;
import org.oht.miami.dcm4che.net.service.BasicCGetSCP;
import org.oht.miami.dcm4che.net.service.BasicCMoveSCP;
import org.oht.miami.dcm4che.net.service.DicomServiceException;
import org.oht.miami.dcm4che.net.service.InstanceLocator;
import org.oht.miami.msdar.model.AEInfo;
import org.oht.miami.msdar.model.DeIdentifiedStudyInfo;
import org.oht.miami.msdar.model.StudyInfo;
import org.oht.miami.msdar.service.StudyStorageService;
import org.oht.miami.msdtk.store.StudyStore;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

@ApplicationScoped
public class DicomForwardService {

    @Inject
    Logger log;

    @Inject
    private StudyInfoRepository studyInfoRepository;

    @Inject
    private DeIdentifiedStudyInfoRepository deIdentifiedStudyInfoRepository;

    @Inject
    private AEInfoRepository aeInfoRepository;

    @Inject
    private StudyStorageService studyStorage;

    @Inject
    private StudyDenormalizationService studyDemormalizer;

    public BasicCGetSCP newCGetSCP(String sopClass, String... qrLevels) {

        return new CGetSCP(this, sopClass, qrLevels);
    }

    public BasicCMoveSCP newCMoveSCP(String sopClass, String... qrLevels) {

        return new CMoveSCP(this, sopClass, qrLevels);
    }

    List<InstanceLocator> calculateMatches(DicomObject keys) throws DicomServiceException {

        List<InstanceLocator> instances = new ArrayList<InstanceLocator>();
        boolean deIdentified = "YES".equalsIgnoreCase(keys.getString(Tag.PatientIdentityRemoved));
        String[] studyInstanceUIDs = keys.getStrings(Tag.StudyInstanceUID);
        try {
            if (deIdentified) {
                StudyStore deIdentifiedStudyStore = studyStorage.getDeIdentifiedStudyStore();
                List<DeIdentifiedStudyInfo> deIdentifiedStudyInfos = deIdentifiedStudyInfoRepository
                        .findByStudyInstanceUIDs(studyInstanceUIDs);
                for (DeIdentifiedStudyInfo deIdentifiedStudyInfo : deIdentifiedStudyInfos) {
                    Study deIdentifiedStudy = deIdentifiedStudyStore.readStudy(new DicomUID(
                            deIdentifiedStudyInfo.getStudyInstanceUID()), deIdentifiedStudyInfo
                            .getVersionUUID());
                    instances.addAll(studyDemormalizer.locateInstances(deIdentifiedStudy,
                            deIdentifiedStudyStore));
                }
            } else {
                StudyStore studyStore = studyStorage.getStudyStore();
                List<StudyInfo> studyInfos = studyInfoRepository
                        .findByStudyInstanceUIDs(studyInstanceUIDs);
                for (StudyInfo studyInfo : studyInfos) {
                    Study study = studyStore.readStudy(
                            new DicomUID(studyInfo.getStudyInstanceUID()),
                            studyInfo.getVersionUUID());
                    instances.addAll(studyDemormalizer.locateInstances(study, studyStore));
                }
            }
        } catch (Exception e) {
            throw new DicomServiceException(Status.UnableToCalculateNumberOfMatches, e);
        }
        return instances;
    }

    AEInfo findDestinationAE(String destinationAETitle) throws DicomServiceException {

        AEInfo aeInfo = aeInfoRepository.findByAETitle(destinationAETitle);
        if (aeInfo == null) {
            throw new DicomServiceException(Status.MoveDestinationUnknown, "Move Destination: "
                    + destinationAETitle + " unknown");
        }
        return aeInfo;
    }
}
