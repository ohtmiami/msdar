package org.oht.miami.msdar.service.ingest;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.data.StudyInfoRepository;
import org.oht.miami.msdar.model.StudyInfo;
import org.oht.miami.msdar.service.StudyStorageService;
import org.oht.miami.msdar.util.Cached;
import org.oht.miami.msdtk.store.StudyStore;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalListeners;
import com.google.common.cache.RemovalNotification;

@ApplicationScoped
public class ActiveStudiesService extends CacheLoader<DicomUID, Study> implements
        RemovalListener<DicomUID, Study> {

    @Inject
    private Logger log;

    @Inject
    private StudyInfoRepository studyInfoRepository;

    @Inject
    private StudyStorageService studyStorage;

    @Inject
    @Cached
    private ExecutorService threadPool;

    @Inject
    private ScheduledExecutorService scheduledThreadPool;

    @Inject
    private Event<Study> studyEventSource;

    private LoadingCache<DicomUID, Study> activeStudies;

    private long timeoutMillis = 30000;

    private long maintenanceInitialDelayMillis = 10000;

    private long maintenancePeriodMillis = 10000;

    @PostConstruct
    public void start() {

        // TODO Load configuration from file
        RemovalListener<DicomUID, Study> timeoutListener = RemovalListeners.asynchronous(this,
                threadPool);
        activeStudies = CacheBuilder.newBuilder()
                .expireAfterAccess(timeoutMillis, TimeUnit.MILLISECONDS)
                .removalListener(timeoutListener).build(this);
        scheduledThreadPool.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {

                activeStudies.cleanUp();
            }
        }, maintenanceInitialDelayMillis, maintenancePeriodMillis, TimeUnit.MILLISECONDS);
    }

    @PreDestroy
    public void stop() {

        // TODO Anything?
    }

    @Override
    public void onRemoval(RemovalNotification<DicomUID, Study> notification) {

        Study study = notification.getValue();
        log.debugf("Study %s is no longer active", study.getStudyInstanceUID());
        studyEventSource.fire(study);
    }

    @Override
    public Study load(DicomUID studyInstanceUID) throws Exception {

        StudyInfo studyInfo = studyInfoRepository.findByStudyInstanceUID(studyInstanceUID
                .toString());
        if (studyInfo == null) {
            return new Study(studyInstanceUID);
        }

        StudyStore studyStore = studyStorage.getStudyStore();
        return studyStore.readStudy(studyInstanceUID, studyInfo.getVersionUUID());
    }

    public Study getActiveStudy(DicomUID studyInstanceUID) throws Exception {

        try {
            return activeStudies.get(studyInstanceUID);
        } catch (ExecutionException e) {
            Throwable t = e.getCause();
            if (t instanceof Exception) {
                throw (Exception) t;
            } else {
                throw new RuntimeException("Unexpected error", t);
            }
        }
    }
}
