package org.oht.miami.msdar.service.search;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.net.pdu.PresentationContext;
import org.oht.miami.dcm4che.net.Association;
import org.oht.miami.dcm4che.net.Status;
import org.oht.miami.dcm4che.net.service.BasicQueryTask;
import org.oht.miami.dcm4che.net.service.DicomServiceException;
import org.oht.miami.msdar.model.PatientInfo;
import org.oht.miami.msdar.model.SeriesInfo;
import org.oht.miami.msdar.model.StudyInfo;

class SeriesQueryTask extends BasicQueryTask {

    private final PatientInfo patientInfo;

    private final StudyInfo studyInfo;

    private final Iterator<SeriesInfo> seriesInfoIterator;

    SeriesQueryTask(DicomSearchService searcher, Association association, PresentationContext pc,
            DicomObject rq, DicomObject keys) throws DicomServiceException {

        super(association, pc, rq, keys);

        String studyInstanceUID = keys.getString(Tag.StudyInstanceUID, "");
        if (studyInstanceUID.isEmpty()) {
            throw new DicomServiceException(Status.MissingAttribute,
                    "Missing (0020,000D) Study Instance UID");
        }
        try {
            studyInfo = searcher.findStudy(studyInstanceUID);
        } catch (Exception e) {
            searcher.log.errorf(e, "Failed to execute query for study %s", studyInstanceUID);
            throw this.wrapException(Status.UnableToProcess, e);
        }
        if (studyInfo == null) {
            patientInfo = null;
            seriesInfoIterator = null;
        } else {
            UUID patientUUID = studyInfo.getPatientUUID();
            try {
                patientInfo = searcher.findPatient(patientUUID);
            } catch (Exception e) {
                searcher.log.errorf(e, "Failed to execute query for patient %s", patientUUID);
                throw this.wrapException(Status.UnableToProcess, e);
            }

            try {
                List<SeriesInfo> seriesInfos = searcher.findSeries(keys);
                seriesInfoIterator = seriesInfos.iterator();
            } catch (Exception e) {
                searcher.log.errorf(e, "Failed to execute series query:\n%s", keys);
                throw this.wrapException(Status.UnableToProcess, e);
            }
        }
    }

    @Override
    protected boolean hasMoreMatches() throws DicomServiceException {

        return seriesInfoIterator != null && seriesInfoIterator.hasNext();
    }

    @Override
    protected DicomObject nextMatch() throws DicomServiceException {

        SeriesInfo seriesInfo = seriesInfoIterator.next();
        DicomObject match = new BasicDicomObject();
        seriesInfo.populateQueryMatch(match, this.keys);
        studyInfo.populateQueryMatch(match, this.keys);
        patientInfo.populateQueryMatch(match, this.keys);
        return match;
    }
}
