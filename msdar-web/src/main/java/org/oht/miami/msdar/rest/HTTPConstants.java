package org.oht.miami.msdar.rest;

import javax.ws.rs.core.MediaType;

public class HTTPConstants {

    public static final String UUID_HEADER = "UUID";

    public static final String APPLICATION_MSDICOM = "application/ms-dicom";

    public static final MediaType APPLICATION_MSDICOM_TYPE = new MediaType("application",
            "ms-dicom");

    public static final String MULTIPART_MINT = "multipart/mint";

    public static final MediaType MULTIPART_MINT_TYPE = new MediaType("multipart", "mint");
}
