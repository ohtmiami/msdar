package org.oht.miami.msdar.rest;

import javax.ws.rs.Produces;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.plugins.providers.multipart.MultipartWriter;

@Provider
@Produces(HTTPConstants.MULTIPART_MINT)
public class MINTMultipartWriter extends MultipartWriter {
}
