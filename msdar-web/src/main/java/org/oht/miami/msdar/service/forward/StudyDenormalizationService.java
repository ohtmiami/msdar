package org.oht.miami.msdar.service.forward;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.util.TagUtils;
import org.jboss.logging.Logger;
import org.oht.miami.dcm4che.net.DataSetWriter;
import org.oht.miami.dcm4che.net.PDataTFOutputStream;
import org.oht.miami.dcm4che.net.service.InstanceLocator;
import org.oht.miami.msdtk.store.StudyStore;
import org.oht.miami.msdtk.studymodel.BulkDataDicomElement;
import org.oht.miami.msdtk.studymodel.Instance;
import org.oht.miami.msdtk.studymodel.Series;
import org.oht.miami.msdtk.studymodel.Study;

@ApplicationScoped
public class StudyDenormalizationService {

    @Inject
    private Logger log;

    public List<InstanceLocator> locateInstances(Study study, StudyStore studyStore) {

        List<InstanceLocator> instances = new ArrayList<InstanceLocator>(study.instanceCount());
        String studyCUID = study.getValueForAttributeAsString(Tag.SOPClassUID);
        String studyTSUID = study.getValueForAttributeAsString(Tag.TransferSyntaxUID);
        for (Iterator<Series> seriesIterator = study.seriesIterator(); seriesIterator.hasNext();) {
            Series series = seriesIterator.next();
            String seriesCUID = studyCUID == null ? series
                    .getValueForAttributeAsString(Tag.SOPClassUID) : studyCUID;
            String seriesTSUID = studyTSUID == null ? series
                    .getValueForAttributeAsString(Tag.TransferSyntaxUID) : studyTSUID;
            for (Iterator<Instance> instanceIterator = series.instanceIterator(); instanceIterator
                    .hasNext();) {
                Instance instance = instanceIterator.next();
                String cuid = seriesCUID == null ? instance
                        .getValueForAttributeAsString(Tag.SOPClassUID) : seriesCUID;
                String iuid = instance.getSOPInstanceUID();
                String tsuid = seriesTSUID == null ? instance
                        .getValueForAttributeAsString(Tag.TransferSyntaxUID) : seriesTSUID;
                instances.add(new StudyInstanceLocator(cuid, iuid, tsuid, instance, studyStore));
            }
        }
        return instances;
    }

    static class StudyInstanceLocator extends InstanceLocator {

        private static final long serialVersionUID = 2281235003781508026L;

        private final Instance instance;

        private final StudyStore studyStore;

        StudyInstanceLocator(String cuid, String iuid, String tsuid, Instance instance,
                StudyStore studyStore) {

            super(cuid, iuid, tsuid);
            if (instance == null) {
                throw new NullPointerException("instance");
            }
            if (studyStore == null) {
                throw new NullPointerException("studyStore");
            }
            this.instance = instance;
            this.studyStore = studyStore;
        }

        @Override
        public DataSetWriter getDataSetWriter(String tsuid) throws IOException {

            return new InstanceDataSetWriter(instance, studyStore);
        }
    }

    static class InstanceDataSetWriter implements DataSetWriter {

        private final Instance instance;

        private final StudyStore studyStore;

        InstanceDataSetWriter(Instance instance, StudyStore studyStore) {

            if (instance == null) {
                throw new NullPointerException("instance");
            }
            if (studyStore == null) {
                throw new NullPointerException("studyStore");
            }
            this.instance = instance;
            this.studyStore = studyStore;
        }

        private static void addAllDataSetAttributes(DicomObject attributeSet,
                Iterator<DicomElement> attributeIterator) {

            for (; attributeIterator.hasNext();) {
                DicomElement attribute = attributeIterator.next();
                if (TagUtils.isFileMetaInfoElement(attribute.tag())) {
                    continue;
                }
                attributeSet.add(attribute);
            }
        }

        private DicomObject getAttributeSet() {

            Series series = instance.getSeriesParent();
            Study study = series.getStudyParent();
            DicomObject attributeSet = new BasicDicomObject(study.elementCount());
            addAllDataSetAttributes(attributeSet, study.attributeIterator());
            addAllDataSetAttributes(attributeSet, series.attributeIterator());
            addAllDataSetAttributes(attributeSet, instance.attributeIterator());
            if (instance.hasChildFrames()) {
                DicomElement perFrameSequence = attributeSet.putSequence(
                        Tag.PerFrameFunctionalGroupsSequence, instance.frameCount());
                for (Instance frame : instance.getChildrenFrames()) {
                    DicomObject frameItem = new BasicDicomObject();
                    addAllDataSetAttributes(frameItem, frame.attributeIterator());
                    perFrameSequence.addDicomObject(frameItem);
                }
            }
            return attributeSet;
        }

        @Override
        public void writeTo(PDataTFOutputStream pdOut, String tsuid) throws IOException {

            DicomOutputStream dicomOut = new DicomOutputStream(pdOut);
            dicomOut.setTransferSyntax(tsuid);

            DicomObject attributeSet = getAttributeSet();
            writeAttributesTo(pdOut, dicomOut, attributeSet.datasetIterator());
        }

        private void writeAttributesTo(PDataTFOutputStream pdOut, DicomOutputStream dicomOut,
                Iterator<DicomElement> attributeIterator) throws IOException {

            for (; attributeIterator.hasNext();) {
                DicomElement attribute = attributeIterator.next();
                writeAttributeTo(pdOut, dicomOut, attribute);
            }
        }

        private void writeAttributeTo(PDataTFOutputStream pdOut, DicomOutputStream dicomOut,
                DicomElement attribute) throws IOException {

            Study study = instance.getSeriesParent().getStudyParent();
            if (attribute.vr() == VR.BD) {
                // Bulk Data Attribute
                BulkDataDicomElement bulkDataAttribute = (BulkDataDicomElement) attribute;
                ByteBuffer valueBuffer = studyStore.getBulkDataValue(study,
                        bulkDataAttribute.getBulkDataValueID());
                VR vr = bulkDataAttribute.getBulkDataVR();
                int length = valueBuffer.remaining();
                dicomOut.writeHeader(attribute.tag(), vr, (length + 1) & ~1);
                // TODO How to handle big endian?
                pdOut.write(valueBuffer);
                if ((length & 1) != 0) {
                    pdOut.write(vr.padding());
                }
            } else {
                VR vr = attribute.vr();
                int length = attribute.length();
                dicomOut.writeHeader(attribute.tag(), vr, length);
                attribute.bigEndian(dicomOut.getTransferSyntax().bigEndian());
                if (attribute.hasFragments()) {
                    // Fragment Sequence
                    for (int i = 0, n = attribute.countItems(); i < n; i++) {
                        DicomObject item = attribute.getDicomObject(i);
                        DicomElement fragmentAttribute = item.get(Tag.Fragment);
                        int fragmentLength = 0;
                        if (fragmentAttribute.vr() == VR.BD) {
                            // Bulk Data Fragment
                            BulkDataDicomElement bulkDataAttribute = (BulkDataDicomElement) fragmentAttribute;
                            ByteBuffer fragmentBuffer = studyStore.getBulkDataValue(study,
                                    bulkDataAttribute.getBulkDataValueID());
                            fragmentLength = fragmentBuffer.remaining();
                            dicomOut.writeHeader(Tag.Item, null, (fragmentLength + 1) & ~1);
                            pdOut.write(fragmentBuffer);
                        } else {
                            // Simple Fragment
                            byte[] value = fragmentAttribute.getBytes();
                            fragmentLength = value.length;
                            dicomOut.writeHeader(Tag.Item, null, (fragmentLength + 1) & ~1);
                            pdOut.write(value);
                        }
                        if ((fragmentLength & 1) != 0) {
                            pdOut.write(0);
                        }
                    }
                    if (length != 0) {
                        dicomOut.writeHeader(Tag.SequenceDelimitationItem, null, 0);
                    }
                } else if (attribute.hasDicomObjects()) {
                    // Data Set Sequence
                    for (int i = 0, n = attribute.countItems(); i < n; i++) {
                        DicomObject item = attribute.getDicomObject(i);
                        int itemLength = item.isEmpty() ? 0 : -1;
                        dicomOut.writeHeader(Tag.Item, null, itemLength);
                        writeAttributesTo(pdOut, dicomOut, item.iterator());
                        dicomOut.writeHeader(Tag.ItemDelimitationItem, null, 0);
                    }
                    if (length != 0) {
                        dicomOut.writeHeader(Tag.SequenceDelimitationItem, null, 0);
                    }
                } else if (length > 0) {
                    // Simple Attribute
                    byte[] value = attribute.getBytes();
                    pdOut.write(value);
                    if ((value.length & 1) != 0) {
                        pdOut.write(vr.padding());
                    }
                }
            }
        }
    }
}
