package org.oht.miami.msdar.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.model.PatientInfo;
import org.oht.miami.msdar.model.SeriesInfo;
import org.oht.miami.msdar.model.StudyInfo;
import org.oht.miami.msdar.service.CassandraService;
import org.oht.miami.msdtk.studymodel.Series;
import org.oht.miami.msdtk.studymodel.Study;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Query;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.querybuilder.QueryBuilder;

@ApplicationScoped
public class StudyInfoRepository {

    private static final String TABLE_NAME = "studyinfo";

    private static enum Column {

        STUDY_INSTANCE_UID("study_instance_uid", DataType.text()), STUDY_DATE("study_date",
                DataType.timestamp()), ACCESSION_NUMBER("accession_number", DataType.text()), ISSUER_OF_ACCESSION_NUMBER(
                "issuer_of_accession_number", DataType.text()), MODALITIES("modalities", DataType
                .set(DataType.text())), SOP_CLASSES("sop_classes", DataType.set(DataType.text())), REFERRING_PHYSICIAN_NAME(
                "referring_physician_name", DataType.text()), STUDY_DESCRIPTION(
                "study_description", DataType.text()), PHYSICIANS_OF_RECORD("physicians_of_record",
                DataType.text()), STUDY_ID("study_id", DataType.text()), NUM_SERIES("num_series",
                DataType.cint()), NUM_INSTANCES("num_instances", DataType.cint()), NUM_FRAMES(
                "num_frames", DataType.cint()), PATIENT_UUID("patient_uuid", DataType.uuid()), VERSION_UUID(
                "version_uuid", DataType.uuid()), BULK_DATA_UUIDS("bulk_data_uuids", DataType
                .set(DataType.uuid())), LAST_ACCESS_TYPE("last_access_type", DataType.text()), LAST_ACCESS_DATE(
                "last_access_date", DataType.timestamp());

        private String name;

        private DataType dataType;

        private Column(String name, DataType dataType) {

            this.name = name;
            this.dataType = dataType;
        }

        @Override
        public String toString() {

            return String.format("%s %s", name, dataType);
        }
    }

    private static final String CREATE_TABLE_CQL = String.format("CREATE TABLE %s (\n", TABLE_NAME)
            + String.format("  %s PRIMARY KEY,\n", Column.STUDY_INSTANCE_UID)
            + String.format("  %s,\n", Column.STUDY_DATE)
            + String.format("  %s,\n", Column.ACCESSION_NUMBER)
            + String.format("  %s,\n", Column.ISSUER_OF_ACCESSION_NUMBER)
            + String.format("  %s,\n", Column.MODALITIES)
            + String.format("  %s,\n", Column.SOP_CLASSES)
            + String.format("  %s,\n", Column.REFERRING_PHYSICIAN_NAME)
            + String.format("  %s,\n", Column.STUDY_DESCRIPTION)
            + String.format("  %s,\n", Column.PHYSICIANS_OF_RECORD)
            + String.format("  %s,\n", Column.STUDY_ID)
            + String.format("  %s,\n", Column.NUM_SERIES)
            + String.format("  %s,\n", Column.NUM_INSTANCES)
            + String.format("  %s,\n", Column.NUM_FRAMES)
            + String.format("  %s,\n", Column.PATIENT_UUID)
            + String.format("  %s,\n", Column.VERSION_UUID)
            + String.format("  %s,\n", Column.BULK_DATA_UUIDS)
            + String.format("  %s,\n", Column.LAST_ACCESS_TYPE)
            + String.format("  %s\n", Column.LAST_ACCESS_DATE) + ")";

    private static final String CREATE_IAN_INDEX_CQL = String.format("CREATE INDEX %s"
            + " ON %s ( %s );", TABLE_NAME + "_ian", TABLE_NAME,
            Column.ISSUER_OF_ACCESSION_NUMBER.name);

    private static final String CREATE_PATIENT_UUID_INDEX_CQL = String.format("CREATE INDEX %s"
            + " ON %s ( %s );", TABLE_NAME + "_patient_uuid", TABLE_NAME, Column.PATIENT_UUID.name);

    private static final String INSERT_CQL = String.format("INSERT INTO %s (\n", TABLE_NAME)
            + String.format("  %s,\n", Column.STUDY_INSTANCE_UID.name)
            + String.format("  %s,\n", Column.STUDY_DATE.name)
            + String.format("  %s,\n", Column.ACCESSION_NUMBER.name)
            + String.format("  %s,\n", Column.ISSUER_OF_ACCESSION_NUMBER.name)
            + String.format("  %s,\n", Column.MODALITIES.name)
            + String.format("  %s,\n", Column.SOP_CLASSES.name)
            + String.format("  %s,\n", Column.REFERRING_PHYSICIAN_NAME.name)
            + String.format("  %s,\n", Column.STUDY_DESCRIPTION.name)
            + String.format("  %s,\n", Column.PHYSICIANS_OF_RECORD.name)
            + String.format("  %s,\n", Column.STUDY_ID.name)
            + String.format("  %s,\n", Column.NUM_SERIES.name)
            + String.format("  %s,\n", Column.NUM_INSTANCES.name)
            + String.format("  %s,\n", Column.NUM_FRAMES.name)
            + String.format("  %s,\n", Column.PATIENT_UUID.name)
            + String.format("  %s,\n", Column.VERSION_UUID.name)
            + String.format("  %s,\n", Column.BULK_DATA_UUIDS.name)
            + String.format("  %s,\n", Column.LAST_ACCESS_TYPE.name)
            + String.format("  %s\n", Column.LAST_ACCESS_DATE.name) + ") VALUES (\n"
            + CassandraService.generateValuePlaceholders(Column.values().length) + "\n)";

    private static final Query FIND_ALL_QUERY = QueryBuilder.select().all().from(TABLE_NAME);

    private static final String FIND_BY_STUDY_INSTANCE_UID_CQL = String.format("SELECT * FROM %s",
            TABLE_NAME) + String.format(" WHERE %s = ?", Column.STUDY_INSTANCE_UID.name);

    @Inject
    private Logger log;

    @Inject
    private CassandraService cassandra;

    @Inject
    private PatientInfoRepository patientInfoRepository;

    @Inject
    private SeriesInfoRepository seriesInfoRepository;

    private PreparedStatement insertQuery;

    private PreparedStatement findByStudyInstanceUIDQuery;

    @PostConstruct
    private void init() throws PersistenceException {

        cassandra.createTableIfNotExists(TABLE_NAME, CREATE_TABLE_CQL, CREATE_IAN_INDEX_CQL,
                CREATE_PATIENT_UUID_INDEX_CQL);
        insertQuery = cassandra.getSession().prepare(INSERT_CQL);
        findByStudyInstanceUIDQuery = cassandra.getSession()
                .prepare(FIND_BY_STUDY_INSTANCE_UID_CQL);
    }

    public void save(StudyInfo studyInfo) throws PersistenceException {

        BoundStatement boundInsertQuery = new BoundStatement(insertQuery);
        boundInsertQuery.bind(studyInfo.getStudyInstanceUID(), studyInfo.getStudyDate(),
                studyInfo.getAccessionNumber(), studyInfo.getIssuerOfAccessionNumber(),
                studyInfo.getModalities(), studyInfo.getSOPClasses(),
                studyInfo.getReferringPhysicianName(), studyInfo.getStudyDescription(),
                studyInfo.getPhysiciansOfRecord(), studyInfo.getStudyID(),
                studyInfo.getNumSeries(), studyInfo.getNumInstances(), studyInfo.getNumFrames(),
                studyInfo.getPatientUUID(), studyInfo.getVersionUUID(),
                studyInfo.getBulkDataUUIDs(), studyInfo.getLastAccessType().toString(),
                studyInfo.getLastAccessDate());
        try {
            cassandra.getSession().execute(boundInsertQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(String.format("Failed to create/update database entry"
                    + " for study %s", studyInfo.getStudyInstanceUID()), e);
        }
        log.infof("Created/updated database entry for study %s", studyInfo.getStudyInstanceUID());
    }

    public void save(Study study) throws PersistenceException {

        PatientInfo patientInfo = new PatientInfo();
        patientInfo.populateWith(study);
        PatientInfo existingPatientInfo = patientInfoRepository.findByPatientIDAndIssuer(
                patientInfo.getPatientID(), patientInfo.getIssuerOfPatientID());
        if (existingPatientInfo == null) {
            patientInfo.setPreferredUUID(UUID.randomUUID());
        } else {
            // TODO Merge
            patientInfo.setPreferredUUID(existingPatientInfo.getPreferredUUID());
        }
        patientInfoRepository.save(patientInfo);

        StudyInfo studyInfo = new StudyInfo();
        // TODO Check for an existing entry?
        studyInfo.populateWith(study);
        studyInfo.setPatientUUID(patientInfo.getPreferredUUID());
        studyInfo.setLastAccessType(StudyInfo.LastAccessType.WRITE);
        studyInfo.setLastAccessDate(new Date());
        save(studyInfo);

        for (Iterator<Series> seriesIterator = study.seriesIterator(); seriesIterator.hasNext();) {
            Series series = seriesIterator.next();
            SeriesInfo seriesInfo = new SeriesInfo();
            seriesInfo.populateWith(series);
            seriesInfoRepository.save(seriesInfo);
        }
    }

    private static StudyInfo transformResult(Row row) {

        StudyInfo studyInfo = new StudyInfo();
        studyInfo.setStudyInstanceUID(row.getString(Column.STUDY_INSTANCE_UID.name));
        studyInfo.setStudyDate(row.getDate(Column.STUDY_DATE.name));
        studyInfo.setAccessionNumber(row.getString(Column.ACCESSION_NUMBER.name));
        studyInfo.setIssuerOfAccessionNumber(row.getString(Column.ISSUER_OF_ACCESSION_NUMBER.name));
        studyInfo.setModalities(row.getSet(Column.MODALITIES.name, String.class));
        studyInfo.setSOPClasses(row.getSet(Column.SOP_CLASSES.name, String.class));
        studyInfo.setReferringPhysicianName(row.getString(Column.REFERRING_PHYSICIAN_NAME.name));
        studyInfo.setStudyDescription(row.getString(Column.STUDY_DESCRIPTION.name));
        studyInfo.setPhysiciansOfRecord(row.getString(Column.PHYSICIANS_OF_RECORD.name));
        studyInfo.setStudyID(row.getString(Column.STUDY_ID.name));
        studyInfo.setNumSeries(row.getInt(Column.NUM_SERIES.name));
        studyInfo.setNumInstances(row.getInt(Column.NUM_INSTANCES.name));
        studyInfo.setNumFrames(row.getInt(Column.NUM_FRAMES.name));
        studyInfo.setPatientUUID(row.getUUID(Column.PATIENT_UUID.name));
        studyInfo.setVersionUUID(row.getUUID(Column.VERSION_UUID.name));
        studyInfo.setBulkDataUUIDs(row.getSet(Column.BULK_DATA_UUIDS.name, UUID.class));
        studyInfo.setLastAccessType(StudyInfo.LastAccessType.valueOf(row
                .getString(Column.LAST_ACCESS_TYPE.name)));
        studyInfo.setLastAccessDate(row.getDate(Column.LAST_ACCESS_DATE.name));
        return studyInfo;
    }

    public List<StudyInfo> findAll() throws PersistenceException {

        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(FIND_ALL_QUERY);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<StudyInfo> studyInfos = new ArrayList<StudyInfo>();
        for (Row result : resultSet) {
            StudyInfo studyInfo = transformResult(result);
            studyInfos.add(studyInfo);
        }
        return studyInfos;
    }

    public StudyInfo findByStudyInstanceUID(String studyInstanceUID) throws PersistenceException {

        BoundStatement boundFindByStudyInstanceUIDQuery = new BoundStatement(
                findByStudyInstanceUIDQuery);
        boundFindByStudyInstanceUIDQuery.bind(studyInstanceUID);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindByStudyInstanceUIDQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        Row result = resultSet.one();
        return result == null ? null : transformResult(result);
    }

    public List<StudyInfo> findByStudyInstanceUIDs(String[] studyInstanceUIDs)
            throws PersistenceException {

        Query findByStudyInstanceUIDsQuery = QueryBuilder
                .select()
                .all()
                .from(TABLE_NAME)
                .where(QueryBuilder
                        .in(Column.STUDY_INSTANCE_UID.name, (Object[]) studyInstanceUIDs));
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(findByStudyInstanceUIDsQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<StudyInfo> studyInfos = new ArrayList<StudyInfo>();
        for (Row result : resultSet) {
            StudyInfo studyInfo = transformResult(result);
            studyInfos.add(studyInfo);
        }
        return studyInfos;
    }
}
