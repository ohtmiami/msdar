package org.oht.miami.msdar.data;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.model.AEInfo;
import org.oht.miami.msdar.service.CassandraService;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Query;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.querybuilder.QueryBuilder;

@ApplicationScoped
public class AEInfoRepository {

    private static final String TABLE_NAME = "aeinfo";

    private static enum Column {

        AE_TITLE("ae_title", DataType.text()), HOSTNAME("hostname", DataType.text()), PORT("port",
                DataType.cint()), LOCAL("local", DataType.cboolean());

        private String name;

        private DataType dataType;

        private Column(String name, DataType dataType) {

            this.name = name;
            this.dataType = dataType;
        }

        @Override
        public String toString() {

            return String.format("%s %s", name, dataType);
        }
    }

    private static final String CREATE_TABLE_CQL = String.format("CREATE TABLE %s (\n", TABLE_NAME)
            + String.format("  %s PRIMARY KEY,\n", Column.AE_TITLE)
            + String.format("  %s,\n", Column.HOSTNAME) + String.format("  %s,\n", Column.PORT)
            + String.format("  %s\n", Column.LOCAL) + ")";

    private static final String CREATE_LOCAL_INDEX_CQL = String.format("CREATE INDEX %s"
            + " ON %s ( %s );", TABLE_NAME + "_local", TABLE_NAME, Column.LOCAL.name);

    private static final String INSERT_CQL = String.format("INSERT INTO %s (\n", TABLE_NAME)
            + String.format("  %s,\n", Column.AE_TITLE.name)
            + String.format("  %s,\n", Column.HOSTNAME.name)
            + String.format("  %s,\n", Column.PORT.name)
            + String.format("  %s\n", Column.LOCAL.name) + ") VALUES (\n"
            + CassandraService.generateValuePlaceholders(Column.values().length) + "\n)";

    private static final Query FIND_ALL_QUERY = QueryBuilder.select().all().from(TABLE_NAME);

    private static final String FIND_BY_AE_TITLE_CQL = String
            .format("SELECT * FROM %s", TABLE_NAME)
            + String.format(" WHERE %s = ?", Column.AE_TITLE.name);

    private static final Query FIND_LOCAL_QUERY = QueryBuilder.select().all().from(TABLE_NAME)
            .where(QueryBuilder.eq(Column.LOCAL.name, true));

    private static final Query FIND_REMOTE_QUERY = QueryBuilder.select().all().from(TABLE_NAME)
            .where(QueryBuilder.eq(Column.LOCAL.name, false));

    @Inject
    private Logger log;

    @Inject
    private CassandraService cassandra;

    @Inject
    private Event<AEInfo> aeInfoEventSource;

    private PreparedStatement insertQuery;

    private PreparedStatement findByAETitleQuery;

    @PostConstruct
    private void init() throws PersistenceException {

        cassandra.createTableIfNotExists(TABLE_NAME, CREATE_TABLE_CQL, CREATE_LOCAL_INDEX_CQL);
        insertQuery = cassandra.getSession().prepare(INSERT_CQL);
        findByAETitleQuery = cassandra.getSession().prepare(FIND_BY_AE_TITLE_CQL);
    }

    public void save(AEInfo aeInfo) throws PersistenceException {

        BoundStatement boundInsertQuery = new BoundStatement(insertQuery);
        boundInsertQuery.bind(aeInfo.getAETitle(), aeInfo.getHostname(), aeInfo.getPort(),
                aeInfo.isLocal());
        try {
            cassandra.getSession().execute(boundInsertQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(String.format("Failed to create/update database entry"
                    + " for AE %s", aeInfo.getAETitle()), e);
        }
        log.infof("Created/updated database entry for AE %s", aeInfo.getAETitle());

        aeInfoEventSource.fire(aeInfo);
    }

    private static AEInfo transformResult(Row row) {

        AEInfo aeInfo = new AEInfo();
        aeInfo.setAETitle(row.getString(Column.AE_TITLE.name));
        aeInfo.setHostname(row.getString(Column.HOSTNAME.name));
        aeInfo.setPort(row.getInt(Column.PORT.name));
        aeInfo.setLocal(row.getBool(Column.LOCAL.name));
        return aeInfo;
    }

    public List<AEInfo> findAll() throws PersistenceException {

        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(FIND_ALL_QUERY);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<AEInfo> aeInfos = new ArrayList<AEInfo>();
        for (Row result : resultSet) {
            AEInfo aeInfo = transformResult(result);
            aeInfos.add(aeInfo);
        }
        return aeInfos;
    }

    public AEInfo findByAETitle(String aeTitle) throws PersistenceException {

        BoundStatement boundFindByAETitleQuery = new BoundStatement(findByAETitleQuery);
        boundFindByAETitleQuery.bind(aeTitle);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindByAETitleQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        Row result = resultSet.one();
        return result == null ? null : transformResult(result);
    }

    public List<AEInfo> findLocal() throws PersistenceException {

        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(FIND_LOCAL_QUERY);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<AEInfo> aeInfos = new ArrayList<AEInfo>();
        for (Row result : resultSet) {
            AEInfo aeInfo = transformResult(result);
            aeInfos.add(aeInfo);
        }
        return aeInfos;
    }

    public List<AEInfo> findRemote() throws PersistenceException {

        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(FIND_REMOTE_QUERY);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<AEInfo> aeInfos = new ArrayList<AEInfo>();
        for (Row result : resultSet) {
            AEInfo aeInfo = transformResult(result);
            aeInfos.add(aeInfo);
        }
        return aeInfos;
    }
}
