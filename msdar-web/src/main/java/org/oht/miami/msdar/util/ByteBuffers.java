package org.oht.miami.msdar.util;

import java.nio.ByteBuffer;

public class ByteBuffers {

    public static final byte[] EMPTY_BYTES = {};

    public static final ByteBuffer EMPTY_BYTE_BUFFER = ByteBuffer.wrap(EMPTY_BYTES);

    public static ByteBuffer wrapIfNull(ByteBuffer value) {

        return value == null ? EMPTY_BYTE_BUFFER : value;
    }
}
