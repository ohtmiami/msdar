package org.oht.miami.msdar.rest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartInput;
import org.oht.miami.msdar.data.StudyInfoRepository;
import org.oht.miami.msdar.model.StudyInfo;
import org.oht.miami.msdar.service.StudyStorageService;
import org.oht.miami.msdtk.crypto.StudyCipher;
import org.oht.miami.msdtk.crypto.StudyCryptoException;
import org.oht.miami.msdtk.crypto.StudyCryptoInfo;
import org.oht.miami.msdtk.io.MultiSeriesDicomReader;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

@Path("/studies")
@RequestScoped
public class StudyUploadRESTService {

    @Inject
    private Logger log;

    @Inject
    private StudyInfoRepository studyInfoRepository;

    @Inject
    private StudyStorageService studyStorage;

    private ByteBuf aad = Unpooled.buffer(StudyCipher.AAD_SIZE);

    private Study processMetadata(InputPart metadataPart) throws WebApplicationException {

        if (!HTTPConstants.APPLICATION_MSDICOM_TYPE.equals(metadataPart.getMediaType())) {
            log.errorf("Wrong media type %s for study metadata", metadataPart.getMediaType());
            throw new WebApplicationException(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        }

        try (InputStream versionIn = metadataPart.getBody(InputStream.class, null)) {
            aad.clear();
            if (aad.writeBytes(versionIn, StudyCipher.AAD_SIZE) != StudyCipher.AAD_SIZE) {
                log.error("Unexpected EOF in AAD of study metadata");
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }

            StudyCryptoInfo cryptoInfo = new StudyCryptoInfo();
            try {
                cryptoInfo.load(aad.duplicate());
            } catch (Exception e) {
                log.error("Failed to parse AAD of study metadata", e);
                throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
            }

            DicomUID studyInstanceUID = cryptoInfo.getStudyInstanceUID();
            UUID versionUUID = cryptoInfo.getUUID();
            StudyInfo existingStudyInfo = studyInfoRepository
                    .findByStudyInstanceUID(studyInstanceUID.toString());
            if (existingStudyInfo != null && versionUUID.equals(existingStudyInfo.getVersionUUID())) {
                log.errorf("Version %s of study %s already exists", versionUUID, studyInstanceUID);
                throw new WebApplicationException(Response.Status.CONFLICT);
            }

            // TODO How to get the compressed size?
            int encryptedSize = (int) (StudyCipher.AAD_SIZE + cryptoInfo.getLength());
            StudyCipher cipher = StudyCipher.getInstance();
            ByteBuf encrypted = cipher.allocateBufferForEncryptedData(encryptedSize);
            encrypted.writeBytes(aad);
            try {
                int bytesTransferred = 0;
                do {
                    bytesTransferred = encrypted.writeBytes(versionIn, encrypted.writableBytes());
                } while (bytesTransferred > 0);
            } catch (IOException e) {
                log.errorf(e, "Failed to read metadata of version %s of study %s", versionUUID,
                        studyInstanceUID);
                throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
            }

            ByteBuf decrypted = null;
            try {
                cipher.initDecrypt(cryptoInfo.getStudyInstanceUID());
                decrypted = cipher.doFinal(encrypted.duplicate());
            } catch (StudyCryptoException e) {
                log.errorf(e, "Failed to decrypt metadata of version %s of study %s", versionUUID,
                        studyInstanceUID);
                throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
            }

            Study study = null;
            try {
                study = new MultiSeriesDicomReader().readUnencryptedStudy(decrypted, versionUUID);
            } catch (IOException e) {
                log.errorf(e, "Failed to parse metadata of version %s of study %s", versionUUID,
                        studyInstanceUID);
                throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
            }

            java.nio.file.Path versionFile = studyStorage.getStudyStore().getVersionFile(
                    studyInstanceUID, versionUUID);
            try {
                Files.createDirectories(versionFile.getParent());
            } catch (IOException e) {
                log.errorf(e, "Failed to create directories for version %s of study %s",
                        versionUUID, studyInstanceUID);
                throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
            }

            try (FileChannel versionOut = FileChannel.open(versionFile,
                    StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)) {
                encrypted.readBytes(versionOut, encrypted.readableBytes());
            } catch (IOException e) {
                log.errorf("Failed to write version %s of study %s to %s", versionUUID,
                        studyInstanceUID, versionFile);
                throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
            }

            studyInfoRepository.save(study);
            return study;
        } catch (IOException e) {
            log.error("Failed to read study metadata part", e);
            throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
        }
    }

    private void processBulkData(InputPart bulkDataPart, Study study)
            throws WebApplicationException {

        DicomUID studyInstanceUID = study.getStudyInstanceUID();
        UUID versionUUID = study.getVersionUUID();

        if (!MediaType.APPLICATION_OCTET_STREAM_TYPE.equals(bulkDataPart.getMediaType())) {
            log.errorf("Wrong media type %s for bulk data of version %s of study %s",
                    bulkDataPart.getMediaType(), versionUUID, studyInstanceUID);
            throw new WebApplicationException(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        }

        UUID bulkDataUUID = null;
        String bulkDataUUIDValue = bulkDataPart.getHeaders().getFirst(HTTPConstants.UUID_HEADER);
        if (bulkDataUUIDValue == null) {
            log.errorf("Missing bulk data file of version %s of study %s", versionUUID,
                    studyInstanceUID);
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        try {
            bulkDataUUID = UUID.fromString(bulkDataUUIDValue);
        } catch (IllegalArgumentException e) {
            log.errorf("Invalid bulk data file %s of version %s of study %s", bulkDataUUIDValue,
                    versionUUID, studyInstanceUID);
            throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
        }

        if (!study.getBulkDataUUIDs().contains(bulkDataUUID)) {
            log.errorf("Unexpected bulk data file %s of version %s of study %s", bulkDataUUID,
                    versionUUID, studyInstanceUID);
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }

        java.nio.file.Path bulkDataFile = studyStorage.getStudyStore().getBulkDataFile(
                studyInstanceUID, bulkDataUUID);
        try {
            Files.createDirectories(bulkDataFile.getParent());
        } catch (IOException e) {
            log.errorf(e, "Failed to create directories for bulk data file %s"
                    + " of version %s of study %s", versionUUID, studyInstanceUID);
            throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
        }

        try (ReadableByteChannel bulkDataIn = Channels.newChannel(bulkDataPart.getBody(
                InputStream.class, null))) {
            try (FileChannel bulkDataOut = FileChannel.open(bulkDataFile,
                    StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)) {
                bulkDataOut.transferFrom(bulkDataIn, 0, Integer.MAX_VALUE);
            } catch (IOException e) {
                log.errorf(
                        "Failed to write bulk data file %s" + " of version %s of study %s to %s",
                        bulkDataUUID, versionUUID, studyInstanceUID, bulkDataFile);
                throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
            }
        } catch (IOException e) {
            log.errorf(e, "Failed to read bulk data file %s of version %s of study %s",
                    bulkDataUUID, versionUUID, studyInstanceUID);
        }
    }

    @POST
    @Consumes(HTTPConstants.MULTIPART_MINT)
    public Response uploadStudy(MultipartInput input) throws WebApplicationException {

        List<InputPart> parts = input.getParts();
        int numParts = parts.size();
        log.debugf("Create Study (#parts=%d)", numParts);

        try {
            if (numParts == 0) {
                log.error("No content in study upload request");
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }

            Iterator<InputPart> partIterator = parts.iterator();
            Study study = processMetadata(partIterator.next());

            int numBulkDataFiles = study.getBulkDataUUIDs().size();
            if (numBulkDataFiles != numParts - 1) {
                log.errorf("Unexpected number of bulk data parts %d" + ". Expected %d",
                        parts.size() - 1, numBulkDataFiles);
                throw new WebApplicationException(Response.Status.BAD_REQUEST);
            }

            while (partIterator.hasNext()) {
                processBulkData(partIterator.next(), study);
            }
        } finally {
            input.close();
        }
        return Response.status(Response.Status.CREATED).build();
    }
}
