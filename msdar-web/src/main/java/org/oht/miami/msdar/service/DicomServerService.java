package org.oht.miami.msdar.service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.dcm4che2.data.UID;
import org.jboss.logging.Logger;
import org.oht.miami.msdar.data.AEInfoRepository;
import org.oht.miami.dcm4che.net.Connection;
import org.oht.miami.dcm4che.net.Device;
import org.oht.miami.dcm4che.net.ApplicationEntity;
import org.oht.miami.dcm4che.net.TransferCapability;
import org.oht.miami.dcm4che.net.service.BasicCEchoSCP;
import org.oht.miami.dcm4che.net.service.DicomServiceRegistry;
import org.oht.miami.msdar.model.AEInfo;
import org.oht.miami.msdar.service.forward.DicomForwardService;
import org.oht.miami.msdar.service.ingest.DicomIngestService;
import org.oht.miami.msdar.service.search.DicomSearchService;
import org.oht.miami.msdar.util.Cached;

@Singleton
@Startup
public class DicomServerService {

    @Inject
    private Logger log;

    @Inject
    private AEInfoRepository aeInfoRepository;

    @Inject
    @Cached
    private ExecutorService threadPool;

    @Inject
    private ScheduledExecutorService scheduledThreadPool;

    @Inject
    private DicomIngestService ingester;

    @Inject
    private DicomSearchService searcher;

    @Inject
    private DicomForwardService forwarder;

    private final Device device = new Device("DicomServer");

    private DicomServiceRegistry createServiceRegistry() {

        final String[] patientRootQueryRetrieveLevels = { "PATIENT", "STUDY", "SERIES", "IMAGE" };
        final String[] studyRootQueryRetrieveLevels = { "STUDY", "SERIES", "IMAGE" };

        DicomServiceRegistry serviceRegistry = new DicomServiceRegistry();
        serviceRegistry.addDicomService(new BasicCEchoSCP());
        serviceRegistry.addDicomService(ingester);
        serviceRegistry.addDicomService(searcher.newCFindSCP(
                UID.PatientRootQueryRetrieveInformationModelFIND, patientRootQueryRetrieveLevels));
        serviceRegistry.addDicomService(searcher.newCFindSCP(
                UID.StudyRootQueryRetrieveInformationModelFIND, studyRootQueryRetrieveLevels));
        serviceRegistry.addDicomService(forwarder.newCGetSCP(
                UID.PatientRootQueryRetrieveInformationModelGET, patientRootQueryRetrieveLevels));
        serviceRegistry.addDicomService(forwarder.newCGetSCP(
                UID.StudyRootQueryRetrieveInformationModelGET, studyRootQueryRetrieveLevels));
        serviceRegistry.addDicomService(forwarder.newCMoveSCP(
                UID.PatientRootQueryRetrieveInformationModelMOVE, patientRootQueryRetrieveLevels));
        serviceRegistry.addDicomService(forwarder.newCMoveSCP(
                UID.StudyRootQueryRetrieveInformationModelMOVE, studyRootQueryRetrieveLevels));
        return serviceRegistry;
    }

    private ApplicationEntity addLocalAE(AEInfo localAEInfo) {

        ApplicationEntity ae = new ApplicationEntity(localAEInfo.getAETitle());
        ae.setAssociationAcceptor(true);
        ae.addTransferCapability(new TransferCapability("*", TransferCapability.Role.SCP, "*"));
        ae.addTransferCapability(new TransferCapability("*", TransferCapability.Role.SCU, "*"));

        Connection localConnection = new Connection();
        String hostname = localAEInfo.getHostname();
        // If and only if hostname is null, port will be bound to all local
        // interfaces
        localConnection.setHostname("".equals(hostname) ? null : hostname);
        localConnection.setPort(localAEInfo.getPort());

        device.addLocalConnection(localConnection);
        device.addApplicationEntity(ae);
        ae.addLocalConnection(localConnection);
        return ae;
    }

    @PostConstruct
    public void start() throws IOException {

        List<AEInfo> localAEInfos = aeInfoRepository.findLocal();
        if (localAEInfos.isEmpty()) {
            // TODO Load default AE configuration from file
            AEInfo aeInfo = new AEInfo();
            aeInfo.setAETitle("MSDAR_SCP");
            aeInfo.setHostname("");
            aeInfo.setPort(11122);
            aeInfo.setLocal(true);
            aeInfoRepository.save(aeInfo);
            localAEInfos.add(aeInfo);
        }
        for (AEInfo localAEInfo : localAEInfos) {
            log.infof("Found local AE %s", localAEInfo.getAETitle());
            addLocalAE(localAEInfo);
        }

        device.setDimseRQHandler(createServiceRegistry());
        device.setThreadPool(threadPool);
        device.setScheduledThreadPool(scheduledThreadPool);
        device.bindConnections();
        log.info("DICOM Server started");
    }

    @PreDestroy
    public void stop() {

        device.shutDown();
        log.info("DICOM Server terminated");
    }
}
