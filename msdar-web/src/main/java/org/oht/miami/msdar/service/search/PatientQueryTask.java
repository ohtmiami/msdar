package org.oht.miami.msdar.service.search;

import java.util.Iterator;
import java.util.List;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.net.pdu.PresentationContext;
import org.oht.miami.dcm4che.net.Association;
import org.oht.miami.dcm4che.net.Status;
import org.oht.miami.dcm4che.net.service.BasicQueryTask;
import org.oht.miami.dcm4che.net.service.DicomServiceException;
import org.oht.miami.msdar.model.PatientInfo;

class PatientQueryTask extends BasicQueryTask {

    private final Iterator<PatientInfo> patientInfoIterator;

    PatientQueryTask(DicomSearchService searcher, Association association, PresentationContext pc,
            DicomObject rq, DicomObject keys) throws DicomServiceException {

        super(association, pc, rq, keys);
        try {
            List<PatientInfo> patientInfos = searcher.findPatients(keys);
            patientInfoIterator = patientInfos.iterator();
        } catch (Exception e) {
            searcher.log.errorf(e, "Failed to execute patient query:\n%s", keys);
            throw this.wrapException(Status.UnableToProcess, e);
        }
    }

    @Override
    protected boolean hasMoreMatches() throws DicomServiceException {

        return patientInfoIterator.hasNext();
    }

    @Override
    protected DicomObject nextMatch() throws DicomServiceException {

        PatientInfo patientInfo = patientInfoIterator.next();
        DicomObject match = new BasicDicomObject();
        patientInfo.populateQueryMatch(match, this.keys);
        return match;
    }
}
