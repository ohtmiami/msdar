package org.oht.miami.msdar.model;

import java.util.Date;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.oht.miami.msdtk.studymodel.Series;

public class SeriesInfo {

    private String studyInstanceUID;

    private String seriesInstanceUID;

    private Date seriesDate;

    private String modality;

    private String bodyPartExamined;

    private String protocolName;

    private String seriesDescription;

    private String performingPhysicianName;

    private Integer seriesNumber;

    private String laterality;

    private Integer numInstances;

    private Integer numFrames;

    public String getStudyInstanceUID() {

        return studyInstanceUID;
    }

    public void setStudyInstanceUID(String studyInstanceUID) {

        this.studyInstanceUID = studyInstanceUID;
    }

    public String getSeriesInstanceUID() {

        return seriesInstanceUID;
    }

    public void setSeriesInstanceUID(String seriesInstanceUID) {

        this.seriesInstanceUID = seriesInstanceUID;
    }

    public Date getSeriesDate() {

        return seriesDate;
    }

    public void setSeriesDate(Date seriesDate) {

        this.seriesDate = seriesDate;
    }

    public String getModality() {

        return modality;
    }

    public void setModality(String modality) {

        this.modality = modality;
    }

    public String getBodyPartExamined() {

        return bodyPartExamined;
    }

    public void setBodyPartExamined(String bodyPartExamined) {

        this.bodyPartExamined = bodyPartExamined;
    }

    public String getProtocolName() {

        return protocolName;
    }

    public void setProtocolName(String protocolName) {

        this.protocolName = protocolName;
    }

    public String getSeriesDescription() {

        return seriesDescription;
    }

    public void setSeriesDescription(String seriesDescription) {

        this.seriesDescription = seriesDescription;
    }

    public String getPerformingPhysicianName() {

        return performingPhysicianName;
    }

    public void setPerformingPhysicianName(String performingPhysicianName) {

        this.performingPhysicianName = performingPhysicianName;
    }

    public Integer getSeriesNumber() {

        return seriesNumber;
    }

    public void setSeriesNumber(Integer seriesNumber) {

        this.seriesNumber = seriesNumber;
    }

    public String getLaterality() {

        return laterality;
    }

    public void setLaterality(String laterality) {

        this.laterality = laterality;
    }

    public Integer getNumInstances() {

        return numInstances;
    }

    public void setNumInstances(Integer numInstances) {

        this.numInstances = numInstances;
    }

    public Integer getNumFrames() {

        return numFrames;
    }

    public void setNumFrames(Integer numFrames) {

        this.numFrames = numFrames;
    }

    public void populateWith(Series series) {

        studyInstanceUID = series.getStudyParent().getStudyInstanceUIDAsString();
        seriesInstanceUID = series.getSeriesInstanceUID();
        numInstances = series.instanceCount();
        numFrames = series.frameCount();

        DicomObject attributeSet = new BasicDicomObject();
        copyAttributeIfPresent(attributeSet, series, Tag.SpecificCharacterSet, VR.CS);
        copyAttributeIfPresent(attributeSet, series, Tag.SeriesDate, VR.DA);
        copyAttributeIfPresent(attributeSet, series, Tag.SeriesTime, VR.TM);
        copyAttributeIfPresent(attributeSet, series, Tag.Modality, VR.CS);
        copyAttributeIfPresent(attributeSet, series, Tag.BodyPartExamined, VR.CS);
        copyAttributeIfPresent(attributeSet, series, Tag.ProtocolName, VR.LO);
        copyAttributeIfPresent(attributeSet, series, Tag.SeriesDescription, VR.LO);
        copyAttributeIfPresent(attributeSet, series, Tag.PerformingPhysicianName, VR.PN);
        copyAttributeIfPresent(attributeSet, series, Tag.SeriesNumber, VR.IS);
        copyAttributeIfPresent(attributeSet, series, Tag.Laterality, VR.CS);

        seriesDate = attributeSet.getDate(Tag.SeriesDate, Tag.SeriesTime);
        modality = attributeSet.getString(Tag.Modality, "");
        bodyPartExamined = attributeSet.getString(Tag.BodyPartExamined, "");
        protocolName = attributeSet.getString(Tag.ProtocolName, "");
        seriesDescription = attributeSet.getString(Tag.SeriesDescription, "");
        performingPhysicianName = attributeSet.getString(Tag.PerformingPhysicianName, "");
        seriesNumber = attributeSet.get(Tag.SeriesNumber) == null ? null : attributeSet
                .getInt(Tag.SeriesNumber);
        laterality = attributeSet.getString(Tag.Laterality, "");
    }

    public void populateQueryMatch(DicomObject match, DicomObject keys) {

        if (keys.contains(Tag.SeriesDate)) {
            if (seriesDate != null) {
                match.putDate(Tag.SeriesDate, VR.DA, seriesDate);
            } else {
                match.putNull(Tag.SeriesDate, VR.DA);
            }
        }
        if (keys.contains(Tag.SeriesTime)) {
            if (seriesDate != null) {
                match.putDate(Tag.SeriesTime, VR.TM, seriesDate);
            } else {
                match.putNull(Tag.SeriesTime, VR.TM);
            }
        }
        if (keys.contains(Tag.Modality)) {
            match.putString(Tag.Modality, VR.CS, modality);
        }
        if (keys.contains(Tag.BodyPartExamined)) {
            match.putString(Tag.BodyPartExamined, VR.CS, bodyPartExamined);
        }
        if (keys.contains(Tag.ProtocolName)) {
            match.putString(Tag.ProtocolName, VR.LO, protocolName);
        }
        if (keys.contains(Tag.SeriesDescription)) {
            match.putString(Tag.SeriesDescription, VR.LO, seriesDescription);
        }
        if (keys.contains(Tag.PerformingPhysicianName)) {
            match.putString(Tag.PerformingPhysicianName, VR.PN, performingPhysicianName);
        }
        if (keys.contains(Tag.SeriesInstanceUID)) {
            match.putString(Tag.SeriesInstanceUID, VR.UI, seriesInstanceUID);
        }
        if (keys.contains(Tag.SeriesNumber)) {
            match.putInt(Tag.SeriesNumber, VR.IS, seriesNumber);
        }
        if (keys.contains(Tag.Laterality)) {
            match.putString(Tag.Laterality, VR.CS, laterality);
        }
        if (keys.contains(Tag.NumberOfSeriesRelatedInstances)) {
            match.putInt(Tag.NumberOfSeriesRelatedInstances, VR.IS, numInstances);
        }
    }

    public static void copyAttributeIfPresent(DicomObject dest, Series src, int tag, VR vr) {

        byte[] value = src.getValueForAttribute(tag);
        if (value != null) {
            dest.putBytes(tag, vr, value);
        } else {
            StudyInfo.copyAttributeIfPresent(dest, src.getStudyParent(), tag, vr);
        }
    }
}
