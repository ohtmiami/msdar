package org.oht.miami.msdar.model;

public class AEInfo {

    private String aeTitle;

    private String hostname;

    private Integer port;

    private boolean local;

    public String getAETitle() {

        return aeTitle;
    }

    public void setAETitle(String aeTitle) {

        this.aeTitle = aeTitle;
    }

    public String getHostname() {

        return hostname;
    }

    public void setHostname(String hostname) {

        this.hostname = hostname;
    }

    public Integer getPort() {

        return port;
    }

    public void setPort(Integer port) {

        this.port = port;
    }

    public boolean isLocal() {

        return local;
    }

    public void setLocal(boolean local) {

        this.local = local;
    }
}
