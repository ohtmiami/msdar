package org.oht.miami.msdar.service.search;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.jboss.logging.Logger;
import org.oht.miami.msdar.data.PatientInfoRepository;
import org.oht.miami.msdar.data.SeriesInfoRepository;
import org.oht.miami.msdar.data.StudyInfoRepository;
import org.oht.miami.dcm4che.net.service.BasicCFindSCP;
import org.oht.miami.msdar.model.PatientInfo;
import org.oht.miami.msdar.model.SeriesInfo;
import org.oht.miami.msdar.model.StudyInfo;

@ApplicationScoped
public class DicomSearchService {

    @Inject
    Logger log;

    @Inject
    private PatientInfoRepository patientInfoRepository;

    @Inject
    private StudyInfoRepository studyInfoRepository;

    @Inject
    private SeriesInfoRepository seriesInfoRepository;

    public BasicCFindSCP newCFindSCP(String sopClass, String... qrLevels) {

        return new CFindSCP(this, sopClass, qrLevels);
    }

    List<PatientInfo> findPatients(DicomObject keys) throws PersistenceException {

        String[] patientIDs = keys.getStrings(Tag.PatientID);
        if (patientIDs == null || patientIDs.length == 0) {
            throw new UnsupportedOperationException("Search criteria other than Patient ID"
                    + " + Issuer of Patient ID" + " are not yet supported");
        }
        String issuerOfPatientID = keys.getString(Tag.IssuerOfPatientID);
        if (issuerOfPatientID == null || issuerOfPatientID.isEmpty()) {
            if (patientIDs.length > 1) {
                throw new UnsupportedOperationException("Queries with multiple Patient IDs"
                        + " and no Issuer of Patient ID" + " are not yet supported");
            }
            return patientInfoRepository.findByPatientID(patientIDs[0]);
        } else {
            List<PatientInfo> patientInfos = new ArrayList<PatientInfo>(patientIDs.length);
            for (String patientID : patientIDs) {
                PatientInfo patientInfo = patientInfoRepository.findByPatientIDAndIssuer(patientID,
                        issuerOfPatientID);
                if (patientInfo != null) {
                    patientInfos.add(patientInfo);
                }
            }
            return patientInfos;
        }
    }

    PatientInfo findPatient(UUID patientUUID) throws PersistenceException {

        return patientInfoRepository.findByPreferredUUID(patientUUID);
    }

    List<StudyInfo> findStudies(DicomObject keys) throws PersistenceException {

        String[] studyInstanceUIDs = keys.getStrings(Tag.StudyInstanceUID);
        if (studyInstanceUIDs == null || studyInstanceUIDs.length == 0) {
            throw new UnsupportedOperationException("Search criteria other than Study Instance UID"
                    + " are not yet supported");
        }
        return studyInfoRepository.findByStudyInstanceUIDs(studyInstanceUIDs);
    }

    StudyInfo findStudy(String studyInstanceUID) throws PersistenceException {

        return studyInfoRepository.findByStudyInstanceUID(studyInstanceUID);
    }

    List<SeriesInfo> findSeries(DicomObject keys) throws PersistenceException {

        String studyInstanceUID = keys.getString(Tag.StudyInstanceUID);
        if (studyInstanceUID == null || studyInstanceUID.isEmpty()) {
            throw new IllegalArgumentException("Missing (0020,000D) Study Instance UID");
        }
        String[] seriesInstanceUIDs = keys.getStrings(Tag.SeriesInstanceUID);
        if (seriesInstanceUIDs == null || seriesInstanceUIDs.length == 0) {
            for (Iterator<DicomElement> keyIterator = keys.iterator(); keyIterator.hasNext();) {
                DicomElement key = keyIterator.next();
                switch (key.tag()) {
                case Tag.SpecificCharacterSet:
                case Tag.TimezoneOffsetFromUTC:
                case Tag.QueryRetrieveLevel:
                case Tag.StudyInstanceUID:
                    break;

                default:
                    if (!key.isEmpty()) {
                        throw new UnsupportedOperationException(
                                "Search criteria other than Series Instance UID"
                                        + " are not yet supported");
                    }
                }
            }

            // Study Instance UID is the only search criterion
            return seriesInfoRepository.findByStudyInstanceUID(studyInstanceUID);
        }
        return seriesInfoRepository.findBySeriesInstanceUIDs(studyInstanceUID, seriesInstanceUIDs);
    }
}
