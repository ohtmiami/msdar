package org.oht.miami.msdar.service.ingest;

import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.net.pdu.PresentationContext;
import org.jboss.logging.Logger;
import org.oht.miami.msdar.data.DeIdentifiedStudyInfoRepository;
import org.oht.miami.msdar.data.StudyInfoRepository;
import org.oht.miami.dcm4che.net.Association;
import org.oht.miami.dcm4che.net.PDVInputStream;
import org.oht.miami.dcm4che.net.Status;
import org.oht.miami.dcm4che.net.service.BasicCStoreSCP;
import org.oht.miami.dcm4che.net.service.DicomServiceException;
import org.oht.miami.msdar.service.StudyStorageService;
import org.oht.miami.msdtk.crypto.StudyCryptoException;
import org.oht.miami.msdtk.io.DicomNormalizer;
import org.oht.miami.msdtk.io.StudyFinder;
import org.oht.miami.msdtk.store.StudyStore;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

@ApplicationScoped
public class DicomIngestService extends BasicCStoreSCP implements StudyFinder {

    @Inject
    private Logger log;

    @Inject
    private StudyInfoRepository studyInfoRepository;

    @Inject
    private DeIdentifiedStudyInfoRepository deIdentifiedStudyInfoRepository;

    @Inject
    private StudyStorageService studyStorage;

    @Inject
    private ActiveStudiesService activeStudies;

    public DicomIngestService() {

        this.setSOPClasses("*");
    }

    public void onStudyTimeout(@Observes Study study) throws IOException, StudyCryptoException {

        DicomUID studyInstanceUID = study.getStudyInstanceUID();
        log.infof("Storing study %s...", studyInstanceUID);

        StudyStore studyStore = studyStorage.getStudyStore();
        // TODO How to handle exceptions?
        studyStore.releaseResources(studyInstanceUID);
        studyStore.writeStudy(study);
        studyInfoRepository.save(study);

        // TODO Make de-identification optional
        StudyStore deIdentifiedStudyStore = studyStorage.getDeIdentifiedStudyStore();
        Study deIdentifiedStudy = study.createDeIdentifiedCopy();
        deIdentifiedStudyStore.writeStudy(deIdentifiedStudy);
        for (UUID bulkDataUUID : study.getBulkDataUUIDs()) {
            Path bulkDataFile = studyStore.getBulkDataFile(studyInstanceUID, bulkDataUUID);
            Path bulkDataFileLink = deIdentifiedStudyStore.getBulkDataFile(
                    deIdentifiedStudy.getStudyInstanceUID(), bulkDataUUID);
            try {
                Files.createSymbolicLink(bulkDataFileLink, bulkDataFile);
            } catch (FileSystemException e) {
                Files.createLink(bulkDataFileLink, bulkDataFile);
            }
        }
        deIdentifiedStudyInfoRepository.save(deIdentifiedStudy, study);
    }

    @Override
    protected void store(Association association, PresentationContext pc, DicomObject rq,
            PDVInputStream dataStream, DicomObject rsp) throws IOException {

        DicomObject attributeSet = new BasicDicomObject();
        String cuid = rq.getString(Tag.AffectedSOPClassUID);
        String iuid = rq.getString(Tag.AffectedSOPInstanceUID);
        String tsuid = pc.getTransferSyntax();
        attributeSet.initFileMetaInformation(cuid, iuid, tsuid);

        DicomNormalizer normalizer = new DicomNormalizer(studyStorage.getStudyStore(), this);
        try (DicomInputStream dicomIn = new DicomInputStream(dataStream, tsuid)) {
            normalizer.normalize(dicomIn, attributeSet);
        } catch (Exception e) {
            // TODO Delete Bulk Data?
            throw new DicomServiceException(Status.ProcessingFailure, e);
        }
    }

    @Override
    public Study findOrCreateStudy(DicomUID studyInstanceUID) throws IOException,
            StudyCryptoException {

        try {
            return activeStudies.getActiveStudy(studyInstanceUID);
        } catch (Exception e) {
            if (e instanceof IOException) {
                log.errorf(e, "Failed to load active study %s", studyInstanceUID);
                throw (IOException) e;
            } else if (e instanceof StudyCryptoException) {
                log.errorf(e, "Failed to decrypt active study %s", studyInstanceUID);
                throw (StudyCryptoException) e;
            } else {
                // TODO How to handle other types of exceptions?
                log.errorf(e, "Failed to find or create study %s", studyInstanceUID);
                throw new RuntimeException("Unexpected error", e);
            }
        }
    }
}
