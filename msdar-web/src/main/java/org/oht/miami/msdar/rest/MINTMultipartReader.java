package org.oht.miami.msdar.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.plugins.providers.multipart.MultipartReader;

@Provider
@Consumes(HTTPConstants.MULTIPART_MINT)
public class MINTMultipartReader extends MultipartReader {
}
