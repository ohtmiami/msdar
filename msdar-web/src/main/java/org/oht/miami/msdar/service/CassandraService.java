package org.oht.miami.msdar.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.AlreadyExistsException;
import com.datastax.driver.core.exceptions.QueryExecutionException;

@Singleton
@Startup
public class CassandraService {

    private static final String CONFIG_RESOURCE_NAME = "cassandra.properties";

    private static final String[] DEFAULT_NODES = { "localhost" };

    private static final String DEFAULT_KEYSPACE_NAME = "msdar";

    private static final String DEFAULT_STRATEGY_CLASS = "SimpleStrategy";

    private static final int DEFAULT_REPLICATION_FACTOR = 1;

    private static final String CREATE_KEYSPACE_CQL = "CREATE KEYSPACE %s WITH replication"
            + " = { 'class' : '%s'" + ", 'replication_factor' : %d }";

    private static final String USE_KEYSPACE_CQL = "USE %s";

    @Inject
    private Logger log;

    private String[] nodes;

    private String keyspaceName;

    private String strategyClass;

    private int replicationFactor;

    private Cluster cluster;

    private Session session;

    @PostConstruct
    public void start() throws Exception {

        loadConfig(CONFIG_RESOURCE_NAME);

        cluster = Cluster.builder().addContactPoints(nodes).build();
        String clusterName = cluster.getMetadata().getClusterName();
        log.infof("Connected to Cassandra cluster %s", clusterName);

        session = cluster.connect();
        try {
            session.execute(String.format(CREATE_KEYSPACE_CQL, keyspaceName, strategyClass,
                    replicationFactor));
            log.infof("Created keyspace %s in cluster %s", keyspaceName, clusterName);
        } catch (AlreadyExistsException e) {
            log.infof("Found keyspace %s in cluster %s", keyspaceName, clusterName);
        }
        session.execute(String.format(USE_KEYSPACE_CQL, keyspaceName));
    }

    @PreDestroy
    public void stop() {

        if (cluster != null) {
            cluster.shutdown();
        }
    }

    private void loadConfig(String configResourceName) {

        Properties config = new Properties();
        try (InputStream configIn = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(configResourceName)) {
            config.load(configIn);
            log.infof("Using Cassandra configuration %s", configResourceName);
        } catch (IOException e) {
            log.warnf(e, "Failed to load Cassandra configuration %s from classpath"
                    + " - using default configuration", configResourceName);
        }

        String nodesValue = config.getProperty("nodes");
        nodes = nodesValue == null ? DEFAULT_NODES : nodesValue.split(",");
        keyspaceName = config.getProperty("keyspaceName", DEFAULT_KEYSPACE_NAME);
        strategyClass = config.getProperty("strategyClass", DEFAULT_STRATEGY_CLASS);

        String replicationFactorValue = config.getProperty("replicationFactor");
        if (replicationFactorValue == null) {
            replicationFactor = DEFAULT_REPLICATION_FACTOR;
        } else {
            try {
                replicationFactor = Integer.parseInt(replicationFactorValue);
            } catch (NumberFormatException e) {
                log.warnf(e, "Invalid replication factor %s" + " - using default value %d instead",
                        replicationFactorValue, DEFAULT_REPLICATION_FACTOR);
                replicationFactor = DEFAULT_REPLICATION_FACTOR;
            }
        }
    }

    public String getKeyspaceName() {

        return keyspaceName;
    }

    public Session getSession() {

        return session;
    }

    public void createTableIfNotExists(String tableName, String createTableQuery,
            String... createIndexQueries) throws PersistenceException {

        try {
            session.execute(createTableQuery);
            for (String createIndexQuery : createIndexQueries) {
                session.execute(createIndexQuery);
            }
            log.infof("Created table %s in keyspace %s", tableName, keyspaceName);
        } catch (AlreadyExistsException e) {
            log.infof("Found table %s in keyspace %s", tableName, keyspaceName);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(String.format(
                    "Failed to create table %s in keyspace %s", tableName, keyspaceName), e);
        }
    }

    public static String generateValuePlaceholders(int numValues) {

        if (numValues <= 0) {
            throw new IllegalArgumentException("numValues");
        }

        final String valuePlaceholder = "  ?";
        final String delimiter = ",\n";

        StringBuilder sb = new StringBuilder(valuePlaceholder.length() * numValues
                + delimiter.length() * (numValues - 1));
        for (int i = 0; i < numValues - 1; i++) {
            sb.append(valuePlaceholder).append(delimiter);
        }
        sb.append(valuePlaceholder);
        return sb.toString();
    }
}
