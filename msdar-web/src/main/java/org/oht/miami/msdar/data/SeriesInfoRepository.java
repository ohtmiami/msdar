package org.oht.miami.msdar.data;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.jboss.logging.Logger;
import org.oht.miami.msdar.model.SeriesInfo;
import org.oht.miami.msdar.service.CassandraService;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Query;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.querybuilder.QueryBuilder;

@ApplicationScoped
public class SeriesInfoRepository {

    private static final String TABLE_NAME = "seriesinfo";

    private static enum Column {

        STUDY_INSTANCE_UID("study_instance_uid", DataType.text()), SERIES_INSTANCE_UID(
                "series_instance_uid", DataType.text()), SERIES_DATE("series_date", DataType
                .timestamp()), MODALITY("modality", DataType.text()), BODY_PART_EXAMINED(
                "body_part_examined", DataType.text()), PROTOCOL_NAME("protocol_name", DataType
                .text()), SERIES_DESCRIPTION("series_description", DataType.text()), PERFORMING_PHYSICIAN_NAME(
                "performing_physician_name", DataType.text()), SERIES_NUMBER("series_number",
                DataType.cint()), LATERALITY("laterality", DataType.text()), NUM_INSTANCES(
                "num_instances", DataType.cint()), NUM_FRAMES("num_frames", DataType.cint());

        private String name;

        private DataType dataType;

        private Column(String name, DataType dataType) {

            this.name = name;
            this.dataType = dataType;
        }

        @Override
        public String toString() {

            return String.format("%s %s", name, dataType);
        }
    }

    private static final String CREATE_TABLE_CQL = String.format("CREATE TABLE %s (\n", TABLE_NAME)
            + String.format("  %s,\n", Column.STUDY_INSTANCE_UID)
            + String.format("  %s,\n", Column.SERIES_INSTANCE_UID)
            + String.format("  %s,\n", Column.SERIES_DATE)
            + String.format("  %s,\n", Column.MODALITY)
            + String.format("  %s,\n", Column.BODY_PART_EXAMINED)
            + String.format("  %s,\n", Column.PROTOCOL_NAME)
            + String.format("  %s,\n", Column.SERIES_DESCRIPTION)
            + String.format("  %s,\n", Column.PERFORMING_PHYSICIAN_NAME)
            + String.format("  %s,\n", Column.SERIES_NUMBER)
            + String.format("  %s,\n", Column.LATERALITY)
            + String.format("  %s,\n", Column.NUM_INSTANCES)
            + String.format("  %s,\n", Column.NUM_FRAMES)
            + String.format("  PRIMARY KEY (%s, %s)\n", Column.STUDY_INSTANCE_UID.name,
                    Column.SERIES_INSTANCE_UID.name) + ")";

    private static final String INSERT_CQL = String.format("INSERT INTO %s (\n", TABLE_NAME)
            + String.format("  %s,\n", Column.STUDY_INSTANCE_UID.name)
            + String.format("  %s,\n", Column.SERIES_INSTANCE_UID.name)
            + String.format("  %s,\n", Column.SERIES_DATE.name)
            + String.format("  %s,\n", Column.MODALITY.name)
            + String.format("  %s,\n", Column.BODY_PART_EXAMINED.name)
            + String.format("  %s,\n", Column.PROTOCOL_NAME.name)
            + String.format("  %s,\n", Column.SERIES_DESCRIPTION.name)
            + String.format("  %s,\n", Column.PERFORMING_PHYSICIAN_NAME.name)
            + String.format("  %s,\n", Column.SERIES_NUMBER.name)
            + String.format("  %s,\n", Column.LATERALITY.name)
            + String.format("  %s,\n", Column.NUM_INSTANCES.name)
            + String.format("  %s\n", Column.NUM_FRAMES.name) + ") VALUES (\n"
            + CassandraService.generateValuePlaceholders(Column.values().length) + "\n)";

    private static final Query FIND_ALL_QUERY = QueryBuilder.select().all().from(TABLE_NAME);

    private static final String FIND_BY_SERIES_INSTANCE_UID_CQL = String.format("SELECT * FROM %s",
            TABLE_NAME)
            + String.format(" WHERE %s = ?", Column.STUDY_INSTANCE_UID.name)
            + String.format(" AND %s = ?", Column.SERIES_INSTANCE_UID.name);

    private static final String FIND_BY_STUDY_INSTANCE_UID_CQL = String.format("SELECT * FROM %s",
            TABLE_NAME) + String.format(" WHERE %s = ?", Column.STUDY_INSTANCE_UID.name);

    @Inject
    private Logger log;

    @Inject
    private CassandraService cassandra;

    private PreparedStatement insertQuery;

    private PreparedStatement findBySeriesInstanceUIDQuery;

    private PreparedStatement findByStudyInstanceUIDQuery;

    @PostConstruct
    private void init() throws PersistenceException {

        cassandra.createTableIfNotExists(TABLE_NAME, CREATE_TABLE_CQL);
        insertQuery = cassandra.getSession().prepare(INSERT_CQL);
        findBySeriesInstanceUIDQuery = cassandra.getSession().prepare(
                FIND_BY_SERIES_INSTANCE_UID_CQL);
        findByStudyInstanceUIDQuery = cassandra.getSession()
                .prepare(FIND_BY_STUDY_INSTANCE_UID_CQL);
    }

    public void save(SeriesInfo seriesInfo) throws PersistenceException {

        BoundStatement boundInsertQuery = new BoundStatement(insertQuery);
        boundInsertQuery.bind(seriesInfo.getStudyInstanceUID(), seriesInfo.getSeriesInstanceUID(),
                seriesInfo.getSeriesDate(), seriesInfo.getModality(),
                seriesInfo.getBodyPartExamined(), seriesInfo.getProtocolName(),
                seriesInfo.getSeriesDescription(), seriesInfo.getPerformingPhysicianName(),
                seriesInfo.getSeriesNumber(), seriesInfo.getLaterality(),
                seriesInfo.getNumInstances(), seriesInfo.getNumFrames());
        try {
            cassandra.getSession().execute(boundInsertQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(String.format("Failed to create/update database entry"
                    + " for series %s of study %s", seriesInfo.getSeriesInstanceUID(),
                    seriesInfo.getStudyInstanceUID()), e);
        }
        log.infof("Created/updated database entry for series %s of study %s",
                seriesInfo.getSeriesInstanceUID(), seriesInfo.getStudyInstanceUID());
    }

    private static SeriesInfo transformResult(Row row) {

        SeriesInfo seriesInfo = new SeriesInfo();
        seriesInfo.setStudyInstanceUID(row.getString(Column.STUDY_INSTANCE_UID.name));
        seriesInfo.setSeriesInstanceUID(row.getString(Column.SERIES_INSTANCE_UID.name));
        seriesInfo.setSeriesDate(row.getDate(Column.SERIES_DATE.name));
        seriesInfo.setModality(row.getString(Column.MODALITY.name));
        seriesInfo.setBodyPartExamined(row.getString(Column.BODY_PART_EXAMINED.name));
        seriesInfo.setProtocolName(row.getString(Column.PROTOCOL_NAME.name));
        seriesInfo.setSeriesDescription(row.getString(Column.SERIES_DESCRIPTION.name));
        seriesInfo.setPerformingPhysicianName(row.getString(Column.PERFORMING_PHYSICIAN_NAME.name));
        seriesInfo.setSeriesNumber(row.isNull(Column.SERIES_NUMBER.name) ? null : row
                .getInt(Column.SERIES_NUMBER.name));
        seriesInfo.setLaterality(row.getString(Column.LATERALITY.name));
        seriesInfo.setNumInstances(row.getInt(Column.NUM_INSTANCES.name));
        seriesInfo.setNumFrames(row.getInt(Column.NUM_FRAMES.name));
        return seriesInfo;
    }

    public List<SeriesInfo> findAll() throws PersistenceException {

        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(FIND_ALL_QUERY);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<SeriesInfo> seriesInfos = new ArrayList<SeriesInfo>();
        for (Row result : resultSet) {
            SeriesInfo seriesInfo = transformResult(result);
            seriesInfos.add(seriesInfo);
        }
        return seriesInfos;
    }

    public SeriesInfo findBySeriesInstanceUID(String studyInstanceUID, String seriesInstanceUID)
            throws PersistenceException {

        BoundStatement boundFindBySeriesInstanceUIDQuery = new BoundStatement(
                findBySeriesInstanceUIDQuery);
        boundFindBySeriesInstanceUIDQuery.bind(studyInstanceUID, seriesInstanceUID);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindBySeriesInstanceUIDQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        Row result = resultSet.one();
        return result == null ? null : transformResult(result);
    }

    public List<SeriesInfo> findBySeriesInstanceUIDs(String studyInstanceUID,
            String[] seriesInstanceUIDs) throws PersistenceException {

        Query findBySeriesInstanceUIDsQuery = QueryBuilder
                .select()
                .all()
                .from(TABLE_NAME)
                .where(QueryBuilder.eq(Column.STUDY_INSTANCE_UID.name, studyInstanceUID))
                .and(QueryBuilder
                        .in(Column.SERIES_INSTANCE_UID.name, (Object[]) seriesInstanceUIDs));
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(findBySeriesInstanceUIDsQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<SeriesInfo> seriesInfos = new ArrayList<SeriesInfo>();
        for (Row result : resultSet) {
            SeriesInfo seriesInfo = transformResult(result);
            seriesInfos.add(seriesInfo);
        }
        return seriesInfos;
    }

    public List<SeriesInfo> findByStudyInstanceUID(String studyInstanceUID)
            throws PersistenceException {

        BoundStatement boundFindByStudyInstanceUIDQuery = new BoundStatement(
                findByStudyInstanceUIDQuery);
        boundFindByStudyInstanceUIDQuery.bind(studyInstanceUID);
        ResultSet resultSet = null;
        try {
            resultSet = cassandra.getSession().execute(boundFindByStudyInstanceUIDQuery);
        } catch (QueryExecutionException e) {
            throw new PersistenceException(e);
        }

        List<SeriesInfo> seriesInfos = new ArrayList<SeriesInfo>();
        for (Row result : resultSet) {
            SeriesInfo seriesInfo = transformResult(result);
            seriesInfos.add(seriesInfo);
        }
        return seriesInfos;
    }
}
