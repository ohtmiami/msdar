package org.oht.miami.msdar.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.oht.miami.msdar.model.AEInfo;

@RequestScoped
public class AEListProducer {

    @Inject
    private AEInfoRepository aeInfoRepository;

    private List<AEInfo> aeInfos;

    @Produces
    @Named
    public List<AEInfo> getAEInfos() {

        return aeInfos;
    }

    public void onAEListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) AEInfo aeInfo) {

        retrieveAllAEInfos();
    }

    @PostConstruct
    public void retrieveAllAEInfos() {

        aeInfos = aeInfoRepository.findAll();
    }
}
