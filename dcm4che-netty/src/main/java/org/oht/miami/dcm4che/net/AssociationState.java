/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net;

import java.io.IOException;

import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.jboss.netty.buffer.ChannelBuffer;
import org.oht.miami.dcm4che.net.pdu.PDV;

public enum AssociationState {

    STA1("Sta1 - Idle") {

        @Override
        void abort(Association association, AAbort aa) {

            // NOOP
        }

        @Override
        void closeChannel(Association association) {

            // NOOP
        }

        @Override
        void closeChannelDelayed(Association association) {

            // NOOP
        }
    },
    STA2("Sta2 - Transport connection open") {

        @Override
        void receivedAssociateRQ(Association association, AAssociateRQ rq) throws IOException {

            association.onAssociateRQ(rq);
        }
    },
    STA3("Sta3 - Awaiting local A-ASSOCIATE response primitive") {
    },
    STA4("Sta4 - Awaiting transport connection opening to complete") {
    },
    STA5("Sta5 - Awaiting A-ASSOCIATE-AC or A-ASSOCIATE-RJ PDU") {

        @Override
        void receivedAssociateAC(Association association, AAssociateAC ac) throws IOException {

            association.onAssociateAC(ac);
        }

        @Override
        void receivedAssociateRJ(Association association, AAssociateRJ rj) throws IOException {

            association.onAssociateRJ(rj);
        }
    },
    STA6("Sta6 - Association established and ready for data transfer") {

        @Override
        void receivedPDV(Association association, PDV pdv) throws IOException {

            association.onPDV(pdv);
        }

        @Override
        public void sendPDataTF(Association association, ChannelBuffer pd) throws IOException {

            association.writePDataTF(pd);
        }

        @Override
        void receivedReleaseRQ(Association association) throws IOException {

            association.onReleaseRQ();
        }

        @Override
        void sendReleaseRQ(Association association) throws IOException {

            association.writeReleaseRQ();
        }
    },
    STA7("Sta7 - Awaiting A-RELEASE-RP PDU") {

        @Override
        void receivedPDV(Association association, PDV pdv) throws IOException {

            association.onPDV(pdv);
        }

        @Override
        void receivedReleaseRQ(Association association) throws IOException {

            association.onCollisionReleaseRQ();
        }

        @Override
        void receivedReleaseRP(Association association) throws IOException {

            association.onReleaseRP();
        }
    },
    STA8("Sta8 - Awaiting local A-RELEASE response primitive") {

        @Override
        public void sendPDataTF(Association association, ChannelBuffer pd) throws IOException {

            association.writePDataTF(pd);
        }
    },
    STA9("Sta9 - Release collision requestor side; awaiting A-RELEASE response") {
    },
    STA10("Sta10 - Release collision acceptor side; awaiting A-RELEASE-RP PDU") {

        @Override
        void receivedReleaseRP(Association association) throws IOException {

            association.onCollisionReleaseRP();
        }
    },
    STA11("Sta11 - Release collision requestor side; awaiting A-RELEASE-RP PDU") {

        @Override
        void receivedReleaseRP(Association association) throws IOException {

            association.onReleaseRP();
        }
    },
    STA12("Sta12 - Release collision acceptor side; awaiting A-RELEASE response primitive") {
    },
    STA13("Sta13 - Awaiting Transport Connection Close Indication") {

        @Override
        void receivedPDV(Association association, PDV pdv) throws IOException {

            // NOOP
        }

        @Override
        void receivedReleaseRQ(Association association) throws IOException {

            // NOOP
        }

        @Override
        void receivedReleaseRP(Association association) throws IOException {

            // NOOP
        }

        @Override
        void abort(Association association, AAbort aa) {

            // NOOP
        }

        @Override
        void closeChannelDelayed(Association association) {

            // NOOP
        }
    };

    private final String name;

    AssociationState(String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return name;
    }

    void receivedAssociateRQ(Association association, AAssociateRQ rq) throws IOException {

        association.unexpectedPDU("A-ASSOCIATE-RQ");
    }

    void receivedAssociateAC(Association association, AAssociateAC ac) throws IOException {

        association.unexpectedPDU("A-ASSOCIATE-AC");
    }

    void receivedAssociateRJ(Association association, AAssociateRJ rj) throws IOException {

        association.unexpectedPDU("A-ASSOCIATE-RJ");
    }

    void receivedPDV(Association association, PDV pdv) throws IOException {

        association.unexpectedPDU("P-DATA-TF");
    }

    void receivedReleaseRQ(Association association) throws IOException {

        association.unexpectedPDU("A-RELEASE-RQ");
    }

    void receivedReleaseRP(Association association) throws IOException {

        association.unexpectedPDU("A-RELEASE-RP");
    }

    public void sendPDataTF(Association association, ChannelBuffer pd) throws IOException {

        throw new AssociationStateException(this);
    }

    void sendReleaseRQ(Association association) throws IOException {

        throw new AssociationStateException(this);
    }

    void abort(Association association, AAbort aa) {

        association.writeAbort(aa);
    }

    void closeChannel(Association association) {

        association.doCloseChannel();
    }

    void closeChannelDelayed(Association association) {

        association.doCloseChannelDelayed();
    }
}
