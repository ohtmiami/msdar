/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.dcm4che.net;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerAssociationHandler extends AssociationHandler {

    private static Logger LOG = LoggerFactory.getLogger(ServerAssociationHandler.class);

    private final Device device;

    public ServerAssociationHandler(Device device) {

        super(device.getAllChannels());
        this.device = device;
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        Channel channel = e.getChannel();
        LOG.info("Accepted connection from {}", channel.getRemoteAddress());
        Channel parentChannel = channel.getParent();
        for (Connection localConnection : device.getLocalConnections()) {
            if (localConnection.getServerChannel() == parentChannel) {
                this.setAssociation(new Association(null, localConnection, channel));
                break;
            }
        }
        super.channelConnected(ctx, e);
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {

        Channel channel = e.getChannel();
        LOG.info("Disconnected from {}", channel.getRemoteAddress());
        super.channelDisconnected(ctx, e);
    }
}
