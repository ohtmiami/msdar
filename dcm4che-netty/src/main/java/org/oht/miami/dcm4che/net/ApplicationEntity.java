/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.UID;
import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.CommonExtendedNegotiation;
import org.dcm4che2.net.pdu.ExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.dcm4che2.net.pdu.UserIdentityAC;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationEntity implements Serializable {

    private static final long serialVersionUID = -2287584218576603937L;

    private static Logger LOG = LoggerFactory.getLogger(ApplicationEntity.class);

    private Device device;

    private String aeTitle = null;

    private String[] preferredCallingAETitles = {};

    private String[] preferredCalledAETitles = {};

    private final Set<String> acceptedCallingAETitles = new LinkedHashSet<String>();

    private boolean associationAcceptor = false;

    private boolean associationInitiator = false;

    private final List<Connection> localConnections = new ArrayList<Connection>(1);

    private final Map<String, TransferCapability> scuTCs = new HashMap<String, TransferCapability>();

    private final Map<String, TransferCapability> scpTCs = new HashMap<String, TransferCapability>();

    private transient DimseRQHandler dimseRQHandler;

    public ApplicationEntity(String aeTitle) {

        setAETitle(aeTitle);
    }

    public Device getDevice() {

        return device;
    }

    void setDevice(Device device) {

        if (device != null) {
            if (this.device != null) {
                throw new IllegalStateException("already owned by " + this.device.getName());
            }
            for (Connection localConnection : localConnections)
                if (localConnection.getDevice() != device) {
                    throw new IllegalStateException(localConnection + " not owned by "
                            + device.getName());
                }
        }
        this.device = device;
    }

    public String getAETitle() {

        return aeTitle;
    }

    public void setAETitle(String aeTitle) {

        if (aeTitle.isEmpty()) {
            throw new IllegalArgumentException("AE title cannot be empty");
        }
        if (device != null) {
            device.removeApplicationEntity(this.aeTitle);
        }
        this.aeTitle = aeTitle;
        if (device != null) {
            device.addApplicationEntity(this);
        }
    }

    public String[] getPreferredCalledAETitles() {

        return preferredCalledAETitles;
    }

    public void setPreferredCalledAETitles(String... aeTitles) {

        preferredCalledAETitles = aeTitles;
    }

    public String[] getPreferredCallingAETitles() {

        return preferredCallingAETitles;
    }

    public void setPreferredCallingAETitles(String... aeTitles) {

        preferredCallingAETitles = aeTitles;
    }

    public String[] getAcceptedCallingAETitles() {

        return acceptedCallingAETitles.toArray(new String[acceptedCallingAETitles.size()]);
    }

    public void setAcceptedCallingAETitles(String... aeTitles) {

        acceptedCallingAETitles.clear();
        for (String aeTitle : aeTitles) {
            acceptedCallingAETitles.add(aeTitle);
        }
    }

    public boolean isAcceptedCallingAETitle(String aeTitle) {

        return acceptedCallingAETitles.isEmpty() || acceptedCallingAETitles.contains(aeTitle);
    }

    public boolean isAssociationAcceptor() {

        return associationAcceptor;
    }

    public void setAssociationAcceptor(boolean acceptor) {

        associationAcceptor = acceptor;
    }

    public boolean isAssociationInitiator() {

        return associationInitiator;
    }

    public void setAssociationInitiator(boolean initiator) {

        associationInitiator = initiator;
    }

    public DimseRQHandler getDimseRQHandler() {

        if (dimseRQHandler != null) {
            return dimseRQHandler;
        }
        return device != null ? device.getDimseRQHandler() : null;
    }

    public void setDimseRQHandler(DimseRQHandler dimseRQHandler) {

        this.dimseRQHandler = dimseRQHandler;
    }

    void onDimseRQ(Association association, PresentationContext pc, Dimse dimse,
            DicomObject command, PDVInputStream dataStream) throws IOException {

        DimseRQHandler handler = getDimseRQHandler();
        if (handler == null) {
            LOG.error("DimseRQHandler not initalized");
            throw new AAbort();
        }
        handler.onDimseRQ(association, pc, dimse, command, dataStream);
    }

    public void addLocalConnection(Connection connection) {

        if (device != null && device != connection.getDevice()) {
            throw new IllegalStateException(connection + " not contained by Device: "
                    + device.getName());
        }
        localConnections.add(connection);
    }

    public boolean removeLocalConnection(Connection connection) {

        return localConnections.remove(connection);
    }

    List<Connection> getLocalConnections() {

        return localConnections;
    }

    public TransferCapability addTransferCapability(TransferCapability tc) {

        tc.setApplicationEntity(this);
        TransferCapability prevTC = (tc.getRole() == TransferCapability.Role.SCU ? scuTCs : scpTCs)
                .put(tc.getSOPClass(), tc);
        if (prevTC != null & prevTC != tc) {
            prevTC.setApplicationEntity(null);
        }
        return prevTC;
    }

    public TransferCapability removeTransferCapabilityFor(String sopClass,
            TransferCapability.Role role) {

        TransferCapability tc = (role == TransferCapability.Role.SCU ? scuTCs : scpTCs)
                .remove(sopClass);
        if (tc != null) {
            tc.setApplicationEntity(null);
        }
        return tc;
    }

    public Collection<TransferCapability> getTransferCapabilities() {

        Collection<TransferCapability> tcs = new ArrayList<TransferCapability>(scuTCs.size()
                + scpTCs.size());
        tcs.addAll(scuTCs.values());
        tcs.addAll(scpTCs.values());
        return tcs;
    }

    public Collection<TransferCapability> getTransferCapabilitiesFor(TransferCapability.Role role) {

        Map<String, TransferCapability> selectedTCs = role == TransferCapability.Role.SCU ? scuTCs
                : scpTCs;
        Collection<TransferCapability> tcs = new ArrayList<TransferCapability>(selectedTCs.size());
        tcs.addAll(selectedTCs.values());
        return tcs;
    }

    public TransferCapability getTransferCapabilityFor(String sopClass, TransferCapability.Role role) {

        return (role == TransferCapability.Role.SCU ? scuTCs : scpTCs).get(sopClass);
    }

    protected PresentationContext negotiate(AAssociateRQ rq, AAssociateAC ac,
            PresentationContext rqpc) {

        PresentationContext pc = new PresentationContext();
        pc.setPCID(rqpc.getPCID());

        String asuid = rqpc.getAbstractSyntax();
        TransferCapability tc = doRoleSelection(rq, ac, asuid);
        if (tc == null) {
            pc.setResult(PresentationContext.ABSTRACT_SYNTAX_NOT_SUPPORTED);
            pc.addTransferSyntax(rqpc.getTransferSyntax());
            return pc;
        }

        for (String tsuid : rqpc.getTransferSyntaxes()) {
            if (tc.containsTransferSyntax(tsuid)) {
                ExtendedNegotiation extNeg = tc.negotiate(rq.getExtendedNegotiationFor(asuid));
                if (extNeg != null) {
                    ac.addExtendedNegotiation(extNeg);
                }
                pc.setResult(PresentationContext.ACCEPTANCE);
                pc.addTransferSyntax(tsuid);
                return pc;
            }
        }

        pc.setResult(PresentationContext.TRANSFER_SYNTAX_NOT_SUPPORTED);
        pc.addTransferSyntax(rqpc.getTransferSyntax());
        return pc;
    }

    private TransferCapability doRoleSelection(AAssociateRQ rq, AAssociateAC ac, String asuid) {

        RoleSelection rqrs = rq.getRoleSelectionFor(asuid);
        if (rqrs == null) {
            return getTC(scpTCs, asuid, rq);
        }

        RoleSelection acrs = ac.getRoleSelectionFor(asuid);
        if (acrs != null) {
            return getTC(acrs.isSCU() ? scpTCs : scuTCs, asuid, rq);
        }

        TransferCapability scuTC = null;
        TransferCapability scpTC = null;
        boolean scu = rqrs.isSCU() && (scpTC = getTC(scpTCs, asuid, rq)) != null;
        boolean scp = rqrs.isSCP() && (scuTC = getTC(scuTCs, asuid, rq)) != null;
        ac.addRoleSelection(new RoleSelection(asuid, scu, scp));
        return scu ? scpTC : scuTC;
    }

    private TransferCapability getTC(Map<String, TransferCapability> tcs, String asuid,
            AAssociateRQ rq) {

        TransferCapability tc = tcs.get(asuid);
        if (tc != null) {
            return tc;
        }

        CommonExtendedNegotiation commonExtNeg = rq.getCommonExtendedNegotiationFor(asuid);
        if (commonExtNeg != null) {
            for (String cuid : commonExtNeg.getRelatedGeneralSOPClassUIDs()) {
                tc = tcs.get(cuid);
                if (tc != null) {
                    return tc;
                }
            }
            tc = tcs.get(commonExtNeg.getServiceClassUID());
            if (tc != null) {
                return tc;
            }
        }
        return tcs.get("*");
    }

    public Association connect(final Connection localConnection, Connection remoteConnection,
            AAssociateRQ rq) throws IncompatibleConnectionException, IOException,
            InterruptedException {

        if (device == null) {
            throw new IllegalStateException("Not attached to Device");
        }

        if (rq.getCallingAET() == null) {
            rq.setCallingAET(aeTitle);
        }
        rq.setMaxOpsInvoked(localConnection.getMaxOpsInvoked());
        rq.setMaxOpsPerformed(localConnection.getMaxOpsPerformed());
        rq.setMaxPDULength(localConnection.getReceivePDULength());

        final Object associationLock = new Object();
        ChannelFuture connectFuture = localConnection.connect(remoteConnection);
        connectFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                if (future.isSuccess()) {
                    Channel channel = future.getChannel();
                    Association association = new Association(ApplicationEntity.this,
                            localConnection, channel);
                    AssociationHandler associationHandler = channel.getPipeline().get(
                            AssociationHandler.class);
                    synchronized (associationLock) {
                        associationHandler.setAssociation(association);
                        associationLock.notifyAll();
                    }
                }
            }
        });

        int connectTimeout = localConnection.getConnectTimeout();
        if (connectTimeout > 0) {
            connectFuture.await(connectTimeout);
        } else {
            connectFuture.await();
        }
        if (!connectFuture.isSuccess()) {
            connectFuture.getChannel().close();
            throw new IOException("Failed to connect to " + remoteConnection + " from "
                    + localConnection, connectFuture.getCause());
        }

        AssociationHandler associationHandler = connectFuture.getChannel().getPipeline()
                .get(AssociationHandler.class);
        Association association = null;
        synchronized (associationLock) {
            while (true) {
                association = associationHandler.getAssociation();
                if (association == null) {
                    associationLock.wait();
                } else {
                    break;
                }
            }
        }
        association.writeAssociateRQ(rq);
        // If successful, state will be changed to STA6 by onAssociateAC()
        // Otherwise, state will be changed to STA1 by closeChannel() or
        // onAssociateRJ()
        association.waitForLeaving(AssociationState.STA5);
        return association;
    }

    public Association connect(Connection remoteConnection, AAssociateRQ rq)
            throws IncompatibleConnectionException, IOException, InterruptedException {

        return connect(findCompatibleConnection(remoteConnection), remoteConnection, rq);
    }

    public Connection findCompatibleConnection(Connection remoteConnection)
            throws IncompatibleConnectionException {

        for (Connection localConnection : localConnections) {
            if (localConnection.isCompatibleWith(remoteConnection)) {
                return localConnection;
            }
        }
        throw new IncompatibleConnectionException("No compatible connection to " + remoteConnection
                + " available on " + this);
    }

    // TODO toString()

    AAssociateAC negotiate(Association association, AAssociateRQ rq) throws AAssociateRJ {

        if ((rq.getProtocolVersion() & 1) == 0) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_PROVIDER_ACSE,
                    AAssociateRJ.REASON_PROTOCOL_VERSION_NOT_SUPPORTED);
        }
        if (!rq.getApplicationContext().equals(UID.DICOMApplicationContextName)) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_USER,
                    AAssociateRJ.REASON_APP_CTX_NAME_NOT_SUPPORTED);
        }
        if (!localConnections.contains(association.getLocalConnection())
                || !isAssociationAcceptor()) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_USER, AAssociateRJ.REASON_CALLED_AET_NOT_RECOGNIZED);
        }
        if (!isAcceptedCallingAETitle(rq.getCallingAET())) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_USER,
                    AAssociateRJ.REASON_CALLING_AET_NOT_RECOGNIZED);
        }
        // TODO
        UserIdentityAC userIdentity = null;
        // TODO device.isLimitOfOpenAssociationsExceeded()
        return makeAAssociateAC(association, rq, userIdentity);
    }

    private AAssociateAC makeAAssociateAC(Association association, AAssociateRQ rq,
            UserIdentityAC userIdentity) {

        AAssociateAC ac = new AAssociateAC();
        ac.setCalledAET(rq.getCalledAET());
        ac.setCallingAET(rq.getCallingAET());
        Connection connection = association.getLocalConnection();
        ac.setMaxPDULength(connection.getReceivePDULength());
        ac.setMaxOpsInvoked(minZeroAsMax(rq.getMaxOpsInvoked(), connection.getMaxOpsPerformed()));
        ac.setMaxOpsPerformed(minZeroAsMax(rq.getMaxOpsPerformed(), connection.getMaxOpsInvoked()));
        ac.setUserIdentity(userIdentity);
        for (PresentationContext rqpc : rq.getPresentationContexts()) {
            ac.addPresentationContext(negotiate(rq, ac, rqpc));
        }
        return ac;
    }

    private static int minZeroAsMax(int i1, int i2) {

        return i1 == 0 ? i2 : i2 == 0 ? i1 : Math.min(i1, i2);
    }
}
