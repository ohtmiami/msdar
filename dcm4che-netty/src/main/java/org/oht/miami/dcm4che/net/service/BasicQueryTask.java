/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/gunterze/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * See @authors listed below
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net.service;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.net.pdu.PresentationContext;
import org.oht.miami.dcm4che.net.Association;
import org.oht.miami.dcm4che.net.Commands;
import org.oht.miami.dcm4che.net.Status;

public class BasicQueryTask implements QueryTask {

    protected final Association association;

    protected final PresentationContext pc;

    protected final DicomObject rq;

    protected final DicomObject keys;

    protected volatile boolean cancelled;

    public BasicQueryTask(Association association, PresentationContext pc, DicomObject rq,
            DicomObject keys) {

        this.association = association;
        this.pc = pc;
        this.rq = rq;
        this.keys = keys;
    }

    @Override
    public void onCancelRQ(Association association) {

        cancelled = true;
    }

    @Override
    public void run() {

        int messageID = rq.getInt(Tag.MessageID, -1);
        association.addCancelRQHandler(messageID, this);
        try {
            while (!cancelled && hasMoreMatches()) {
                DicomObject match = adjust(nextMatch());
                if (match != null) {
                    int status = optionalKeyNotSupported(match) ? Status.PendingWarning
                            : Status.Pending;
                    association.writeDimseRSP(pc, Commands.mkCFindRSP(rq, status), match);
                }
            }
            int status = cancelled ? Status.Cancel : Status.Success;
            association.writeDimseRSP(pc, Commands.mkCFindRSP(rq, status));
        } catch (DicomServiceException e) {
            DicomObject rsp = e.mkRSP(0x8020, messageID);
            association.writeDimseRSP(pc, rsp, e.getDataSet());
        } finally {
            association.removeCancelRQHandler(messageID);
            close();
        }
    }

    protected void close() {
    }

    protected DicomObject nextMatch() throws DicomServiceException {

        throw wrapException(Status.UnableToProcess, new NoSuchElementException());
    }

    protected boolean hasMoreMatches() throws DicomServiceException {

        return false;
    }

    protected DicomObject adjust(DicomObject match) {

        if (match == null) {
            return null;
        }

        // See DICOM PS 3.4, C.4.1.1.3.2
        match.putString(Tag.QueryRetrieveLevel, VR.CS, keys.getString(Tag.QueryRetrieveLevel));
        match.putString(Tag.RetrieveAETitle, VR.AE, association.getCalledAETitle());
        return match;
    }

    protected boolean optionalKeyNotSupported(DicomObject match) {

        for (Iterator<DicomElement> attributeIterator = keys.iterator(); attributeIterator
                .hasNext();) {
            DicomElement attribute = attributeIterator.next();
            if (!match.contains(attribute.tag())) {
                return true;
            }
        }
        return false;
    }

    protected DicomServiceException wrapException(int status, Throwable cause) {

        return new DicomServiceException(status, cause);
    }
}
