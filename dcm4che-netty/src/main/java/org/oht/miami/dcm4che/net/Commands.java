/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/gunterze/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * See @authors listed below
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

public class Commands {

    public static DicomObject mkCStoreRSP(DicomObject command, int status) {

        return mkRSP(command, status, Dimse.C_STORE_RQ);
    }

    public static DicomObject mkCFindRSP(DicomObject command, int status) {

        return mkRSP(command, status, Dimse.C_FIND_RQ);
    }

    public static DicomObject mkCGetRSP(DicomObject command, int status) {

        return mkRSP(command, status, Dimse.C_GET_RQ);
    }

    public static DicomObject mkCMoveRSP(DicomObject command, int status) {

        return mkRSP(command, status, Dimse.C_MOVE_RQ);
    }

    public static DicomObject mkCEchoRSP(DicomObject command, int status) {

        return mkRSP(command, status, Dimse.C_ECHO_RQ);
    }

    private static DicomObject mkRSP(DicomObject rq, int status, Dimse dimse) {

        DicomObject rsp = new BasicDicomObject();
        rsp.putInt(Tag.CommandField, VR.US, dimse.commandFieldOfRSP());
        rsp.putInt(Tag.Status, VR.US, status);
        rsp.putInt(Tag.MessageIDBeingRespondedTo, VR.US, rq.getInt(Tag.MessageID, 0));
        rsp.putString(Tag.AffectedSOPClassUID, VR.UI, rq.getString(dimse.tagOfSOPClassUID()));
        int tagOfIUID = dimse.tagOfSOPInstanceUID();
        if (tagOfIUID != 0) {
            rsp.putString(Tag.AffectedSOPInstanceUID, VR.UI, rq.getString(tagOfIUID));
        }
        return rsp;
    }
}
