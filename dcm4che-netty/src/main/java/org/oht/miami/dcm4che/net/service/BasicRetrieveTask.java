/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/gunterze/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * See @authors listed below
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.PresentationContext;
import org.oht.miami.dcm4che.net.Association;
import org.oht.miami.dcm4che.net.AssociationStateException;
import org.oht.miami.dcm4che.net.Commands;
import org.oht.miami.dcm4che.net.DimseRSPHandler;
import org.oht.miami.dcm4che.net.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicRetrieveTask implements RetrieveTask {

    private static final Logger LOG = LoggerFactory.getLogger(BasicRetrieveTask.class);

    public static enum Service {

        C_GET {

            @Override
            DicomObject mkRSP(DicomObject rq, int status) {

                return Commands.mkCGetRSP(rq, status);
            }

            @Override
            void releaseStoreAssociation(Association storeAssociation) throws IOException {
            }

            @Override
            int commandFieldOfRSP() {

                return 0x8010;
            }
        },
        C_MOVE {

            @Override
            DicomObject mkRSP(DicomObject rq, int status) {

                return Commands.mkCMoveRSP(rq, status);
            }

            @Override
            void releaseStoreAssociation(Association storeAssociation) throws IOException {

                storeAssociation.release();
            }

            @Override
            int commandFieldOfRSP() {

                return 0x8021;
            }
        };

        abstract DicomObject mkRSP(DicomObject rq, int status);

        abstract void releaseStoreAssociation(Association storeAssociation) throws IOException;

        abstract int commandFieldOfRSP();
    }

    protected final Service service;

    protected final Association association;

    protected final PresentationContext pc;

    protected final DicomObject rq;

    protected int status = Status.Success;

    protected boolean sendPendingRSP;

    protected int pendingRSPInterval;

    protected boolean cancelled;

    protected int numWarningSubOps;

    protected int numCompletedSubOps;

    protected final List<String> failedInstances = new ArrayList<String>();

    protected final List<InstanceLocator> instances;

    protected int numOutstandingRSPs = 0;

    protected Object outstandingRSPLock = new Object();

    // TODO private ScheduledFuture<?> writePendingRSPFuture;

    public BasicRetrieveTask(Service service, Association association, PresentationContext pc,
            DicomObject rq, List<InstanceLocator> instances) {

        this.service = service;
        this.association = association;
        this.pc = pc;
        this.rq = rq;
        this.instances = instances;
    }

    public void setSendPendingRSP(boolean sendPendingRSP) {

        this.sendPendingRSP = sendPendingRSP;
    }

    public void setPendingRSPInterval(int pendingRSPInterval) {

        this.pendingRSPInterval = pendingRSPInterval;
    }

    @Override
    public void onCancelRQ(Association association) {

        cancelled = true;
    }

    @Override
    public void run() {

        int messageID = rq.getInt(Tag.MessageID, -1);
        association.addCancelRQHandler(messageID, this);
        try {
            if (!instances.isEmpty()) {
                Association storeAssociation = getStoreAssociation();
                if (pendingRSPInterval > 0) {
                    // TODO startWritePendingRSPs()
                }
                for (InstanceLocator instance : instances) {
                    if (!storeAssociation.isReadyForDataTransfer()) {
                        failedInstances.add(instance.iuid);
                        if (status != Status.UnableToPerformSubOperations) {
                            status = Status.UnableToPerformSubOperations;
                            LOG.warn(
                                    "{}: Unable to perform sub-operation: association to {} in state: {}",
                                    new Object[] { association,
                                                    storeAssociation.getRemoteAETitle(),
                                                    storeAssociation.getState() });
                        }
                        continue;
                    }
                    if (cancelled) {
                        status = Status.Cancel;
                        break;
                    }
                    if (sendPendingRSP) {
                        // TODO writePendingRSP()
                    }
                    try {
                        cstore(storeAssociation, instance);
                    } catch (Exception e) {
                        failedInstances.add(instance.iuid);
                        status = Status.UnableToPerformSubOperations;
                        LOG.warn(association
                                + ": Unable to perform sub-operation on association to "
                                + storeAssociation.getRemoteAETitle(), e);
                    }
                }
                waitForOutstandingCStoreRSPs(storeAssociation);
                releaseStoreAssociation(storeAssociation);
                // TODO stopWritePendingRSPs()
            }
            writeRSP(status);
        } catch (DicomServiceException e) {
            DicomObject rsp = e.mkRSP(service.commandFieldOfRSP(), messageID);
            writeRSP(rsp, e.getDataSet());
        } finally {
            association.removeCancelRQHandler(messageID);
            close();
        }
    }

    private void waitForOutstandingCStoreRSPs(Association storeAssociation) {

        try {
            synchronized (outstandingRSPLock) {
                while (numOutstandingRSPs > 0) {
                    outstandingRSPLock.wait();
                }
            }
        } catch (InterruptedException e) {
            LOG.warn(association + ": failed to wait for outstanding RSP on association to "
                    + storeAssociation.getRemoteAETitle(), e);
        }
    }

    protected Association getStoreAssociation() throws DicomServiceException {

        return association;
    }

    protected AAssociateRQ makeAAssociateRQ() {

        final String[] defaultTSUIDs = { UID.ExplicitVRLittleEndian, UID.ImplicitVRLittleEndian };

        AAssociateRQ aarq = new AAssociateRQ();
        aarq.setCallingAET(association.getLocalAETitle());
        aarq.setCalledAET(rq.getString(Tag.MoveDestination));
        for (InstanceLocator instance : instances) {
            if (!containsPresentationContextFor(aarq, instance.cuid, instance.tsuid)) {
                PresentationContext pc = new PresentationContext();
                pc.setPCID(aarq.getNumberOfPresentationContexts() * 2 + 1);
                pc.setAbstractSyntax(instance.cuid);
                pc.addTransferSyntax(instance.tsuid);
                aarq.addPresentationContext(pc);
            }
            for (String tsuid : defaultTSUIDs) {
                if (!instance.tsuid.equals(tsuid)
                        && !containsPresentationContextFor(aarq, instance.cuid, tsuid)) {
                    PresentationContext pc = new PresentationContext();
                    pc.setPCID(aarq.getNumberOfPresentationContexts() * 2 + 1);
                    pc.setAbstractSyntax(instance.cuid);
                    pc.addTransferSyntax(tsuid);
                    aarq.addPresentationContext(pc);
                }
            }
        }
        return aarq;
    }

    private static boolean containsPresentationContextFor(AAssociateRQ aarq, String asuid,
            String tsuid) {

        // TODO This is inefficient
        Collection<PresentationContext> pcs = aarq.getPresentationContexts();
        for (PresentationContext pc : pcs) {
            if (asuid.equals(pc.getAbstractSyntax()) && pc.getTransferSyntaxes().contains(tsuid)) {
                return true;
            }
        }
        return false;
    }

    protected void releaseStoreAssociation(Association storeAssociation) {

        try {
            service.releaseStoreAssociation(storeAssociation);
        } catch (AssociationStateException e) {
        } catch (IOException e) {
            LOG.warn(
                    association + ": failed to release association to "
                            + storeAssociation.getRemoteAETitle(), e);
        }
    }

    protected void cstore(Association storeAssociation, InstanceLocator instance)
            throws IOException, InterruptedException {

        String tsuid = selectTransferSyntaxFor(storeAssociation, instance);
        DimseRSPHandler rspHandler = new CStoreRSPHandler(association.nextMessageID(),
                instance.iuid);

        if (storeAssociation == association) {
            storeAssociation.cstore(instance.cuid, instance.iuid, rq.getInt(Tag.Priority, 0),
                    instance.getDataSetWriter(tsuid), tsuid, rspHandler);
        } else {
            storeAssociation.cstore(instance.cuid, instance.iuid, rq.getInt(Tag.Priority, 0),
                    association.getRemoteAETitle(), rq.getInt(Tag.MessageID, 0),
                    instance.getDataSetWriter(tsuid), tsuid, rspHandler);
        }

        synchronized (outstandingRSPLock) {
            ++numOutstandingRSPs;
        }
    }

    private class CStoreRSPHandler extends DimseRSPHandler {

        private final String sopInstanceUID;

        public CStoreRSPHandler(int messageID, String sopInstanceUID) {

            super(messageID);
            this.sopInstanceUID = sopInstanceUID;
        }

        @Override
        public void onDimseRSP(Association association, DicomObject command, DicomObject dataSet) {

            super.onDimseRSP(association, command, dataSet);
            int storeStatus = command.getInt(Tag.Status, -1);
            if (storeStatus == Status.Success) {
                ++numCompletedSubOps;
            } else if ((storeStatus & 0xB000) == 0xB000) {
                ++numWarningSubOps;
            } else {
                failedInstances.add(sopInstanceUID);
                if (status == Status.Success) {
                    status = Status.OneOrMoreFailures;
                }
            }
            synchronized (outstandingRSPLock) {
                if (--numOutstandingRSPs == 0)
                    outstandingRSPLock.notify();
            }
        }

        @Override
        public void onClose(Association association) {

            super.onClose(association);
            synchronized (outstandingRSPLock) {
                numOutstandingRSPs = 0;
                outstandingRSPLock.notify();
            }
        }
    }

    protected String selectTransferSyntaxFor(Association storeAssociation, InstanceLocator instance) {

        return instance.tsuid;
    }

    public void writePendingRSP() {

        writeRSP(Status.Pending);
    }

    private void writeRSP(int status) {

        DicomObject command = service.mkRSP(rq, status);
        if (status == Status.Pending || status == Status.Cancel) {
            command.putInt(Tag.NumberOfRemainingSuboperations, VR.US, remaining());
        }
        command.putInt(Tag.NumberOfCompletedSuboperations, VR.US, numCompletedSubOps);
        command.putInt(Tag.NumberOfFailedSuboperations, VR.US, failedInstances.size());
        command.putInt(Tag.NumberOfWarningSuboperations, VR.US, numWarningSubOps);
        DicomObject dataSet = null;
        if (!failedInstances.isEmpty() && status != Status.Pending) {
            dataSet = new BasicDicomObject();
            dataSet.putStrings(Tag.FailedSOPInstanceUIDList, VR.UI,
                    failedInstances.toArray(new String[failedInstances.size()]));
        }
        writeRSP(command, dataSet);
    }

    private void writeRSP(DicomObject command, DicomObject dataSet) {

        association.writeDimseRSP(pc, command, dataSet);
    }

    private int remaining() {

        return instances.size() - numCompletedSubOps - numWarningSubOps - failedInstances.size();
    }

    protected void close() {
    }
}
