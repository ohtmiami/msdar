/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/gunterze/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * See @authors listed below
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;

public class Connection implements Serializable {

    private static final long serialVersionUID = 4322779974607163439L;

    public static final int SYNCHRONOUS_MODE = 1;

    public static final int NOT_LISTENING = -1;

    public static final int DEF_SOCKET_DELAY = 50;

    public static final int DEF_MAX_PDU_LENGTH = 16378;

    private Device device;

    private String hostname;

    private int port = NOT_LISTENING;

    private int connectTimeout;

    private int requestTimeout;

    private int acceptTimeout;

    private int releaseTimeout;

    private int responseTimeout;

    private int retrieveTimeout;

    private int idleTimeout;

    private int socketCloseDelay = DEF_SOCKET_DELAY;

    private int sendPDULength = DEF_MAX_PDU_LENGTH;

    private int receivePDULength = DEF_MAX_PDU_LENGTH;

    private int maxOpsPerformed = SYNCHRONOUS_MODE;

    private int maxOpsInvoked = SYNCHRONOUS_MODE;

    private boolean packPDV = true;

    private boolean tcpNoDelay = true;

    private transient volatile Channel serverChannel;

    private transient boolean rebindNeeded;

    public Connection() {
    }

    public Connection(String hostname) {

        this(hostname, NOT_LISTENING);
    }

    public Connection(String hostname, int port) {

        this.hostname = hostname;
        this.port = port;
    }

    public Device getDevice() {

        return device;
    }

    void setDevice(Device device) {

        if (device != null && this.device != null) {
            throw new IllegalStateException("already owned by " + device.getName());
        }
        this.device = device;
    }

    public String getHostname() {

        return hostname;
    }

    public void setHostname(String hostname) {

        if (hostname != null ? hostname.equals(this.hostname) : this.hostname == null) {
            return;
        }
        this.hostname = hostname;
        needRebind();
    }

    boolean isRebindNeeded() {

        return rebindNeeded;
    }

    void needRebind() {

        this.rebindNeeded = true;
    }

    public int getPort() {

        return port;
    }

    public void setPort(int port) {

        if (this.port == port) {
            return;
        }

        if ((port <= 0 || port > 0xFFFF) && port != NOT_LISTENING) {
            throw new IllegalArgumentException("port out of range:" + port);
        }
        this.port = port;
        needRebind();
    }

    public boolean isServer() {

        return port > 0;
    }

    public int getConnectTimeout() {

        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {

        if (connectTimeout < 0) {
            throw new IllegalArgumentException("connectTimeout: " + connectTimeout);
        }
        this.connectTimeout = connectTimeout;
    }

    public int getRequestTimeout() {

        return requestTimeout;
    }

    public void setRequestTimeout(int requestTimeout) {

        if (requestTimeout < 0) {
            throw new IllegalArgumentException("requestTimeout: " + requestTimeout);
        }
        this.requestTimeout = requestTimeout;
    }

    public int getAcceptTimeout() {

        return acceptTimeout;
    }

    public void setAcceptTimeout(int acceptTimeout) {

        if (acceptTimeout < 0) {
            throw new IllegalArgumentException("acceptTimeout: " + acceptTimeout);
        }
        this.acceptTimeout = acceptTimeout;
    }

    public int getReleaseTimeout() {

        return releaseTimeout;
    }

    public void setReleaseTimeout(int releaseTimeout) {

        if (releaseTimeout < 0) {
            throw new IllegalArgumentException("releaseTimeout: " + releaseTimeout);
        }
        this.releaseTimeout = releaseTimeout;
    }

    public int getResponseTimeout() {

        return responseTimeout;
    }

    public void setResponseTimeout(int responseTimeout) {

        if (responseTimeout < 0) {
            throw new IllegalArgumentException("responseTimeout: " + responseTimeout);
        }
        this.responseTimeout = responseTimeout;
    }

    public int getRetrieveTimeout() {

        return retrieveTimeout;
    }

    public void setRetrieveTimeout(int retrieveTimeout) {

        if (retrieveTimeout < 0) {
            throw new IllegalArgumentException("retrieveTimeout: " + retrieveTimeout);
        }
        this.retrieveTimeout = retrieveTimeout;
    }

    public int getIdleTimeout() {

        return idleTimeout;
    }

    public void setIdleTimeout(int idleTimeout) {

        if (idleTimeout < 0) {
            throw new IllegalArgumentException("idleTimeout: " + idleTimeout);
        }
        this.idleTimeout = idleTimeout;
    }

    public int getSocketCloseDelay() {

        return socketCloseDelay;
    }

    public void setSocketCloseDelay(int socketCloseDelay) {

        if (socketCloseDelay < 0) {
            throw new IllegalArgumentException("socketCloseDelay: " + socketCloseDelay);
        }
        this.socketCloseDelay = socketCloseDelay;
    }

    public boolean isTLS() {

        // TODO
        return false;
    }

    public int getSendPDULength() {

        return sendPDULength;
    }

    public void setSendPDULength(int sendPDULength) {

        this.sendPDULength = sendPDULength;
    }

    public int getReceivePDULength() {

        return receivePDULength;
    }

    public void setReceivePDULength(int receivePDULength) {

        this.receivePDULength = receivePDULength;
    }

    public int getMaxOpsPerformed() {

        return maxOpsPerformed;
    }

    public void setMaxOpsPerformed(int maxOpsPerformed) {

        this.maxOpsPerformed = maxOpsPerformed;
    }

    public int getMaxOpsInvoked() {

        return maxOpsInvoked;
    }

    public void setMaxOpsInvoked(int maxOpsInvoked) {

        this.maxOpsInvoked = maxOpsInvoked;
    }

    public boolean isPackPDV() {

        return packPDV;
    }

    public void setPackPDV(boolean packPDV) {

        this.packPDV = packPDV;
    }

    public boolean isTCPNoDelay() {

        return tcpNoDelay;
    }

    public void setTCPNoDelay(boolean tcpNoDelay) {

        this.tcpNoDelay = tcpNoDelay;
    }

    // TODO rebind()

    // TODO toString()

    private InetAddress addr() throws UnknownHostException {

        return hostname != null ? InetAddress.getByName(hostname) : null;
    }

    public InetSocketAddress getEndPoint() throws UnknownHostException {

        return new InetSocketAddress(addr(), port);
    }

    public Channel getServerChannel() {

        return serverChannel;
    }

    public synchronized boolean bind() throws IOException {

        if (!isServer()) {
            rebindNeeded = false;
            return false;
        }
        if (device == null) {
            throw new IllegalStateException("Not attached to Device");
        }
        if (isListening()) {
            throw new IllegalStateException("Already listening - " + serverChannel);
        }

        ServerBootstrap serverBootstrap = device.getServerBootstrap();
        serverBootstrap.setPipelineFactory(new ServerPipelineFactory(device));
        serverBootstrap.setOption("child.tcpNoDelay", tcpNoDelay);
        InetSocketAddress endPoint = getEndPoint();
        serverChannel = serverBootstrap.bind(endPoint);
        device.getAllChannels().add(serverChannel);
        rebindNeeded = false;
        return true;
    }

    public boolean isListening() {

        return serverChannel != null;
    }

    public synchronized void unbind() {

        // TODO
    }

    public ChannelFuture connect(Connection remoteConnection)
            throws IncompatibleConnectionException, IOException {

        if (!isCompatibleWith(remoteConnection)) {
            throw new IncompatibleConnectionException(remoteConnection.toString());
        }
        if (device == null) {
            throw new IllegalStateException("Not attached to Device");
        }

        ClientBootstrap clientBootstrap = device.getClientBootstrap();
        clientBootstrap.setPipelineFactory(new ClientPipelineFactory(device));
        clientBootstrap.setOption("tcpNoDelay", tcpNoDelay);
        InetSocketAddress bindPoint = getBindPoint();
        InetSocketAddress endPoint = new InetSocketAddress(remoteConnection.getHostname(),
                remoteConnection.getPort());
        return clientBootstrap.connect(endPoint, bindPoint);
    }

    public boolean isCompatibleWith(Connection remoteConnection) {

        // TODO
        return isTLS() == remoteConnection.isTLS();
    }

    private InetSocketAddress getBindPoint() throws UnknownHostException {

        return new InetSocketAddress(maskLoopbackAddress(addr()), 0);
    }

    private static InetAddress maskLoopbackAddress(InetAddress addr) {

        return addr != null && addr.isLoopbackAddress() ? null : addr;
    }
}
