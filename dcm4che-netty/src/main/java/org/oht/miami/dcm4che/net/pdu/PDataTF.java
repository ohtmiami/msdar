/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.dcm4che.net.pdu;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.net.pdu.PresentationContext;
import org.oht.miami.dcm4che.net.DataSetWriter;

public class PDataTF {

    private final PresentationContext pc;

    private final DicomObject command;

    private final DataSetWriter dataSetWriter;

    public PDataTF(PresentationContext pc, DicomObject command, DataSetWriter dataSetWriter) {

        this.pc = pc;
        this.command = command;
        this.dataSetWriter = dataSetWriter;
    }

    public PresentationContext getPresentationContext() {

        return pc;
    }

    public int getPCID() {

        return pc.getPCID();
    }

    public String getTransferSyntax() {

        return pc.getTransferSyntax();
    }

    public DicomObject getCommand() {

        return command;
    }

    public DataSetWriter getDataSetWriter() {

        return dataSetWriter;
    }
}
