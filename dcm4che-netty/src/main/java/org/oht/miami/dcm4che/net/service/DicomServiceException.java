/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/gunterze/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * See @authors listed below
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net.service;

import java.io.IOException;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;

public class DicomServiceException extends IOException {

    private static final long serialVersionUID = 1745198279893019251L;

    private final DicomObject rsp;

    private DicomObject dataSet;

    public DicomServiceException(int status) {

        rsp = new BasicDicomObject();
        setStatus(status);
    }

    public DicomServiceException(int status, String message) {

        super(message);
        rsp = new BasicDicomObject();
        setStatus(status);
        setErrorComment(getMessage());
    }

    public DicomServiceException(int status, Throwable cause) {

        super(cause);
        rsp = new BasicDicomObject();
        setStatus(status);
        setErrorComment(getMessage());
    }

    public static Throwable initialCauseOf(Throwable e) {

        if (e == null) {
            return null;
        }

        Throwable cause;
        while ((cause = e.getCause()) != null) {
            e = cause;
        }
        return e;
    }

    private void setStatus(int status) {

        rsp.putInt(Tag.Status, VR.US, status);
    }

    public DicomServiceException setUID(int tag, String value) {

        rsp.putString(tag, VR.UI, value);
        return this;
    }

    public DicomServiceException setErrorComment(String value) {

        if (value != null) {
            if (value.length() > 64) {
                value = value.substring(0, 64);
            }
            rsp.putString(Tag.ErrorComment, VR.LO, value);
        }
        return this;
    }

    public DicomServiceException setErrorID(int value) {

        rsp.putInt(Tag.ErrorID, VR.US, value);
        return this;
    }

    public DicomServiceException setEventTypeID(int value) {

        rsp.putInt(Tag.EventTypeID, VR.US, value);
        return this;
    }

    public DicomServiceException setActionTypeID(int value) {

        rsp.putInt(Tag.ActionTypeID, VR.US, value);
        return this;
    }

    public DicomServiceException setOffendingElements(int... tags) {

        rsp.putInts(Tag.OffendingElement, VR.AT, tags);
        return this;
    }

    public DicomServiceException setAttributeIdentifierList(int... tags) {

        rsp.putInts(Tag.AttributeIdentifierList, VR.AT, tags);
        return this;
    }

    public DicomObject mkRSP(int commandField, int messageID) {

        rsp.putInt(Tag.CommandField, VR.US, commandField);
        rsp.putInt(Tag.MessageIDBeingRespondedTo, VR.US, messageID);
        return rsp;
    }

    public DicomObject getDataSet() {

        return dataSet;
    }

    public DicomServiceException setDataSet(DicomObject dataSet) {

        this.dataSet = dataSet;
        return this;
    }

    // TODO valueOf()
}
