/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/gunterze/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 * See @authors listed below
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.ClientSocketChannelFactory;
import org.jboss.netty.channel.socket.ServerSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

public class Device implements Serializable {

    private static final long serialVersionUID = 2880955671228271981L;

    private final String name;

    private final List<Connection> localConnections = new ArrayList<Connection>();

    private final Map<String, ApplicationEntity> aes = new LinkedHashMap<String, ApplicationEntity>();

    private transient DimseRQHandler dimseRQHandler;

    private transient ExecutorService threadPool;

    private transient ScheduledExecutorService scheduledThreadPool;

    private transient final ChannelGroup allChannels;

    private transient final ServerSocketChannelFactory serverChannelFactory;

    private transient final ClientSocketChannelFactory clientChannelFactory;

    private transient final ServerBootstrap serverBootstrap;

    private transient final ClientBootstrap clientBootstrap;

    public Device(String name) {

        if (name.isEmpty()) {
            throw new IllegalArgumentException("name cannot be empty");
        }
        this.name = name;
        allChannels = new DefaultChannelGroup();
        serverChannelFactory = new NioServerSocketChannelFactory(Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool());
        clientChannelFactory = new NioClientSocketChannelFactory(Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool());
        serverBootstrap = new ServerBootstrap(serverChannelFactory);
        clientBootstrap = new ClientBootstrap(clientChannelFactory);
    }

    public String getName() {

        return name;
    }

    public DimseRQHandler getDimseRQHandler() {

        return dimseRQHandler;
    }

    public void setDimseRQHandler(DimseRQHandler dimseRQHandler) {

        this.dimseRQHandler = dimseRQHandler;
    }

    public void bindConnections() throws IOException {

        for (Connection localConnection : localConnections) {
            localConnection.bind();
        }
    }

    // TODO rebindConnections

    public void unbindConnections() {

        for (Connection localConnection : localConnections) {
            localConnection.unbind();
        }
    }

    public ExecutorService getThreadPool() {

        return threadPool;
    }

    public void setThreadPool(ExecutorService threadPool) {

        this.threadPool = threadPool;
    }

    public ScheduledExecutorService getScheduledThreadPool() {

        return scheduledThreadPool;
    }

    public void setScheduledThreadPool(ScheduledExecutorService scheduledThreadPool) {

        this.scheduledThreadPool = scheduledThreadPool;
    }

    public ChannelGroup getAllChannels() {

        return allChannels;
    }

    ServerSocketChannelFactory getServerChannelFactory() {

        return serverChannelFactory;
    }

    ClientSocketChannelFactory getClientChannelFactory() {

        return clientChannelFactory;
    }

    ServerBootstrap getServerBootstrap() {

        return serverBootstrap;
    }

    ClientBootstrap getClientBootstrap() {

        return clientBootstrap;
    }

    public void addLocalConnection(Connection connection) {

        connection.setDevice(this);
        localConnections.add(connection);
        connection.needRebind();
    }

    public boolean removeLocalConnection(Connection connection) {

        for (ApplicationEntity ae : aes.values()) {
            if (ae.getLocalConnections().contains(connection)) {
                throw new IllegalStateException(connection + " used by AE: " + ae.getAETitle());
            }
        }

        if (!localConnections.remove(connection)) {
            return false;
        }

        connection.setDevice(null);
        connection.unbind();
        return true;
    }

    List<Connection> getLocalConnections() {

        return localConnections;
    }

    public void addApplicationEntity(ApplicationEntity ae) {

        ae.setDevice(this);
        aes.put(ae.getAETitle(), ae);
    }

    public ApplicationEntity removeApplicationEntity(ApplicationEntity ae) {

        return removeApplicationEntity(ae.getAETitle());
    }

    public ApplicationEntity removeApplicationEntity(String aeTitle) {

        ApplicationEntity ae = aes.remove(aeTitle);
        if (ae != null) {
            ae.setDevice(null);
        }
        return ae;
    }

    public ApplicationEntity getApplicationEntity(String aeTitle) {

        ApplicationEntity ae = aes.get(aeTitle);
        if (ae == null) {
            ae = aes.get("*");
        }
        return ae;
    }

    // TODO toString()

    public void shutDown() {

        allChannels.close().awaitUninterruptibly();
        serverBootstrap.releaseExternalResources();
        clientBootstrap.releaseExternalResources();
    }
}
