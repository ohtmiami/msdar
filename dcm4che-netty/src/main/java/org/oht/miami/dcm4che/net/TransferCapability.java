/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net;

import org.dcm4che2.net.pdu.ExtendedNegotiation;

public class TransferCapability {

    public static enum Role {

        SCU, SCP
    };

    private static byte[] NO_EXT_INFO = {};

    private ApplicationEntity ae;

    private String commonName;

    private String sopClass;

    private Role role;

    private String[] transferSyntaxes;

    private byte[] extInfo = NO_EXT_INFO;

    /**
     * Creates the <code>TransferCapability</code> instance with the specified
     * presentation context..
     * 
     * @param sopClass
     *            A String containing the SOP Class UID.
     * @param role
     *            An enum constant of type <code>Role</code> defining the role
     *            selection (SCU or SCP) for this
     *            <code>TransferCapability</code>instance.
     * @param transferSyntaxes
     *            A String array containing the acceptable transfer syntaxes for
     *            <tt>sopClass</tt>.
     */
    public TransferCapability(String sopClass, Role role, String... transferSyntaxes) {

        setSOPClass(sopClass);
        setRole(role);
        setTransferSyntaxes(transferSyntaxes);
    }

    public void setApplicationEntity(ApplicationEntity ae) {

        if (ae != null && this.ae != null) {
            throw new IllegalStateException("already owned by AE " + this.ae.getAETitle());
        }
        this.ae = ae;
    }

    /**
     * Get the name of the Transfer Capability object. Can be a meaningful name
     * or any unique sequence of characters.
     * 
     * @return A String containing the common name.
     */
    public String getCommonName() {

        return commonName;
    }

    /**
     * Get the name of the Transfer Capability object. Can be a meaningful name
     * or any unique sequence of characters.
     * 
     * @param commonName
     *            A String containing the common name.
     */
    public void setCommonName(String commonName) {

        this.commonName = commonName;
    }

    /**
     * Get the role selection for this <code>TransferCapability</code>instance.
     * 
     * @return An enum constant of type <code>Role</code> defining the role
     *         selection (SCU or SCP) for this <code>TransferCapability</code>
     *         instance.
     */
    public Role getRole() {

        return role;
    }

    /**
     * Set the role selection for this <code>TransferCapability</code>instance.
     * 
     * @param role
     *            An enum constant of type <code>Role</code> defining the role
     *            selection (SCU or SCP) for this
     *            <code>TransferCapability</code>instance.
     */
    public void setRole(Role role) {

        if (role == null) {
            throw new NullPointerException("role");
        }

        if (this.role == role) {
            return;
        }

        if (this.ae != null) {
            ae.removeTransferCapabilityFor(sopClass, this.role);
        }

        this.role = role;

        if (ae != null) {
            ae.addTransferCapability(this);
        }
    }

    /**
     * Get the SOP Class of this Transfer Capability object.
     * 
     * @return A String containing the SOP Class UID.
     */
    public String getSOPClass() {

        return sopClass;
    }

    /**
     * Set the SOP Class of this Transfer Capability object.
     * 
     * @param sopClass
     *            A String containing the SOP Class UID.
     */
    public void setSOPClass(String sopClass) {

        if (sopClass == null) {
            throw new NullPointerException("sopClass");
        }
        this.sopClass = sopClass;
    }

    /**
     * Get the transfer syntax(es) that may be requested as an SCU or that are
     * offered as an SCP.
     * 
     * @return String array containing the transfer syntaxes.
     */
    public String[] getTransferSyntaxes() {

        return transferSyntaxes.clone();
    }

    /**
     * Set the transfer syntax(es) that may be requested as an SCU or that are
     * offered as an SCP.
     * 
     * @param transferSyntaxes
     *            String array containing the transfer syntaxes.
     */
    public void setTransferSyntaxes(String... transferSyntaxes) {

        if (transferSyntaxes.length == 0) {
            throw new IllegalArgumentException("transferSyntaxes.length = 0");
        }
        for (int i = 0; i < transferSyntaxes.length; i++) {
            if (transferSyntaxes[i] == null) {
                throw new NullPointerException("transferSyntaxes[" + i + "]");
            }
        }
        this.transferSyntaxes = transferSyntaxes.clone();
    }

    public boolean containsTransferSyntax(String tsuid) {

        if ("*".equals(transferSyntaxes[0])) {
            return true;
        }

        for (String s : transferSyntaxes) {
            if (tsuid.equals(s)) {
                return true;
            }
        }
        return false;
    }

    public byte[] getExtInfo() {

        return extInfo.clone();
    }

    public void setExtInfo(byte[] info) {

        extInfo = info != null ? (byte[]) info.clone() : NO_EXT_INFO;
    }

    /**
     * @param field
     * @return
     */
    public boolean getExtInfoBoolean(int field) {

        return extInfo != null && extInfo.length > field && extInfo[field] != 0;
    }

    /**
     * @param field
     * @return
     */
    public int getExtInfoInt(int field) {

        return extInfo != null && extInfo.length > field ? extInfo[field] & 0xff : 0;
    }

    /**
     * @param field
     * @param b
     */
    public void setExtInfoBoolean(int field, boolean b) {

        setExtInfoInt(field, b ? 1 : 0);
    }

    /**
     * @param field
     * @param value
     */
    public void setExtInfoInt(int field, int value) {

        extInfo[field] = (byte) value;
    }

    /**
     * Negotiate any extended negotiation items for the association.
     * 
     * @param offered
     *            The <code>ExtendedNegotiation</code> that was offered.
     * @return <code>ExtendedNegotiation</code> that was negotiated.
     */
    public ExtendedNegotiation negotiate(ExtendedNegotiation offered) {

        if (offered == null || extInfo == null) {
            return null;
        }
        byte[] info = offered.getInformation();
        for (int i = 0; i < info.length; i++) {
            info[i] &= getExtInfoInt(i);
        }
        return new ExtendedNegotiation(sopClass, info);
    }
}
