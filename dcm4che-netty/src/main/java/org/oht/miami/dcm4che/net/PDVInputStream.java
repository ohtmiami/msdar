package org.oht.miami.dcm4che.net;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomInputStream;

public class PDVInputStream extends PipedInputStream implements ReadableByteChannel {

    private static final int DEFAULT_PIPE_SIZE = 256 * 1024;

    private volatile boolean open;

    public PDVInputStream(PipedOutputStream pipedOut, int pipeSize) throws IOException {

        super(pipedOut, pipeSize);
        open = true;
    }

    public PDVInputStream(PipedOutputStream pipedOut) throws IOException {

        this(pipedOut, DEFAULT_PIPE_SIZE);
    }

    @Override
    public void close() throws IOException {

        super.close();
        open = false;
    }

    @Override
    public boolean isOpen() {

        return open;
    }

    @Override
    public synchronized int read(ByteBuffer dst) throws IOException {

        if (dst == null) {
            throw new NullPointerException();
        } else if (!dst.hasRemaining()) {
            return 0;
        }

        // Possibly wait on the first character
        int c = this.read();
        if (c < 0) {
            return -1;
        }
        dst.put((byte) c);

        int copied = 1;
        while (this.in >= 0 && dst.hasRemaining()) {
            int available;
            if (this.in > this.out) {
                available = Math.min(this.buffer.length - this.out, this.in - this.out);
            } else {
                available = this.buffer.length - this.out;
            }
            if (available > dst.remaining()) {
                available = dst.remaining();
            }

            dst.put(this.buffer, this.out, available);
            this.out += available;
            copied += available;

            if (this.out >= this.buffer.length) {
                this.out = 0;
            }
            if (this.in == this.out) {
                // Now empty
                this.in = -1;
            }
        }
        return copied;
    }

    public DicomObject readDataSet(String tsuid) throws IOException {

        try (DicomInputStream dicomIn = new DicomInputStream(this, tsuid)) {
            return dicomIn.readDicomObject();
        }
    }
}
