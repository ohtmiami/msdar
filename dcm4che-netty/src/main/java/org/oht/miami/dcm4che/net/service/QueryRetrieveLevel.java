package org.oht.miami.dcm4che.net.service;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.oht.miami.dcm4che.net.Status;

public enum QueryRetrieveLevel {

    PATIENT, STUDY, SERIES, IMAGE, FRAME;

    public static QueryRetrieveLevel valueOf(DicomObject attributeSet, String[] qrLevels)
            throws DicomServiceException {

        String level = attributeSet.getString(Tag.QueryRetrieveLevel);
        boolean valid = false;
        for (String qrLevel : qrLevels) {
            if (qrLevel.equals(level)) {
                valid = true;
                break;
            }
        }
        if (!valid) {
            throw new DicomServiceException(Status.IdentifierDoesNotMatchSOPClass);
        }
        return QueryRetrieveLevel.valueOf(level);
    }
}
