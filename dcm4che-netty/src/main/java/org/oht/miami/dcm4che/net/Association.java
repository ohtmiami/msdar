/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.dcm4che.net;

import java.io.IOException;
import java.io.PipedOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.UIDDictionary;
import org.dcm4che2.data.VR;
import org.dcm4che2.net.CommandUtils;
import org.dcm4che2.net.NoPresentationContextException;
import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.CommonExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.dcm4che2.util.CloseUtils;
import org.dcm4che2.util.IntHashtable;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.oht.miami.dcm4che.net.pdu.AReleaseRP;
import org.oht.miami.dcm4che.net.pdu.AReleaseRQ;
import org.oht.miami.dcm4che.net.pdu.PDV;
import org.oht.miami.dcm4che.net.pdu.PDVType;
import org.oht.miami.dcm4che.net.pdu.PDataTF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Association {

    public static Logger LOG = LoggerFactory.getLogger(Association.class);

    private static final AtomicInteger prevSerialNo = new AtomicInteger();

    private final AtomicInteger messageID = new AtomicInteger();

    private final int serialNo;

    private final boolean requestor;

    private String name;

    private ApplicationEntity ae;

    private final Device device;

    private final Connection localConnection;

    private final Channel channel;

    private AssociationState state;

    private AAssociateRQ associateRQ;

    private AAssociateAC associateAC;

    private IOException exception;

    private int maxOpsInvoked;

    private int maxPDULength;

    private int numPerformingOps;

    private final IntHashtable<DimseRSPHandler> rspHandlers = new IntHashtable<DimseRSPHandler>();

    private final IntHashtable<CancelRQHandler> cancelHandlers = new IntHashtable<CancelRQHandler>();

    private final Map<String, Map<String, PresentationContext>> pcMap = new HashMap<String, Map<String, PresentationContext>>();

    private int expectedPDVType = PDVType.COMMAND;

    private PresentationContext chosenPC = null;

    private PipedOutputStream pdvOut = null;

    private PDVInputStream pdvIn = null;

    private DicomObject command = null;

    private Dimse dimse = null;

    private Future<DicomObject> commandParserFuture = null;

    private Future<Void> dataSetParserFuture = null;

    Association(ApplicationEntity ae, Connection localConnection, Channel channel) {

        if (channel == null) {
            throw new NullPointerException("channel");
        }
        serialNo = prevSerialNo.incrementAndGet();
        this.ae = ae;
        requestor = ae != null;
        name = "Association(" + serialNo + ")";
        this.localConnection = localConnection;
        device = localConnection.getDevice();
        this.channel = channel;
        if (requestor) {
            setState(AssociationState.STA4);
        } else {
            setState(AssociationState.STA2);
            // TODO startRequestTimeout()
        }
    }

    public int nextMessageID() {

        return messageID.incrementAndGet() & 0xFFFF;
    }

    @Override
    public String toString() {

        return name;
    }

    public Channel getChannel() {

        return channel;
    }

    public Connection getLocalConnection() {

        return localConnection;
    }

    public AAssociateAC getAssociateAC() {

        return associateAC;
    }

    public AAssociateRQ getAssociateRQ() {

        return associateRQ;
    }

    public IOException getException() {

        return exception;
    }

    public ApplicationEntity getApplicationEntity() {

        return ae;
    }

    public boolean isRequestor() {

        return requestor;
    }

    public boolean isReadyForDataTransfer() {

        return state == AssociationState.STA6;
    }

    public boolean isSCPFor(String cuid) {

        RoleSelection rs = associateAC.getRoleSelectionFor(cuid);
        if (rs == null) {
            return !requestor;
        }
        return requestor ? rs.isSCP() : rs.isSCU();
    }

    private void checkIsSCU(String cuid) throws NoRoleSelectionException {

        if (!isSCUFor(cuid)) {
            throw new NoRoleSelectionException(cuid, TransferCapability.Role.SCU);
        }
    }

    public boolean isSCUFor(String cuid) {

        RoleSelection rs = associateAC.getRoleSelectionFor(cuid);
        if (rs == null) {
            return requestor;
        }
        return requestor ? rs.isSCU() : rs.isSCP();
    }

    public String getCallingAETitle() {

        return associateRQ != null ? associateRQ.getCallingAET() : null;
    }

    public String getCalledAETitle() {

        return associateRQ != null ? associateRQ.getCalledAET() : null;
    }

    public String getRemoteAETitle() {

        return requestor ? getCalledAETitle() : getCallingAETitle();
    }

    public String getLocalAETitle() {

        return requestor ? getCallingAETitle() : getCalledAETitle();
    }

    public String getRemoteImplementationVersionName() {

        return (requestor ? associateAC : associateRQ).getImplVersionName();
    }

    public String getRemoteImplementationClassUID() {

        return (requestor ? associateAC : associateRQ).getImplClassUID();
    }

    public String getLocalImplementationVersionName() {

        return (requestor ? associateRQ : associateAC).getImplVersionName();
    }

    public String getLocalImplementationClassUID() {

        return (requestor ? associateRQ : associateAC).getImplClassUID();
    }

    int getMaxPDULengthSend() {

        return maxPDULength;
    }

    boolean isPackPDV() {

        return localConnection.isPackPDV();
    }

    public void release() throws IOException {

        state.sendReleaseRQ(this);
    }

    void abort(AAbort aa) {

        state.abort(this, aa);
    }

    synchronized void closeChannel() {

        state.closeChannel(this);
    }

    void doCloseChannel() {

        LOG.info("{}: close {}", name, channel);
        channel.close();
        setState(AssociationState.STA1);
    }

    private synchronized void closeChannelDelayed() {

        state.closeChannelDelayed(this);
    }

    void doCloseChannelDelayed() {

        setState(AssociationState.STA13);
        int delay = localConnection.getSocketCloseDelay();
        if (delay > 0) {
            device.getScheduledThreadPool().schedule(new Runnable() {

                @Override
                public void run() {

                    closeChannel();
                }
            }, delay, TimeUnit.MILLISECONDS);
        } else {
            closeChannel();
        }
    }

    synchronized void onIOException(IOException e) {

        if (exception != null) {
            return;
        }

        exception = e;
        LOG.info("{}: i/o exception: {} in state: {}", new Object[] { name, e, state });
        closeChannel();
    }

    void writeAbort(AAbort aa) {

        LOG.info("{} << {}", name, aa);
        exception = aa;
        ChannelFuture writeFuture = channel.write(aa);
        writeFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                if (future.isSuccess()) {
                    closeChannelDelayed();
                }
            }
        });
    }

    void writeReleaseRQ() {

        LOG.info("{} << A-RELEASE-RQ", name);
        setState(AssociationState.STA7);
        channel.write(new AReleaseRQ());
    }

    public void waitForDimseRSP() throws InterruptedException {

        synchronized (rspHandlers) {
            while (!rspHandlers.isEmpty()) {
                rspHandlers.wait();
            }
        }
    }

    void writeAssociateRQ(AAssociateRQ rq) {

        name = rq.getCalledAET() + "(" + serialNo + ")";
        associateRQ = rq;
        LOG.info("{} << A-ASSOCIATE-RQ", name);
        setState(AssociationState.STA5);
        channel.write(rq);
    }

    private void writeAssociateAC(AAssociateAC ac) {

        LOG.info("{} << A-ASSOCIATE-AC", name);
        setState(AssociationState.STA6);
        channel.write(ac);
    }

    private void writeAssociateRJ(AAssociateRJ rj) {

        LOG.info("{} << {}", name, rj);
        ChannelFuture writeFuture = channel.write(rj);
        writeFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                if (future.isSuccess()) {
                    closeChannelDelayed();
                }
            }
        });
    }

    void checkException() throws IOException {

        if (exception != null) {
            throw exception;
        }
    }

    public AssociationState getState() {

        return state;
    }

    private synchronized void setState(AssociationState state) {

        this.state = state;
        this.notifyAll();
    }

    synchronized void waitForLeaving(AssociationState state) throws InterruptedException,
            IOException {

        while (this.state == state) {
            this.wait();
        }
        checkException();
    }

    void onClose() {

        synchronized (rspHandlers) {
            IntHashtable.Visitor visitor = new IntHashtable.Visitor() {

                @Override
                public boolean visit(int key, Object value) {

                    ((DimseRSPHandler) value).onClose(Association.this);
                    return true;
                }
            };
            rspHandlers.accept(visitor);
            rspHandlers.clear();
            rspHandlers.notifyAll();
        }
        if (ae != null) {
            DimseRQHandler dimseRQHandler = ae.getDimseRQHandler();
            if (dimseRQHandler != null) {
                dimseRQHandler.onClose(this);
            }
        }
    }

    void receivedAssociateRQ(AAssociateRQ rq) throws IOException {

        LOG.info("{}: A-ASSOCIATE-RQ {} >> {}",
                new String[] { name, rq.getCallingAET(), rq.getCalledAET() });
        state.receivedAssociateRQ(this, rq);
    }

    void onAssociateRQ(AAssociateRQ rq) {

        associateRQ = rq;
        name = rq.getCallingAET() + "(" + serialNo + ")";
        setState(AssociationState.STA3);
        try {
            ae = device.getApplicationEntity(rq.getCalledAET());
            if (ae == null) {
                throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                        AAssociateRJ.SOURCE_SERVICE_USER,
                        AAssociateRJ.REASON_CALLED_AET_NOT_RECOGNIZED);
            }
            associateAC = ae.negotiate(this, rq);
            initPCMap();
            maxOpsInvoked = associateAC.getMaxOpsInvoked();
            maxPDULength = minZeroAsMax(rq.getMaxPDULength(), localConnection.getSendPDULength());
            writeAssociateAC(associateAC);
        } catch (AAssociateRJ rj) {
            writeAssociateRJ(rj);
        }
    }

    void receivedAssociateAC(AAssociateAC ac) throws IOException {

        LOG.info("{} >> A-ASSOCIATE-AC", name);
        state.receivedAssociateAC(this, ac);
    }

    void onAssociateAC(AAssociateAC ac) {

        associateAC = ac;
        initPCMap();
        maxOpsInvoked = associateAC.getMaxOpsInvoked();
        maxPDULength = minZeroAsMax(associateAC.getMaxPDULength(),
                localConnection.getSendPDULength());
        setState(AssociationState.STA6);
    }

    void receivedAssociateRJ(AAssociateRJ rj) throws IOException {

        LOG.info("{} >> {}", name, rj);
        state.receivedAssociateRJ(this, rj);
    }

    void onAssociateRJ(AAssociateRJ rj) {

        exception = rj;
        closeChannel();
    }

    void receivedReleaseRQ() throws IOException {

        LOG.info("{} >> A-RELEASE-RQ", name);
        state.receivedReleaseRQ(this);
    }

    void onReleaseRQ() {

        setState(AssociationState.STA8);
        waitForPerformingOps();
        ChannelFuture writeFuture = channel.write(new AReleaseRP());
        writeFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                if (future.isSuccess()) {
                    closeChannelDelayed();
                }
            }
        });
    }

    private synchronized void waitForPerformingOps() {

        while (numPerformingOps > 0 && state == AssociationState.STA8) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    void onCollisionReleaseRQ() {

        if (requestor) {
            setState(AssociationState.STA9);
            LOG.info("{} << A-RELEASE-RP", name);
            channel.write(new AReleaseRP());
            setState(AssociationState.STA11);
        } else {
            setState(AssociationState.STA10);
        }
    }

    void receivedReleaseRP() throws IOException {

        LOG.info("{} >> A-RELEASE-RP", name);
        state.receivedReleaseRP(this);
    }

    void onReleaseRP() {

        closeChannel();
    }

    void onCollisionReleaseRP() {

        setState(AssociationState.STA12);
        LOG.info("{} << A-RELEASE-RP", name);
        ChannelFuture writeFuture = channel.write(new AReleaseRP());
        writeFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                if (future.isSuccess()) {
                    closeChannelDelayed();
                }
            }
        });
    }

    void receivedAbort(AAbort aa) {

        LOG.info("{}: >> {}", name, aa);
        exception = aa;
        closeChannel();
    }

    void unexpectedPDU(String pdu) throws AAbort {

        LOG.warn("{} >> unexpected {} in state: {}", new Object[] { name, pdu, state });
        throw new AAbort(AAbort.UL_SERIVE_PROVIDER, AAbort.UNEXPECTED_PDU);
    }

    void receivedPDV(PDV pdv) throws IOException {

        state.receivedPDV(this, pdv);
    }

    void onPDV(PDV pdv) throws IOException {

        if (pdv.getType() != expectedPDVType) {
            LOG.warn(
                    "{}: "
                            + (expectedPDVType == PDVType.COMMAND ? "Expected Command but received Data Set PDV"
                                    : "Expected Data Set but received Command PDV"), this);
            throw new AAbort();
        }

        int pcid = pdv.getPCID();
        if (chosenPC == null) {
            PresentationContext pc = associateAC.getPresentationContext(pcid);
            if (pc == null) {
                LOG.warn("{}: No Presentation Context with given ID - {}", this, pcid);
                throw new AAbort();
            }
            if (!pc.isAccepted()) {
                LOG.warn("{}: No accepted Presentation Context with given ID - {}", this, pcid);
                throw new AAbort();
            }
            chosenPC = pc;
        } else {
            int chosenPCID = chosenPC.getPCID();
            if (pcid != chosenPCID) {
                LOG.warn("{}: Expected PDV with pcid: {} but received with pcid: {}", new Object[] {
                                this, chosenPCID, pcid });
                throw new AAbort();
            }
        }

        if (pdvOut == null) {
            pdvOut = new PipedOutputStream();
            // TODO Tune pipe size
            pdvIn = new PDVInputStream(pdvOut);
            if (expectedPDVType == PDVType.COMMAND) {
                commandParserFuture = device.getThreadPool().submit(new Callable<DicomObject>() {

                    @Override
                    public DicomObject call() throws Exception {

                        return pdvIn.readDataSet(UID.ImplicitVRLittleEndian);
                    }
                });
            } else {
                dataSetParserFuture = device.getThreadPool().submit(new Callable<Void>() {

                    @Override
                    public Void call() throws Exception {

                        if (dimse.isRSP()) {
                            DicomObject dataSet = pdvIn.readDataSet(chosenPC.getTransferSyntax());
                            Dimse.LOG.debug("Data Set:\n{}", dataSet);
                            onDimseRSP(dimse, command, dataSet);
                        } else {
                            onDimseRQ(chosenPC, dimse, command, pdvIn);
                        }
                        return null;
                    }
                });
            }
        }
        ChannelBuffer pdvData = pdv.getData();
        pdvData.readBytes(pdvOut, pdvData.readableBytes());

        if (pdv.isLast()) {
            CloseUtils.safeClose(pdvOut);
            if (expectedPDVType == PDVType.COMMAND) {
                onLastCommandPDV();
            } else {
                onLastDataSetPDV();
            }
        }
    }

    private void onLastCommandPDV() throws IOException {

        try {
            command = commandParserFuture.get();
        } catch (InterruptedException e) {
            LOG.warn(this + ": Interrupted while parsing DIMSE Command", e);
            throw new AAbort();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                LOG.warn(this + ": Unexpected error while parsing DIMSE Command", e);
                throw new AAbort();
            }
        } finally {
            CloseUtils.safeClose(pdvIn);
        }

        pdvOut = null;
        pdvIn = null;

        try {
            dimse = Dimse.valueOf(command.getInt(Tag.CommandField, 0));
        } catch (IllegalArgumentException e) {
            Dimse.LOG.warn("{}: illegal DIMSE:", this);
            Dimse.LOG.warn("\n{}", command);
            throw new AAbort();
        }
        if (Dimse.LOG.isInfoEnabled()) {
            Dimse.LOG.info("{} >> {}", this,
                    dimse.toString(command, chosenPC.getPCID(), chosenPC.getTransferSyntax()));
            Dimse.LOG.debug("Command:\n{}", command);
        }

        if (CommandUtils.hasDataset(command)) {
            expectedPDVType = PDVType.DATA;
        } else {
            if (dimse == Dimse.C_CANCEL_RQ) {
                onCancelRQ(command);
            } else if (dimse.isRSP()) {
                onDimseRSP(dimse, command, null);
            } else {
                onDimseRQ(chosenPC, dimse, command, null);
            }
            chosenPC = null;
        }
    }

    private void onLastDataSetPDV() throws IOException {

        try {
            dataSetParserFuture.get();
        } catch (InterruptedException e) {
            LOG.warn(this + ": Interrupted while processing DIMSE", e);
            throw new AAbort();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                LOG.warn(this + ": Unexpected error while processing DIMSE", e);
                throw new AAbort();
            }
        } finally {
            CloseUtils.safeClose(pdvIn);
        }

        pdvOut = null;
        pdvIn = null;
        expectedPDVType = PDVType.COMMAND;
        chosenPC = null;
    }

    void sendPDataTF(ChannelBuffer pd) throws IOException {

        checkException();
        state.sendPDataTF(this, pd);
    }

    void writePDataTF(ChannelBuffer pd) {

        channel.write(pd);
    }

    void onDimseRQ(PresentationContext pc, Dimse dimse, DicomObject command,
            PDVInputStream dataStream) throws IOException {

        incPerforming();
        ae.onDimseRQ(this, pc, dimse, command, dataStream);
    }

    private synchronized void incPerforming() {

        ++numPerformingOps;
    }

    private synchronized void decPerforming() {

        --numPerformingOps;
        this.notifyAll();
    }

    void onDimseRSP(Dimse dimse, DicomObject command, DicomObject dataSet) throws IOException {

        int messageID = command.getInt(Tag.MessageIDBeingRespondedTo);
        int status = command.getInt(Tag.Status, 0);
        boolean pending = Status.isPending(status);
        DimseRSPHandler rspHandler = getDimseRSPHandler(messageID);
        if (rspHandler == null) {
            LOG.warn("{}: unexpected message ID in DIMSE RSP:\n{}", name, command);
            throw new AAbort();
        }
        rspHandler.onDimseRSP(this, command, dataSet);
        if (pending) {
            // TODO startTimeout()
        } else {
            removeDimseRSPHandler(messageID);
            if (rspHandlers.isEmpty() && numPerformingOps == 0) {
                // TODO startIdleOrReleaseTimeout();
            }
        }
    }

    private void addDimseRSPHandler(DimseRSPHandler rspHandler) throws InterruptedException {

        synchronized (rspHandlers) {
            while (maxOpsInvoked > 0 && rspHandlers.size() >= maxOpsInvoked) {
                rspHandlers.wait();
            }
            rspHandlers.put(rspHandler.getMessageID(), rspHandler);
        }
    }

    private DimseRSPHandler removeDimseRSPHandler(int messageID) {

        synchronized (rspHandlers) {
            DimseRSPHandler rspHandler = (DimseRSPHandler) rspHandlers.remove(messageID);
            rspHandlers.notifyAll();
            return rspHandler;
        }
    }

    private DimseRSPHandler getDimseRSPHandler(int messageID) {

        synchronized (rspHandlers) {
            return rspHandlers.get(messageID);
        }
    }

    void cancel(PresentationContext pc, int messageID) {

        DicomObject command = CommandUtils.mkCCancelRQ(messageID);
        channel.write(new PDataTF(pc, command, null));
    }

    public void writeDimseRSP(PresentationContext pc, DicomObject command) {

        writeDimseRSP(pc, command, null);
    }

    public void writeDimseRSP(PresentationContext pc, DicomObject command, DicomObject dataSet) {

        DataSetWriter dataSetWriter = null;
        int dataSetType = CommandUtils.NO_DATASET;
        if (dataSet != null) {
            dataSetWriter = new DataSetWriterAdapter(dataSet);
            dataSetType = CommandUtils.getWithDatasetType();
        }
        command.putInt(Tag.CommandDataSetType, VR.US, dataSetType);
        // TODO Run this in a separate thread?
        channel.write(new PDataTF(pc, command, dataSetWriter));
        if (!Status.isPending(command.getInt(Tag.Status, 0))) {
            decPerforming();
            // TODO startIdleTimeout()
        }
    }

    void onCancelRQ(DicomObject command) {

        int messageID = command.getInt(Tag.MessageIDBeingRespondedTo, -1);
        CancelRQHandler handler = removeCancelRQHandler(messageID);
        if (handler != null) {
            handler.onCancelRQ(this);
        }
    }

    public void addCancelRQHandler(int messageID, CancelRQHandler handler) {

        synchronized (cancelHandlers) {
            cancelHandlers.put(messageID, handler);
        }
    }

    public CancelRQHandler removeCancelRQHandler(int messageID) {

        synchronized (cancelHandlers) {
            return (CancelRQHandler) cancelHandlers.remove(messageID);
        }
    }

    private void initPCMap() {

        for (PresentationContext pc : associateAC.getPresentationContexts()) {
            if (pc.isAccepted()) {
                initTSMap(associateRQ.getPresentationContext(pc.getPCID()).getAbstractSyntax())
                        .put(pc.getTransferSyntax(), pc);
            }
        }
    }

    private Map<String, PresentationContext> initTSMap(String asuid) {

        Map<String, PresentationContext> tsMap = pcMap.get(asuid);
        if (tsMap == null) {
            pcMap.put(asuid, tsMap = new HashMap<String, PresentationContext>());
        }
        return tsMap;
    }

    private PresentationContext pcFor(String cuid, String tsuid)
            throws NoPresentationContextException {

        Map<String, PresentationContext> tsMap = pcMap.get(cuid);
        if (tsMap == null) {
            throw new NoPresentationContextException("Abstract Syntax "
                    + UIDDictionary.getDictionary().prompt(cuid) + " not supported");
        }
        if (tsuid == null) {
            return tsMap.values().iterator().next();
        }
        PresentationContext pc = tsMap.get(tsuid);
        if (pc == null) {
            throw new NoPresentationContextException("Abstract Syntax "
                    + UIDDictionary.getDictionary().prompt(cuid) + " with Transfer Syntax "
                    + UIDDictionary.getDictionary().prompt(tsuid) + " not supported");
        }
        return pc;
    }

    public Set<String> getTransferSyntaxesFor(String cuid) {

        Map<String, PresentationContext> tsMap = pcMap.get(cuid);
        if (tsMap == null) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet(tsMap.keySet());
    }

    public CommonExtendedNegotiation getCommonExtendedNegotiationFor(String cuid) {

        return associateAC.getCommonExtendedNegotiationFor(cuid);
    }

    public void cstore(String cuid, String iuid, int priority, DataSetWriter dataSetWriter,
            String tsuid, DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        cstore(cuid, cuid, iuid, priority, dataSetWriter, tsuid, rspHandler);
    }

    public void cstore(String asuid, String cuid, String iuid, int priority,
            DataSetWriter dataSetWriter, String tsuid, DimseRSPHandler rspHandler)
            throws IOException, InterruptedException {

        PresentationContext pc = pcFor(asuid, tsuid);
        checkIsSCU(cuid);
        DicomObject cstoreRQ = CommandUtils.mkCStoreRQ(rspHandler.getMessageID(), cuid, iuid,
                priority);
        invoke(pc, cstoreRQ, dataSetWriter, rspHandler, localConnection.getResponseTimeout());
    }

    public DimseRSP cstore(String asuid, String cuid, String iuid, int priority,
            DataSetWriter dataSetWriter, String tsuid) throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP(nextMessageID());
        cstore(asuid, cuid, iuid, priority, dataSetWriter, tsuid, rsp);
        return rsp;
    }

    public void cstore(String cuid, String iuid, int priority, String moveOriginatorAETitle,
            int moveOriginatorMessageID, DataSetWriter dataSetWriter, String tsuid,
            DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        cstore(cuid, cuid, iuid, priority, moveOriginatorAETitle, moveOriginatorMessageID,
                dataSetWriter, tsuid, rspHandler);
    }

    public void cstore(String asuid, String cuid, String iuid, int priority,
            String moveOriginatorAETitle, int moveOriginatorMessageID, DataSetWriter dataSetWriter,
            String tsuid, DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        PresentationContext pc = pcFor(asuid, tsuid);
        checkIsSCU(cuid);
        DicomObject cstoreRQ = CommandUtils.mkCStoreRQ(rspHandler.getMessageID(), cuid, iuid,
                priority);
        cstoreRQ.putString(Tag.MoveOriginatorApplicationEntityTitle, VR.AE, moveOriginatorAETitle);
        cstoreRQ.putInt(Tag.MoveOriginatorMessageID, VR.US, moveOriginatorMessageID);
        invoke(pc, cstoreRQ, dataSetWriter, rspHandler, localConnection.getResponseTimeout());
    }

    public DimseRSP cstore(String cuid, String iuid, int priority, String moveOriginatorAETitle,
            int moveOriginatorMessageID, DataSetWriter dataSetWriter, String tsuid)
            throws IOException, InterruptedException {

        return cstore(cuid, cuid, iuid, priority, moveOriginatorAETitle, moveOriginatorMessageID,
                dataSetWriter, tsuid);
    }

    public DimseRSP cstore(String asuid, String cuid, String iuid, int priority,
            String moveOriginatorAETitle, int moveOriginatorMessageID, DataSetWriter dataSetWriter,
            String tsuid) throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP(nextMessageID());
        cstore(asuid, cuid, iuid, priority, moveOriginatorAETitle, moveOriginatorMessageID,
                dataSetWriter, tsuid, rsp);
        return rsp;
    }

    public void cfind(String cuid, int priority, DicomObject dataSet, String tsuid,
            DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        cfind(cuid, cuid, priority, dataSet, tsuid, rspHandler);
    }

    public void cfind(String asuid, String cuid, int priority, DicomObject dataSet, String tsuid,
            DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        PresentationContext pc = pcFor(asuid, tsuid);
        checkIsSCU(cuid);
        DicomObject cfindRQ = CommandUtils.mkCFindRQ(rspHandler.getMessageID(), cuid, priority);
        invoke(pc, cfindRQ, new DataSetWriterAdapter(dataSet), rspHandler,
                localConnection.getResponseTimeout());
    }

    public DimseRSP cfind(String cuid, int priority, DicomObject dataSet, String tsuid,
            int autoCancel) throws IOException, InterruptedException {

        return cfind(cuid, cuid, priority, dataSet, tsuid, autoCancel);
    }

    public DimseRSP cfind(String asuid, String cuid, int priority, DicomObject dataSet,
            String tsuid, int autoCancel) throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP(nextMessageID());
        rsp.setAutoCancel(autoCancel);
        cfind(asuid, cuid, priority, dataSet, tsuid, rsp);
        return rsp;

    }

    public void cget(String cuid, int priority, DicomObject dataSet, String tsuid,
            DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        cget(cuid, cuid, priority, dataSet, tsuid, rspHandler);
    }

    public void cget(String asuid, String cuid, int priority, DicomObject dataSet, String tsuid,
            DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        PresentationContext pc = pcFor(asuid, tsuid);
        checkIsSCU(cuid);
        DicomObject cgetRQ = CommandUtils.mkCGetRQ(rspHandler.getMessageID(), cuid, priority);
        invoke(pc, cgetRQ, new DataSetWriterAdapter(dataSet), rspHandler,
                localConnection.getRetrieveTimeout());
    }

    public DimseRSP cget(String cuid, int priority, DicomObject dataSet, String tsuid)
            throws IOException, InterruptedException {

        return cget(cuid, cuid, priority, dataSet, tsuid);
    }

    public DimseRSP cget(String asuid, String cuid, int priority, DicomObject dataSet, String tsuid)
            throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP(nextMessageID());
        cget(asuid, cuid, priority, dataSet, tsuid, rsp);
        return rsp;
    }

    public void cmove(String cuid, int priority, DicomObject dataSet, String tsuid,
            String destination, DimseRSPHandler rspHandler) throws IOException,
            InterruptedException {

        cmove(cuid, cuid, priority, dataSet, tsuid, destination, rspHandler);
    }

    public void cmove(String asuid, String cuid, int priority, DicomObject dataSet, String tsuid,
            String destination, DimseRSPHandler rspHandler) throws IOException,
            InterruptedException {

        PresentationContext pc = pcFor(asuid, tsuid);
        checkIsSCU(cuid);
        DicomObject cmoveRQ = CommandUtils.mkCMoveRQ(rspHandler.getMessageID(), cuid, priority,
                destination);
        invoke(pc, cmoveRQ, new DataSetWriterAdapter(dataSet), rspHandler,
                localConnection.getRetrieveTimeout());
    }

    public DimseRSP cmove(String cuid, int priority, DicomObject dataSet, String tsuid,
            String destination) throws IOException, InterruptedException {

        return cmove(cuid, cuid, priority, dataSet, tsuid, destination);
    }

    public DimseRSP cmove(String asuid, String cuid, int priority, DicomObject dataSet,
            String tsuid, String destination) throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP(nextMessageID());
        cmove(asuid, cuid, priority, dataSet, tsuid, destination, rsp);
        return rsp;
    }

    public DimseRSP cecho() throws IOException, InterruptedException {

        return cecho(UID.VerificationSOPClass);
    }

    public DimseRSP cecho(String cuid) throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP(nextMessageID());
        PresentationContext pc = pcFor(cuid, null);
        checkIsSCU(cuid);
        DicomObject cechoRQ = CommandUtils.mkCEchoRQ(rsp.getMessageID(), cuid);
        invoke(pc, cechoRQ, null, rsp, localConnection.getResponseTimeout());
        return rsp;
    }

    void invoke(PresentationContext pc, DicomObject command, DataSetWriter dataSetWriter,
            DimseRSPHandler rspHandler, int rspTimeout) throws IOException, InterruptedException {

        checkException();
        rspHandler.setPresentationContext(pc);
        addDimseRSPHandler(rspHandler);
        channel.write(new PDataTF(pc, command, dataSetWriter));
        rspHandler.setTimeout(System.currentTimeMillis() + rspTimeout);
    }

    private static int minZeroAsMax(int i1, int i2) {

        return i1 == 0 ? i2 : i2 == 0 ? i1 : Math.min(i1, i2);
    }

    public DicomObject createFileMetaInformation(String cuid, String iuid, String tsuid) {

        DicomObject fileMetaInformation = new BasicDicomObject(7);
        fileMetaInformation.putBytes(Tag.FileMetaInformationVersion, VR.OB, new byte[] { 0, 1 });
        fileMetaInformation.putString(Tag.MediaStorageSOPClassUID, VR.UI, cuid);
        fileMetaInformation.putString(Tag.MediaStorageSOPInstanceUID, VR.UI, iuid);
        fileMetaInformation.putString(Tag.TransferSyntaxUID, VR.UI, tsuid);
        fileMetaInformation.putString(Tag.ImplementationClassUID, VR.UI,
                getRemoteImplementationClassUID());
        String implementationVersionName = getRemoteImplementationVersionName();
        if (implementationVersionName != null) {
            fileMetaInformation.putString(Tag.ImplementationVersionName, VR.SH,
                    implementationVersionName);
        }
        fileMetaInformation.putString(Tag.SourceApplicationEntityTitle, VR.SH, getRemoteAETitle());
        return fileMetaInformation;
    }
}
